package com.slob.aqfw.dialog;

import com.google.common.collect.ImmutableList;
import com.slob.aqfw.core.descriptor.DialogDescriptor;
import org.apache.commons.lang3.tuple.ImmutablePair;


public class Dialog {
    private final ImmutableList<ImmutablePair<String, String>> statements;

    Dialog(DialogDescriptor dialogDescriptor) {
        ImmutableList.Builder<ImmutablePair<String, String>> builder = ImmutableList.builder();
        for (DialogDescriptor.StatementDescriptor descriptor : dialogDescriptor.getstatements()) {
            ImmutablePair pair = ImmutablePair.of(descriptor.character, descriptor.dictionaryCode);
            builder.add(pair);
        }
        statements = builder.build();
    }

    public ImmutableList<ImmutablePair<String, String>> getStatements() {
        return statements;
    }
}
