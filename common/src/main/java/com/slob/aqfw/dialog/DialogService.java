package com.slob.aqfw.dialog;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.DialogDescriptor;

import java.util.*;

@Singleton
public class DialogService {
    private final Map<String, Dialog> dialogMap = new HashMap<>();
    private final List<Dialog> activeDialogs = new LinkedList<>();
    private final DescriptorReader reader;
    private int currentDialogIndex = -1;

    @Inject
    DialogService(DescriptorReader reader) {
        this.reader = reader;
    }

    public Optional<Dialog> getCurrentDialog() {
        if (currentDialogIndex != -1) {
            return Optional.of(activeDialogs.get(currentDialogIndex));
        }
        return Optional.empty();
    }

    public void loadDialog(String id) {
        if (!dialogMap.containsKey(id)) {
            DialogDescriptor dialogDescriptor = reader.getById(DialogDescriptor.class, id);
            Dialog dialog = new Dialog(dialogDescriptor);
            dialogMap.put(id, dialog);
        }
        currentDialogIndex++;
        activeDialogs.add(currentDialogIndex, dialogMap.get(id));
    }

    public List<Dialog> getActiveDialogs() {
        return activeDialogs;
    }

    public void resetDialogs() {
        currentDialogIndex = -1;
        activeDialogs.clear();
    }
}
