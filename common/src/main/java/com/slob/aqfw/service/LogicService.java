package com.slob.aqfw.service;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.slob.aqfw.camp.CampService;
import com.slob.aqfw.dialog.DialogService;
import com.slob.aqfw.level.LevelCache;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.service.dictionary.Dictionary;

public class LogicService {
    private static final String DEFAULT_LEVEL_ID = "EMPTY_LEVEL";
    private final Injector injector;
    private final LevelService levelService;
    private final CampService campService;
    private final GameStateCache gameStateCache;
    private final Dictionary dictionary;
    private final DialogService dialogService;
    private final LevelCache levelCache;

    @Inject
    LogicService(LevelService levelService, Injector injector, CampService campService, GameStateCache gameStateCache, Dictionary dictionary, DialogService dialogService, LevelCache levelCache) {
        this.levelService = levelService;
        this.injector = injector;
        this.campService = campService;
        this.gameStateCache = gameStateCache;
        this.dictionary = dictionary;
        this.dialogService = dialogService;
        this.levelCache = levelCache;
        gameStateCache.setGameState(GameState.MAIN_MENU);
    }

    public Injector getInjector() {
        return injector;
    }

    public LevelService getLevelService() {
        return levelService;
    }

    public CampService getCampService() {
        return campService;
    }

    public GameStateCache getGameStateCache() {
        return gameStateCache;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public DialogService getDialogService() {
        return dialogService;
    }

    public void handleGamestate() {
        if (gameStateCache.getGameState() == GameState.WON_LEVEL) {
            levelCache.setCurrentLevelId(levelCache.getNextLevelId());
        }
        if (gameStateCache.getGameState() == GameState.CAMP) {
            dialogService.resetDialogs();
            campService.startCamp();
        }
        if (gameStateCache.getGameState() == GameState.NEW_GAME) {
            gameStateCache.setGameState(GameState.START_LEVEL);
            levelCache.setNextLevelId(DEFAULT_LEVEL_ID);
        }
        if (gameStateCache.getGameState() == GameState.START_LEVEL) {
            dialogService.resetDialogs();
            levelService.generateLevel(levelCache.getNextLevelId());
            levelService.startLevel();
        }
    }
}
