package com.slob.aqfw.service.dictionary;

public enum Lang {
    ENGLISH("en"),
    POLISH("pl");

    private final String code;

    Lang(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
