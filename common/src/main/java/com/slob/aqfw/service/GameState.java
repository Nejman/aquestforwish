package com.slob.aqfw.service;

public enum GameState {
    PLAYERS_TURN,
    ENEMY_TURN,
    CAMP,
    LOST_LEVEL,
    WON_LEVEL,
    START_LEVEL,
    LOAD_LEVEL,
    NEW_GAME,
    MAIN_MENU
}
