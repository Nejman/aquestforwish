package com.slob.aqfw.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class GameStateCache {
    private GameState gameState;

    @Inject
    GameStateCache() {
        //empty guice constructor
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }
}
