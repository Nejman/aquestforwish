package com.slob.aqfw.service;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class MainService {
    private final LogicService service;

    public MainService() {
        Injector injector = Guice.createInjector();
        service = injector.getInstance(LogicService.class);
    }

    public LogicService getService() {
        return service;
    }
}
