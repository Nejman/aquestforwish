package com.slob.aqfw.service.dictionary;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.slob.aqfw.core.descriptor.DescriptorFilenameHelper;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.DictionaryDescriptor;
import com.slob.aqfw.core.skill.Skill;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Singleton
public class Dictionary {
    private static final String SKILL_NAME_SUFFIX = "_NAME";
    private static final String SKILL_DESCRIPTION_SUFFIX = "_DESCRIPTION";
    private final Map<String, List<Entry>> DICTIONARY_MAP;
    private String lang = Lang.ENGLISH.getCode();

    @Inject
    Dictionary(DescriptorReader descriptorReader) {
        ImmutableMap.Builder<String, List<Entry>> builder = ImmutableMap.builder();
        String filename = DescriptorFilenameHelper.getFilenameForDescriptorClass(DictionaryDescriptor.class);
        DictionaryDescriptor.DictionaryDescriptors fromFile = (DictionaryDescriptor.DictionaryDescriptors) descriptorReader.getFromFile(filename, DictionaryDescriptor.DictionaryDescriptors.class);

        for (DictionaryDescriptor descriptor : fromFile.getDictionaryDescriptors()) {
            ImmutableList.Builder<Entry> entryBuilder = ImmutableList.builder();
            for (DictionaryDescriptor.Entry entry : descriptor.getValues()) {
                entryBuilder.add(new Entry(entry.lang, entry.value));
            }
            builder.put(descriptor.getId(), entryBuilder.build());
        }
        DICTIONARY_MAP = builder.build();
    }

    private String getEntry(String id, String lang) {
        List<Entry> entries = DICTIONARY_MAP.get(id);
        Optional<Entry> entryOptional = entries.stream()
                .filter(entry -> entry.getLang().equals(lang))
                .findFirst();
        if (entryOptional.isPresent()) {
            return entryOptional.get().getValue();
        } else {
            return id;
        }
    }

    public String getSkillName(Skill skill) {
        return getEntry(skill.getId() + SKILL_NAME_SUFFIX, lang);
    }

    public String getSkillDescription(Skill skill) {
        return getEntry(skill.getId() + SKILL_DESCRIPTION_SUFFIX, lang);
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
