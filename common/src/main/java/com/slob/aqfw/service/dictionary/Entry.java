package com.slob.aqfw.service.dictionary;

public class Entry {
    private final String lang;
    private final String value;

    public Entry(String lang, String value) {
        this.lang = lang;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getLang() {
        return lang;
    }
}
