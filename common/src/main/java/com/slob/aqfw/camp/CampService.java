package com.slob.aqfw.camp;

import com.google.inject.Inject;
import com.slob.aqfw.camp.collection.Artifact;
import com.slob.aqfw.camp.collection.ArtifactRepository;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.dialog.DialogService;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.level.LevelCache;
import com.slob.aqfw.level.skill.item.ItemRepository;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class CampService {
    private final ArtifactRepository artifactRepository;
    private final ItemRepository itemRepository;
    private final LevelCache levelCache;
    private final DialogService dialogService;

    @Inject
    CampService(ArtifactRepository artifactRepository, ItemRepository itemRepository, LevelCache levelCache, DialogService dialogService) {
        this.artifactRepository = artifactRepository;
        this.itemRepository = itemRepository;
        this.levelCache = levelCache;
        this.dialogService = dialogService;
    }

    public List<Artifact> getCollectedArtifacts() {
        return artifactRepository.getArtifacts();
    }

    public List<Item> getAvailableItems() {
        return itemRepository.getUnassignedItems();
    }

    public Map<Character, Item> getAssignedItems() {
        return itemRepository.getCharacterItemMap();
    }

    public void changeItemAssignment(Item item, Character character) {
        Map<Character, Item> characterItemMap = itemRepository.getCharacterItemMap();
        List<Item> unassignedItems = itemRepository.getUnassignedItems();
        if (character != null) {
            Item assignedItem = characterItemMap.get(character);
            if (assignedItem != null) {
                unassignedItems.add(assignedItem);
            }
            characterItemMap.replace(character, item);
        } else {
            Set<Character> characters = characterItemMap.keySet();
            for (Character assignedCharacter : characters) {
                if (characterItemMap.get(assignedCharacter).equals(item)) {
                    characterItemMap.remove(assignedCharacter, item);
                }
            }
            unassignedItems.add(item);
        }
    }

    public void startCamp() {
        dialogService.resetDialogs();
        dialogService.loadDialog(levelCache.getCampDialogId());
    }
}
