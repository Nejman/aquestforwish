package com.slob.aqfw.camp.collection;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.LinkedList;
import java.util.List;

@Singleton
public class ArtifactRepository {
    private final List<Artifact> artifacts = new LinkedList<>();

    @Inject
    ArtifactRepository() {
        //empty guice constructor
    }

    public List<Artifact> getArtifacts() {
        return artifacts;
    }
}
