package com.slob.aqfw.level.ai;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterMovementManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillFactory;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.level.skill.SkillUseResult;
import com.slob.aqfw.level.skill.SkillUseService;

import java.util.Optional;

public abstract class AI {
    static final ImmutableList<String> MOVEMENT_SKILL_IDS = ImmutableList.of(SkillFactory.BASE_MOVEMENT_SKILL);
    final SkillUseService skillUseService;
    final SkillTargetingHelper skillTargetingHelper;
    final CharacterMovementManager characterMovementManager;
    final CellHelper cellHelper;

    @Inject
    AI(SkillUseService skillUseService, SkillTargetingHelper skillTargetingHelper, CharacterMovementManager characterMovementManager, CellHelper cellHelper) {
        this.skillUseService = skillUseService;
        this.skillTargetingHelper = skillTargetingHelper;
        this.characterMovementManager = characterMovementManager;
        this.cellHelper = cellHelper;
    }

    public abstract Optional<SkillUseResult> handleAI(Character character);
}
