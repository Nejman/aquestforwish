package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.level.trigger.Trigger;
import com.slob.aqfw.level.trigger.TriggerFactory;
import com.slob.aqfw.level.trigger.TriggerRepository;
import com.slob.aqfw.level.trigger.result.TriggerResult;

public class AddTriggersResultResultHandler implements ResultHandler {
    private final TriggerFactory triggerFactory;
    private final TriggerRepository triggerRepository;

    @Inject
    public AddTriggersResultResultHandler(TriggerFactory triggerFactory, TriggerRepository triggerRepository) {
        this.triggerFactory = triggerFactory;
        this.triggerRepository = triggerRepository;
    }

    @Override
    public void handle(TriggerResult triggerResult) {
        for (String id : triggerResult.getIds()) {
            Trigger trigger = triggerFactory.createTrigger(id);
            triggerRepository.getTriggers().add(trigger);
        }
    }
}
