package com.slob.aqfw.level.skill.heroes.wizard;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterAttackManager;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;

public class WizardAttackSkill extends Skill {
    private CharacterDamageManager healthManager;
    private CellHelper helper;

    private WizardAttackSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        WizardAttackSkill skill = new WizardAttackSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        skill.helper = injector.getInstance(CellHelper.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        int attackValue = CharacterAttackManager.getAttackValue(getUser());
        Effect effect = new Effect(EffectType.BASE_ATTACK, attackValue);
        if (targetedCell.getCharacter().isPresent()) {
            healthManager.changeCharacterHealth(effect, targetedCell.getCharacter().get(), getUser());
        }
    }
}
