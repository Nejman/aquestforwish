package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.level.trigger.Trigger;

import java.util.LinkedList;
import java.util.List;

public class GoToZoneAllTriggerHandler implements TriggerHandler {

    @Inject
    public GoToZoneAllTriggerHandler() {
        //empty guice constructor
    }

    @Override
    public boolean isMet(Trigger trigger) {
        List<String> charactersInZone = new LinkedList<>();
        trigger.getCells().forEach(cell -> cell.getCharacter().ifPresent(character -> charactersInZone.add(character.getId())));
        return charactersInZone.containsAll(trigger.getCharactersId());
    }
}
