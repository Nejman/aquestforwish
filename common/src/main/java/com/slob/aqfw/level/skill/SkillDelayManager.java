package com.slob.aqfw.level.skill;

import com.google.inject.Inject;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterRepository;

public class SkillDelayManager {
    private final CharacterRepository characterRepository;

    @Inject
    public SkillDelayManager(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    public void updateDelay() {
        for (Character character : characterRepository.getCharacterList()) {
            character.getSkills().stream()
                    .filter(skill -> skill.getCurrentDelay() > 0)
                    .forEach(skill -> skill.setCurrentDelay(skill.getCurrentDelay() - 1));
        }
    }
}
