package com.slob.aqfw.level.ai;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.slob.aqfw.core.ai.AI_Type;

@Singleton
public class AIRepository {
    private final FighterAIHandler fighterAIHandler;

    @Inject
    AIRepository(FighterAIHandler fighterAIHandler) {
        this.fighterAIHandler = fighterAIHandler;
    }

    public AI getAIForName(String name) {
        if (AI_Type.BASE_FIGHTER_AI.getName().equals(name)) {
            return fighterAIHandler;
        }
        throw new IllegalArgumentException("Unknown ai name: " + name);
    }
}
