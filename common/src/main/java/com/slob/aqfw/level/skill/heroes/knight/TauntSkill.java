package com.slob.aqfw.level.skill.heroes.knight;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;

public class TauntSkill extends Skill {

    private TauntSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        return new TauntSkill(descriptor, character);
    }

    @Override
    public void apply(Cell targetedCell) {
        Effect threatLevelIncreaseEffect = new Effect(EffectType.THREAT_CHANGE, getIntegerValue(SkillValueType.CHANGE_THREAT_VALUE));
        threatLevelIncreaseEffect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, 3);
        getUser().getActiveEffects().add(threatLevelIncreaseEffect);
        for (Side side : Side.values()) {
            Effect armourIncreaseEffect = new Effect(EffectType.ARMOR_CHANGE, getIntegerValue(SkillValueType.CHANGE_ARMOUR_VALUE));
            armourIncreaseEffect.getAdditionalValues().put(EffectAdditionalValueType.SIDE, side);
            armourIncreaseEffect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, 3);
            getUser().getActiveEffects().add(armourIncreaseEffect);
        }
        setCurrentDelay(getDelay());
    }
}
