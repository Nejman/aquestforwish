package com.slob.aqfw.level.skill.item;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.item.Item;

public class CloakOfHidingItem extends Item {
    @Override
    public void use(Entity character, Cell targetCell, Injector injector) {
        Effect effect = new Effect(EffectType.THREAT_CHANGE, -2);
        character.getActiveEffects().add(effect);
    }

    @Override
    public boolean isPassive() {
        return true;
    }
}
