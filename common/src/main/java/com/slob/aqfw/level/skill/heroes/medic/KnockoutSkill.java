package com.slob.aqfw.level.skill.heroes.medic;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.PointType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;

public class KnockoutSkill extends Skill {
    private KnockoutSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        return new KnockoutSkill(descriptor, character);
    }

    @Override
    public void apply(Cell targetedCell) {
        if (targetedCell.getCharacter().isPresent()) {
            Entity target = targetedCell.getCharacter().get();
            for (PointType pointType : PointType.values()) {
                Effect effect = new Effect(pointType.getCorrespondingEffectType(), -100);
                effect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, 1);
                target.getActiveEffects().add(effect);
            }
        }
    }
}
