package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.level.aura.AuraFactory;
import com.slob.aqfw.level.trigger.result.TriggerResult;

public class AddAurasResultResultHandler implements ResultHandler {
    private final AuraFactory auraFactory;

    @Inject
    public AddAurasResultResultHandler(AuraFactory auraFactory) {
        this.auraFactory = auraFactory;
    }

    @Override
    public void handle(TriggerResult triggerResult) {
        for (String id : triggerResult.getIds()) {
            for (Cell cell : triggerResult.getCells()) {
                auraFactory.createAura(id, cell);
            }
        }
    }
}
