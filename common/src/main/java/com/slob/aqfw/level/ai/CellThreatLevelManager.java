package com.slob.aqfw.level.ai;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterRepository;
import com.slob.aqfw.entities.characters.CharacterThreatManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.hex.CellRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CellThreatLevelManager {
    private final CellRepository cellRepository;
    private final CellHelper cellHelper;
    private final CharacterRepository characterRepository;

    @Inject
    CellThreatLevelManager(CellRepository cellRepository, CellHelper cellHelper, CharacterRepository repository) {
        this.cellRepository = cellRepository;
        this.cellHelper = cellHelper;
        this.characterRepository = repository;
    }

    public void updateCellsThreatLevels() {
        cellRepository.getCells().forEach(cell -> cell.setThreat(0));
        List<Character> threatningCharacters = characterRepository.getCharacterList().stream()
                .filter(character -> CharacterThreatManager.getThreatLevel(character) > 0)
                .collect(Collectors.toList());
        threatningCharacters.forEach(character -> cellHelper.getCharacterCell(character).setThreat(CharacterThreatManager.getThreatLevel(character)));

        for (Character character : threatningCharacters) {
            propagateThreat(character);
        }

    }

    private void propagateThreat(Character character) {
        Set<Cell> cellSet = new HashSet<>();
        Cell targetedCell = cellHelper.getCharacterCell(character);
        cellSet.add(targetedCell);
        for (int range = 0; range < CharacterThreatManager.getThreatLevel(character); range++) {
            Set<Cell> temporarySet = new HashSet<>();
            for (Cell cell : cellSet) {
                for (Side side : Side.values()) {
                    Cell neighbourSide = cell.getNeighbours().get(side);
                    if (neighbourSide != null && neighbourSide.getThreat() < cell.getThreat() - 1) {
                        neighbourSide.setThreat(cell.getThreat() - 1);
                        temporarySet.add(neighbourSide);
                    }
                }
            }
            cellSet = temporarySet;
        }
    }

}
