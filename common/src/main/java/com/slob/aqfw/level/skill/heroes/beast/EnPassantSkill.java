package com.slob.aqfw.level.skill.heroes.beast;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;

public class EnPassantSkill extends Skill {

    private EnPassantSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        return new EnPassantSkill(descriptor, character);
    }

    @Override
    public void apply(Cell targetedCell) {
        Effect effect = new Effect(EffectType.EN_PASSANT, getIntegerValue(SkillValueType.DURATION));
        getUser().getActiveEffects().add(effect);
    }
}
