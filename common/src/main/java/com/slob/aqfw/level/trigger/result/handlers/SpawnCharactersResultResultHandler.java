package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterManager;
import com.slob.aqfw.level.trigger.result.TriggerResult;

public class SpawnCharactersResultResultHandler implements ResultHandler {
    private final CharacterFactory characterFactory;
    private final CharacterManager characterManager;

    @Inject
    public SpawnCharactersResultResultHandler(CharacterFactory characterFactory, CharacterManager characterManager) {
        this.characterFactory = characterFactory;
        this.characterManager = characterManager;
    }

    @Override
    public void handle(TriggerResult triggerResult) {
        if (triggerResult.getIds().size() != triggerResult.getCells().size()) {
            throw new IllegalStateException("List size mismatch for trigger result with id: " + triggerResult.getId());
        }
        for (int index = 0; index < triggerResult.getIds().size(); index++) {
            String id = triggerResult.getIds().get(index);
            Cell cell = triggerResult.getCells().get(index);
            Character character = characterFactory.createCharacter(id);
            characterManager.addCharacter(character, cell.getX(), cell.getY());
        }
    }
}
