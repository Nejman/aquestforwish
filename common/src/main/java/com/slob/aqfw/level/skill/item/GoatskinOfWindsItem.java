package com.slob.aqfw.level.skill.item;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.KnockbackManager;

public class GoatskinOfWindsItem extends Item {
    @Override
    public void use(Entity character, Cell targetCell, Injector injector) {
        CellHelper cellHelper = injector.getInstance(CellHelper.class);
        Cell characterCell = cellHelper.getCharacterCell(character);
        KnockbackManager knockbackManager = injector.getInstance(KnockbackManager.class);
        for (Cell neighbour : characterCell.getNeighbours().values()) {
            if (neighbour.getCharacter().isPresent()) {
                knockbackManager.knockback(neighbour.getCharacter().get(), characterCell);
            }
        }
    }

    @Override
    public int getRange() {
        return 0;
    }
}
