package com.slob.aqfw.level.skill;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterMovementManager;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SkillTargetingHelper {
    private final CharacterMovementManager movementManager;
    private final CellHelper helper;

    @Inject
    public SkillTargetingHelper(CharacterMovementManager movementManager, CellHelper helper) {
        this.movementManager = movementManager;
        this.helper = helper;
    }

    public List<Cell> getAllowedTargetList(Skill skill) {
        if (skill.isMovementSkill()) {
            return movementManager.getAllowedCellsForMovement(skill.getRange(), skill.getUser());
        }
        List<Cell> result;
        Cell characterCell = helper.getCharacterCell(skill.getUser());
        switch (skill.getTargetsType()) {
            case LINE:
                result = getLineTargeting(skill);
                break;
            case SELF:
                result = ImmutableList.of(characterCell);
                break;
            case ANYWHERE:
                result = getAnywhereTargeting(characterCell, skill.getRange());
                break;
            default:
                throw new IllegalStateException("Unknown targeting type: " + skill.getTargetsType().toString());
        }

        return skill.modifyTargetedCells(result);
    }

    private static List<Side> getGlobalSideValuesForSkill(Skill skill) {
        ImmutableList.Builder<Side> result = ImmutableList.builder();

        int rotation = skill.getUser().getRotation();
        for (Side side : skill.getAllowedSides()) {
            int rotated = side.getSideCode() + rotation;
            result.add(Side.getSideForCode(rotated));
        }

        return result.build();
    }

    private List<Cell> getLineTargeting(Skill skill) {
        ImmutableList.Builder<Cell> result = ImmutableList.builder();
        Character user = (Character) skill.getUser();

        for (Side side : getGlobalSideValuesForSkill(skill)) {
            Cell targetedCell = helper.getCharacterCell(user).getNeighbours().get(side);
            int range = 0;
            while (targetedCell != null && range < skill.getRange()) {
                result.add(targetedCell);
                targetedCell = targetedCell.getNeighbours().get(side);
                range++;
            }
        }
        return result.build();

    }

    public List<Cell> getAnywhereTargeting(Cell targetedCell, int maxRange) {
        Set<Cell> cellSet = new HashSet<>();
        cellSet.add(targetedCell);
        for (int range = 0; range < maxRange; range++) {
            Set<Cell> temporarySet = new HashSet<>();
            for (Cell cell : cellSet) {
                for (Side side : Side.values()) {
                    Cell neighbourSide = cell.getNeighbours().get(side);
                    if (neighbourSide != null) {
                        temporarySet.add(neighbourSide);
                    }
                }
            }
            cellSet.addAll(temporarySet);
        }
        return ImmutableList.copyOf(cellSet);

    }

}
