package com.slob.aqfw.level.skill.base;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterMovementManager;
import com.slob.aqfw.entities.characters.CharacterSpeedManager;

public class BaseMovementSkill extends Skill {
    private CharacterMovementManager movementManager;

    private BaseMovementSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        BaseMovementSkill skill = new BaseMovementSkill(descriptor, character);
        skill.movementManager = injector.getInstance(CharacterMovementManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        movementManager.moveCharacter(getUser(), targetedCell);
    }

    @Override
    public int getRange() {
        return CharacterSpeedManager.getCharacterSpeed(getUser());
    }

    @Override
    public boolean isMovementSkill() {
        return true;
    }
}
