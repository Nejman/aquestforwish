package com.slob.aqfw.level.skill.heroes.medic;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterAttackManager;
import com.slob.aqfw.entities.characters.CharacterDamageManager;

public class HealingSkill extends Skill {
    private CharacterDamageManager healthManager;

    private HealingSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        HealingSkill skill = new HealingSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        int attackValue = CharacterAttackManager.getAttackValue(getUser());
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, attackValue);
        if (targetedCell.getCharacter().isPresent()) {
            healthManager.changeCharacterHealth(effect, targetedCell.getCharacter().get(), getUser());
        }
    }
}
