package com.slob.aqfw.level.skill.item;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.item.Item;

public class BarrierSealItem extends Item {
    @Override
    public void use(Entity character, Cell targetCell, Injector injector) {
        for (Side side : Side.values()) {
            Effect effect = new Effect(EffectType.ARMOR_CHANGE, 1);
            effect.getAdditionalValues().put(EffectAdditionalValueType.SIDE, side);
            character.getActiveEffects().add(effect);
        }
    }


    @Override
    public int getRange() {
        return 0;
    }
}
