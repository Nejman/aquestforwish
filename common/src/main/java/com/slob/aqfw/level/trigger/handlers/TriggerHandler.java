package com.slob.aqfw.level.trigger.handlers;

import com.slob.aqfw.level.trigger.Trigger;

public interface TriggerHandler {
    boolean isMet(Trigger trigger);

}
