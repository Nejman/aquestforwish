package com.slob.aqfw.level;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.*;
import com.slob.aqfw.level.ai.AI;
import com.slob.aqfw.level.ai.AIRepository;
import com.slob.aqfw.level.aura.AuraManager;
import com.slob.aqfw.level.effect.EffectManager;
import com.slob.aqfw.level.hex.CellRepository;
import com.slob.aqfw.level.skill.SkillDelayManager;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.level.skill.SkillUseResult;
import com.slob.aqfw.level.skill.SkillUseService;
import com.slob.aqfw.level.trigger.TriggerManager;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LevelService {
    private final CellRepository cellRepository;
    private final CharacterRepository characterRepository;
    private final AuraManager auraManager;
    private final EffectManager effectManager;
    private final CharacterPassiveSkillManager characterPassiveSkillManager;
    private final CharacterFactory characterFactory;
    private final SkillTargetingHelper skillTargetingHelper;
    private final SkillDelayManager skillDelayManager;
    private final AIRepository aiRepository;
    private final SkillUseService skillUseService;
    private final GameStateCache gameStateCache;
    private final CharacterManager characterManager;
    private final TriggerManager triggerManager;
    private final LevelFactory levelFactory;
    private final LevelCache levelCache;

    @Inject
    LevelService(CellRepository cellRepository, CharacterRepository characterRepository, AuraManager auraManager, EffectManager effectManager, CharacterPassiveSkillManager characterPassiveSkillManager, CharacterFactory characterFactory, SkillTargetingHelper skillTargetingHelper, SkillDelayManager skillDelayManager, AIRepository aiRepository, SkillUseService skillUseService, GameStateCache gameStateCache, CharacterManager characterManager, TriggerManager triggerManager, LevelFactory levelFactory, LevelCache levelCache) {
        this.cellRepository = cellRepository;
        this.characterRepository = characterRepository;
        this.auraManager = auraManager;
        this.effectManager = effectManager;
        this.characterPassiveSkillManager = characterPassiveSkillManager;
        this.characterFactory = characterFactory;
        this.skillTargetingHelper = skillTargetingHelper;
        this.skillDelayManager = skillDelayManager;
        this.aiRepository = aiRepository;
        this.skillUseService = skillUseService;
        this.gameStateCache = gameStateCache;
        this.characterManager = characterManager;
        this.triggerManager = triggerManager;
        this.levelFactory = levelFactory;
        this.levelCache = levelCache;
    }


    public Character createCharacter(String id) {
        return characterFactory.createCharacter(id);
    }

    public List<Cell> getAllowedCellsForSkill(Skill skill) {
        return skillTargetingHelper.getAllowedTargetList(skill);
    }

    public List<Character> getPlayerCharacters() {
        return ImmutableList.copyOf(
                characterRepository.getCharacterList().stream()
                        .filter(character -> character.getAlignment() == CharacterAlignment.GOOD)
                        .collect(Collectors.toList()));
    }

    public Optional<SkillUseResult> handleAITurn() {
        for (Character character : characterRepository.getCharacterList()) {
            if (!character.getId().startsWith(UnconciousCharacter.UNCONCIOUS_CHARACTTER_ID_PREFIX) &&
                    character.getAlignment() != CharacterAlignment.GOOD) {
                AI ai = aiRepository.getAIForName(character.getAiHandlerName());
                Optional<SkillUseResult> skillUseResult = ai.handleAI(character);
                if (skillUseResult.isPresent()) {
                    return skillUseResult;
                }
            }
        }
        endTurn();
        return Optional.empty();
    }


    public boolean isPlayersTurn() {
        return gameStateCache.getGameState() == GameState.PLAYERS_TURN;
    }

    public void endPlayerTurn() {
        gameStateCache.setGameState(GameState.ENEMY_TURN);
    }

    public void startLevel() {
        triggerManager.handleTriggers();
        gameStateCache.setGameState(GameState.PLAYERS_TURN);
    }

    public SkillUseResult performSkill(Skill skill, Cell target) {
        SkillUseResult skillUseResult = skillUseService.performSkill(skill, target);
        triggerManager.handleTriggers();
        return skillUseResult;
    }

    public void endTurn() {
        auraManager.update();
        effectManager.manageEffectsAtEndOfTurn();
        characterPassiveSkillManager.applyPassiveSkills();
        characterManager.updateCharactersLivingStatus();
        skillDelayManager.updateDelay();
        triggerManager.updateOnEndOfTurn();
        gameStateCache.setGameState(GameState.PLAYERS_TURN);
        characterRepository.getCharacterList().forEach(CharacterPointManager::resetAllPoints);
    }

    public void generateLevel() {
        generateLevel(levelCache.getCurrentLevelId());
    }

    public void generateLevel(String id) {
        clearLevelIfPresent();
        levelFactory.generateLevel(id);
    }

    public void addCharacter(Character character, int x, int y) {
        characterManager.addCharacter(character, x, y); // TODO DELETE
    }

    public List<Cell> getCells() {
        return ImmutableList.copyOf(cellRepository.getCells());
    }

    private void clearLevelIfPresent() {
        if (characterRepository.getCharacterList() != null) {
            characterRepository.getCharacterList().clear();
        }
        if (cellRepository.getCells() != null) {
            cellRepository.getCells().clear();
        }
        triggerManager.clearTriggers();
        gameStateCache.setGameState(GameState.MAIN_MENU);
    }
}
