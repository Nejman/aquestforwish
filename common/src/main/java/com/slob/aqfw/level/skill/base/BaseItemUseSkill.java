package com.slob.aqfw.level.skill.base;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.level.skill.item.ItemRepository;

public class BaseItemUseSkill extends Skill {
    private Injector injector;
    private ItemRepository itemRepository;

    public BaseItemUseSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        BaseItemUseSkill skill = new BaseItemUseSkill(descriptor, character);
        skill.injector = injector;
        skill.itemRepository = injector.getInstance(ItemRepository.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        Item item = itemRepository.getCharacterItemMap().get(getUser());
        if (item != null) {
            item.use(getUser(), targetedCell, injector);
            setCurrentDelay(1);
        }
    }

    @Override
    public int getRange() {
        Item item = itemRepository.getCharacterItemMap().get(getUser());
        if (item != null) {
            return item.getRange();
        }
        return 0;
    }

    @Override
    public boolean isPassive() {
        Item item = itemRepository.getCharacterItemMap().get(getUser());
        if (item != null) {
            return item.isPassive();
        }
        return false;
    }

}
