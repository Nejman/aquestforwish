package com.slob.aqfw.level.skill.item;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.entities.characters.CharacterDamageManager;

public class ForearmBladeItem extends Item {
    @Override
    public void use(Entity character, Cell targetCell, Injector injector) {
        if (targetCell.getCharacter().isPresent()) {
            Entity target = targetCell.getCharacter().get();
            CharacterDamageManager characterDamageManager = injector.getInstance(CharacterDamageManager.class);
            Effect effect = new Effect(EffectType.HEALTH_CHANGE, -1);
            characterDamageManager.changeCharacterHealth(effect, target, null);
        }
    }

}
