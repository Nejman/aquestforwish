package com.slob.aqfw.level.skill.heroes.wizard;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.level.aura.AuraFactory;

public class HailSkill extends Skill {
    private AuraFactory auraFactory;

    private HailSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        HailSkill skill = new HailSkill(descriptor, character);
        skill.auraFactory = injector.getInstance(AuraFactory.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        auraFactory.createAura(AuraFactory.HAIL, targetedCell);
    }
}
