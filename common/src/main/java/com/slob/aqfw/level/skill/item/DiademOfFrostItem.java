package com.slob.aqfw.level.skill.item;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.core.skill.PointType;

public class DiademOfFrostItem extends Item {
    @Override
    public void use(Entity character, Cell targetCell, Injector injector) {
        if (targetCell.getCharacter().isPresent()) {
            Entity target = targetCell.getCharacter().get();
            for (PointType pointType : PointType.values()) {
                Effect effect = new Effect(pointType.getCorrespondingEffectType(), -100);
                effect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, 2);
                target.getActiveEffects().add(effect);
            }
        }
    }

    @Override
    public int getRange() {
        return 3;
    }
}
