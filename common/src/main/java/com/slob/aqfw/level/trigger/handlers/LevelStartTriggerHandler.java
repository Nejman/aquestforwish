package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.level.trigger.Trigger;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;

public class LevelStartTriggerHandler implements TriggerHandler {
    private final GameStateCache gameStateCache;

    @Inject
    public LevelStartTriggerHandler(GameStateCache gameStateCache) {
        this.gameStateCache = gameStateCache;
    }

    @Override
    public boolean isMet(Trigger trigger) {
        return gameStateCache.getGameState() == GameState.START_LEVEL;
    }
}
