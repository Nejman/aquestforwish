package com.slob.aqfw.level.hex;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.slob.aqfw.core.cell.Cell;

import java.util.List;

@Singleton
public class CellRepository {
    private List<Cell> cells;

    @Inject
    public CellRepository() {
        //empty guice constructor
    }

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }
}
