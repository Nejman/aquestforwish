package com.slob.aqfw.level.effect;

import com.google.inject.Inject;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.entities.characters.CharacterRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class EffectManager {
    private final CharacterRepository characterRepository;
    private final CharacterDamageManager damageManager;

    @Inject
    public EffectManager(CharacterRepository characterRepository, CharacterDamageManager damageManager) {
        this.characterRepository = characterRepository;
        this.damageManager = damageManager;
    }

    public void manageEffectsAtEndOfTurn() {
        applyDamageEffects();
        for (Character character : characterRepository.getCharacterList()) {
            character.getActiveEffects().stream()
                    .filter(effect -> effect.getAdditionalValues().containsKey(EffectAdditionalValueType.DURATION))
                    .forEach(effect -> effect.getAdditionalValues().replace(EffectAdditionalValueType.DURATION,
                            (Integer) effect.getAdditionalValues().get(EffectAdditionalValueType.DURATION) - 1));
            LinkedList<Effect> endedEffects = character.getActiveEffects().stream()
                    .filter(effect -> effect.getAdditionalValues().containsKey(EffectAdditionalValueType.DURATION))
                    .filter(effect -> (Integer) effect.getAdditionalValues().get(EffectAdditionalValueType.DURATION) <= 0)
                    .collect(Collectors.toCollection(LinkedList::new));
            character.getActiveEffects().removeAll(endedEffects);
        }
    }

    private void applyDamageEffects() {
        for (Character character : characterRepository.getCharacterList()) {
            List<Effect> effects = character.getActiveEffects().stream()
                    .filter(effect -> effect.getEffectType() == EffectType.HEALTH_CHANGE)
                    .collect(Collectors.toList());
            for (Effect effect : effects) {
                damageManager.changeCharacterHealth(effect, character, null);
            }
        }
    }
}
