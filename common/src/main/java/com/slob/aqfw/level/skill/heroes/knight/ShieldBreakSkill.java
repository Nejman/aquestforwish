package com.slob.aqfw.level.skill.heroes.knight;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;

public class ShieldBreakSkill extends Skill {
    private CharacterDamageManager healthManager;
    private CellHelper cellHelper;

    private ShieldBreakSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        ShieldBreakSkill skill = new ShieldBreakSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        skill.cellHelper = injector.getInstance(CellHelper.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        if (targetedCell.getCharacter().isPresent()) {
            Entity target = targetedCell.getCharacter().get();
            Effect damageEffect = new Effect(EffectType.BASE_ATTACK, 0);
            Effect armourEffect = new Effect(EffectType.ARMOR_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
            armourEffect.getAdditionalValues().put(EffectAdditionalValueType.SIDE, cellHelper.getAttackedSide(target, getUser()).get());
            healthManager.changeCharacterHealth(damageEffect, target, getUser());
            target.getActiveEffects().add(armourEffect);
        }
    }
}
