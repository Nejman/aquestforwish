package com.slob.aqfw.level.skill.eneimies.troll;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterAttackManager;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;

public class TrollAttackSkill extends Skill {
    private CharacterDamageManager healthManager;
    private CellHelper cellHelper;

    private TrollAttackSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        TrollAttackSkill skill = new TrollAttackSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        skill.cellHelper = injector.getInstance(CellHelper.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        int attackValue = CharacterAttackManager.getAttackValue(getUser());
        Effect effect = new Effect(EffectType.BASE_ATTACK, attackValue);
        if (targetedCell.getCharacter().isPresent()) {
            healthManager.changeCharacterHealth(effect, targetedCell.getCharacter().get(), getUser());
        }
        Cell userCell = cellHelper.getCharacterCell(getUser());
        for (Cell cell : targetedCell.getNeighbours().values()) {
            if (cell.getNeighbours().values().contains(userCell)) {
                if (cell.getCharacter().isPresent()) {
                    healthManager.changeCharacterHealth(effect, cell.getCharacter().get(), getUser());
                }
            }
        }
    }
}
