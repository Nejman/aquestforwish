package com.slob.aqfw.level.skill.item;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.entities.characters.Character;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Singleton
public class ItemRepository {
    private final Map<Character, Item> characterItemMap = new HashMap<>();
    private final List<Item> unassignedItems = new LinkedList<>();

    @Inject
    ItemRepository() {
        //empty guice constructor
    }

    public List<Item> getUnassignedItems() {
        return unassignedItems;
    }

    public Map<Character, Item> getCharacterItemMap() {
        return characterItemMap;
    }
}
