package com.slob.aqfw.level.aura;

import com.google.inject.Singleton;

import java.util.LinkedList;
import java.util.List;

@Singleton
public class AuraRepository {
    private List<Aura> auras = new LinkedList<>();

    public List<Aura> getAuras() {
        return auras;
    }
}
