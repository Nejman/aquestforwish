package com.slob.aqfw.level.skill;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.LinkedList;
import java.util.List;

@Singleton
public class SkillHistory {
    private final List<SkillUseResult> resultHistory = new LinkedList<>();

    @Inject
    SkillHistory() {
        //empty guice constructor
    }

    public List<SkillUseResult> getResultHistory() {
        return resultHistory;
    }
}
