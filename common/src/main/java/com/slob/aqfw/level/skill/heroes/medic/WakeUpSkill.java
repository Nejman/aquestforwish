package com.slob.aqfw.level.skill.heroes.medic;

import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterRepository;
import com.slob.aqfw.entities.characters.UnconciousCharacter;

import java.util.List;

public class WakeUpSkill extends Skill {
    private CharacterFactory characterFactory;
    private CharacterRepository characterRepository;

    private WakeUpSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        WakeUpSkill skill = new WakeUpSkill(descriptor, character);
        skill.characterFactory = injector.getInstance(CharacterFactory.class);
        skill.characterRepository = injector.getInstance(CharacterRepository.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        if (targetedCell.getCharacter().isPresent()) {
            UnconciousCharacter unconciousCharacter = (UnconciousCharacter) targetedCell.getCharacter().get();
            String characterId = unconciousCharacter.getDeadCharacterId();
            characterId = characterId.substring(0, characterId.indexOf(CharacterFactory.ID_DELIMITER));
            Character character = characterFactory.createCharacter(characterId);
            targetedCell.setCharacter(character);
            characterRepository.getCharacterList().remove(unconciousCharacter);
            characterRepository.getCharacterList().add(character);
        }
    }

    @Override
    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        ImmutableList.Builder<Cell> result = ImmutableList.builder();

        for (Cell cell : originalCells) {
            if (cell.getCharacter().isPresent()) {
                Entity character = cell.getCharacter().get();
                if (character.getId().startsWith(UnconciousCharacter.UNCONCIOUS_CHARACTTER_ID_PREFIX)) {
                    result.add(cell);
                }
            }
        }

        return result.build();
    }
}
