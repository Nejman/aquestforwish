package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.entities.characters.CharacterRepository;
import com.slob.aqfw.level.trigger.Trigger;

import java.util.LinkedList;
import java.util.List;

public class SurviveSpecificTriggerHandler implements TriggerHandler {
    private final CharacterRepository characterRepository;

    @Inject
    public SurviveSpecificTriggerHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean isMet(Trigger trigger) {
        if (trigger.getElapsed() >= trigger.getTime()) {
            List<String> livingCharacters = new LinkedList<>();
            characterRepository.getCharacterList().forEach(character -> livingCharacters.add(character.getId()));
            return livingCharacters.containsAll(trigger.getCharactersId());
        }
        return false;
    }
}
