package com.slob.aqfw.level.skill.heroes.beast;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;

public class IgnoreObstaclesSkill extends Skill {
    public IgnoreObstaclesSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        return new IgnoreObstaclesSkill(descriptor, character);
    }

    @Override
    public void apply(Cell targetedCell) {
        Effect ignoreObstacleEffect = new Effect(EffectType.IGNORE_OBSTACLE, 0);
        ignoreObstacleEffect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, getIntegerValue(SkillValueType.DURATION));
        getUser().getActiveEffects().add(ignoreObstacleEffect);
    }
}
