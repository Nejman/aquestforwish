package com.slob.aqfw.level.skill.heroes.ranger;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;

public class HarpoonSkill extends Skill {
    private CharacterDamageManager healthManager;
    private CellHelper cellHelper;

    public HarpoonSkill(SkillDescriptor descriptor, Entity user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        HarpoonSkill skill = new HarpoonSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        skill.cellHelper = injector.getInstance(CellHelper.class);
        return skill;
    }

    @Override
    public void apply(Cell cell) {

    }
}
