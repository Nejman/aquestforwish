package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.level.trigger.Trigger;

public class GoToZoneAnyTriggerHandler implements TriggerHandler {

    @Inject
    public GoToZoneAnyTriggerHandler() {
        //empty guice constructor
    }

    @Override
    public boolean isMet(Trigger trigger) {
        for (Cell cell : trigger.getCells()) {
            if (cell.getCharacter().isPresent() && trigger.getCharactersId().contains(cell.getCharacter().get().getId())) {
                return true;
            }
        }
        return false;
    }
}
