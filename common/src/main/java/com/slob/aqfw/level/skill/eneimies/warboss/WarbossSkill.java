package com.slob.aqfw.level.skill.eneimies.warboss;

import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.PointType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterPointManager;
import com.slob.aqfw.service.LogicService;

import java.util.List;

public class WarbossSkill extends Skill {
    private static final ImmutableList<String> CHARACTERS_IDS = ImmutableList.of(CharacterFactory.ORK_ID, CharacterFactory.GOBLIN_WARRIOR_ID, CharacterFactory.GOBLIN_WARRIOR_ID, CharacterFactory.GOBLIN_ARCHER_ID);
    private CharacterFactory characterFactory;
    private LogicService logicService;

    private WarbossSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        WarbossSkill skill = new WarbossSkill(descriptor, character);
        skill.characterFactory = injector.getInstance(CharacterFactory.class);
        skill.logicService = injector.getInstance(LogicService.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        for (String id : CHARACTERS_IDS) { // TODO change to descriptor based
            Character character = characterFactory.createCharacter(id);
            exhaustCharacter(character);
            Cell emptyCell = findNextEmpty(targetedCell);
            logicService.getLevelService().addCharacter(character, emptyCell.getX(), emptyCell.getY());
        }
        setCurrentDelay(getDelay());
    }


    @Override
    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        ImmutableList.Builder<Cell> builder = ImmutableList.builder();

        originalCells.stream()
                .filter(cell -> getCellFreeNeighbourCount(cell) >= 4)
                .forEach(builder::add);

        return builder.build();
    }

    private void exhaustCharacter(Character character) {
        CharacterPointManager.usePoints(1, PointType.ATTACK, character);
        CharacterPointManager.usePoints(1, PointType.MOVEMENT, character);
        CharacterPointManager.usePoints(1, PointType.ACTION, character);
    }

    private Cell findNextEmpty(Cell cell) {
        Cell result = null;
        if (!cell.getCharacter().isPresent()) {
            result = cell;
        } else {
            for (Side side : Side.values()) {
                Cell neighbour = cell.getNeighbours().get(side);
                if (!neighbour.getCharacter().isPresent()) {
                    result = neighbour;
                    break;
                }
            }
        }
        return result;
    }

    private int getCellFreeNeighbourCount(Cell cell) {
        int result = 0;

        for (Cell neighbour : cell.getNeighbours().values()) {
            if (!neighbour.getCharacter().isPresent()) {
                result++;
            }
        }
        if (!cell.getCharacter().isPresent()) {
            result++;
        }

        return result;
    }
}
