package com.slob.aqfw.level.skill.eneimies.ork;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.level.skill.SkillTargetingHelper;

import java.util.List;

public class OrkPassiveSkill extends Skill {
    private SkillTargetingHelper skillTargetingHelper;

    private OrkPassiveSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        OrkPassiveSkill skill = new OrkPassiveSkill(descriptor, character);
        skill.skillTargetingHelper = injector.getInstance(SkillTargetingHelper.class);
        return skill;
    }


    @Override
    public void apply(Cell targetedCell) {
        List<Cell> cells = skillTargetingHelper.getAllowedTargetList(this);
        for (Cell cell : cells) {
            if (cell.getCharacter().isPresent()) {
                Entity entity = cell.getCharacter().get();
                if (entity instanceof Character) {
                    Character character = (Character) entity;
                    if (character.getAlignment() == ((Character) getUser()).getAlignment()) {
                        Effect effect = new Effect(EffectType.ACTION_POINT_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
                        effect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, 1);
                        cell.getCharacter().get().getActiveEffects().add(effect);
                    }
                }
            }
        }
    }
}
