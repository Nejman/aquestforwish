package com.slob.aqfw.level.skill.heroes.medic;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;

public class RegenerationGrenadeSkill extends Skill {
    private CharacterDamageManager characterDamageManager;

    private RegenerationGrenadeSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        RegenerationGrenadeSkill skill = new RegenerationGrenadeSkill(descriptor, character);
        skill.characterDamageManager = injector.getInstance(CharacterDamageManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        targetedCell.getCharacter().ifPresent(this::healCharacter);
        for (Cell cell : targetedCell.getNeighbours().values()) {
            cell.getCharacter().ifPresent(this::healCharacter);
        }
    }

    private void healCharacter(Entity character) {
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
        characterDamageManager.changeCharacterHealth(effect, character, null);
    }
}
