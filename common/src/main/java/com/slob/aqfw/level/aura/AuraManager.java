package com.slob.aqfw.level.aura;

import com.google.inject.Inject;

import java.util.List;
import java.util.stream.Collectors;

public class AuraManager {
    private final AuraRepository auraRepository;

    @Inject
    public AuraManager(AuraRepository auraRepository) {
        this.auraRepository = auraRepository;
    }

    public void update() {
        applyAuras();
        auraRepository.getAuras().forEach(Aura::decreaseTime);
        List<Aura> finishedAuras = auraRepository.getAuras().stream().filter(a -> a.getRemainingTime() == 0).collect(Collectors.toList());
        auraRepository.getAuras().removeAll(finishedAuras);
    }

    private void applyAuras() {
        auraRepository.getAuras().forEach(Aura::apply);
    }
}
