package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.level.trigger.result.TriggerResult;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;

public class WinResultResultHandler implements ResultHandler {
    private final GameStateCache stateCache;

    @Inject
    public WinResultResultHandler(GameStateCache stateCache) {
        this.stateCache = stateCache;
    }

    @Override
    public void handle(TriggerResult triggerResult) {
        stateCache.setGameState(GameState.WON_LEVEL);
    }
}
