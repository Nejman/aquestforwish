package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterAlignment;
import com.slob.aqfw.entities.characters.CharacterRepository;
import com.slob.aqfw.level.trigger.Trigger;


public class KillAllTriggerHandler implements TriggerHandler {
    private final CharacterRepository characterRepository;

    @Inject
    KillAllTriggerHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean isMet(Trigger trigger) {
        boolean result = true;
        for (Character character : characterRepository.getCharacterList()) {
            if (character.getAlignment() == CharacterAlignment.EVIL) {
                result = false;
                break;
            }
        }
        return result;
    }
}
