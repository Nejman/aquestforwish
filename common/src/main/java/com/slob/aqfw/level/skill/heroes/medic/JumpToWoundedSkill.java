package com.slob.aqfw.level.skill.heroes.medic;

import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterMovementManager;

import java.util.List;

public class JumpToWoundedSkill extends Skill {
    private CharacterMovementManager movementManager;

    private JumpToWoundedSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        JumpToWoundedSkill skill = new JumpToWoundedSkill(descriptor, character);
        skill.movementManager = injector.getInstance(CharacterMovementManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        movementManager.teleportCharacter(getUser(), targetedCell);
    }

    @Override
    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        ImmutableList.Builder<Cell> result = ImmutableList.builder();

        for (Cell cell : originalCells) {
            if (!cell.getCharacter().isPresent()) {
                if (isNearHurtCharacter(cell)) {
                    result.add(cell);
                }
            }
        }

        return result.build();
    }

    private boolean isNearHurtCharacter(Cell cell) {
        for (Side side : Side.values()) {
            Cell neighbour = cell.getNeighbours().get(side);
            if (neighbour != null && neighbour.getCharacter().isPresent()) {
                Entity character = neighbour.getCharacter().get();
                if (character.getCurrentHealth() < character.getDefaultMaxHealth()) {
                    return true;
                }
            }
        }
        return false;
    }
}