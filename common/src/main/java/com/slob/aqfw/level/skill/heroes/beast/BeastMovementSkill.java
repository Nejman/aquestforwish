package com.slob.aqfw.level.skill.heroes.beast;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.*;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.List;

public class BeastMovementSkill extends Skill {
    private CharacterMovementManager movementManager;
    private CharacterDamageManager damageManager;
    private CellHelper cellHelper;

    private BeastMovementSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        BeastMovementSkill skill = new BeastMovementSkill(descriptor, character);
        skill.movementManager = injector.getInstance(CharacterMovementManager.class);
        skill.cellHelper = injector.getInstance(CellHelper.class);
        skill.damageManager = injector.getInstance(CharacterDamageManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        Cell cell = cellHelper.getCharacterCell(getUser());
        List<Cell> path = movementManager.getPath(getRange(), getUser(), cell, targetedCell);

        if (isEnPassantOn()) {
            Effect effect = new Effect(EffectType.BASE_ATTACK, 0);
            for (Cell cellOnPath : path) {
                movementManager.moveCharacter(getUser(), cellOnPath);
                for (Cell neighbour : cellOnPath.getNeighbours().values()) {
                    if (neighbour.getCharacter().isPresent()) {
                        if (neighbour.getCharacter().get() instanceof Character) {
                            Character target = (Character) neighbour.getCharacter().get();
                            if (target.getAlignment() == CharacterAlignment.EVIL) {
                                damageManager.changeCharacterHealth(effect, target, getUser());
                            }
                        }
                    }
                }
            }
        } else {
            movementManager.moveCharacter(getUser(), targetedCell);
        }

    }

    @Override
    public int getRange() {
        return CharacterSpeedManager.getCharacterSpeed(getUser());
    }

    private boolean isEnPassantOn() {
        return getUser().getActiveEffects().stream()
                .anyMatch(effect -> effect.getEffectType() == EffectType.EN_PASSANT);
    }

    @Override
    public boolean isMovementSkill() {
        return true;
    }
}
