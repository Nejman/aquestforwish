package com.slob.aqfw.level.aura;

import com.google.common.collect.ImmutableList;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.AuraDescriptor;
import com.slob.aqfw.entities.characters.CharacterAlignment;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class Aura {
    protected final CharacterAlignment alignment;
    protected final Cell center;
    protected final int range;
    protected final int value;
    protected int remainingTime;

    public Aura(AuraDescriptor auraDescriptor, Cell center) {
        this.alignment = CharacterAlignment.getByCode(auraDescriptor.getAlignment());
        this.center = center;
        this.range = auraDescriptor.getRange();
        this.remainingTime = auraDescriptor.getDuration();
        this.value = auraDescriptor.getValue();
    }

    public abstract void apply();

    int getRemainingTime() {
        return remainingTime;
    }

    void decreaseTime() {
        if(remainingTime != -1){
            remainingTime--;
        }
    }

    public List<Cell> getAffectedCells() {
        Set<Cell> cellSet = new HashSet<>();
        cellSet.add(center);
        for (int range = 0; range < this.range; range++) {
            Set<Cell> temporarySet = new HashSet<>();
            for (Cell cell : cellSet) {
                for (Side side : Side.values()) {
                    Cell neighbourSide = cell.getNeighbours().get(side);
                    if (neighbourSide != null) {
                        temporarySet.add(neighbourSide);
                    }
                }
            }
            cellSet.addAll(temporarySet);
        }
        return ImmutableList.copyOf(cellSet);

    }

}
