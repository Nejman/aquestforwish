package com.slob.aqfw.level.skill.heroes.knight;

import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.List;

public class ChargeSkill extends Skill {
    private CharacterDamageManager healthManager;
    private CellHelper cellHelper;

    private ChargeSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        ChargeSkill skill = new ChargeSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        skill.cellHelper = injector.getInstance(CellHelper.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        Cell characterCell = cellHelper.getCharacterCell(getUser());
        Side chargeSide = cellHelper.getSideOfDistantCell(characterCell, targetedCell).get();
        damageAllTargetsOnPath(characterCell.getNeighbours().get(chargeSide), chargeSide);
        moveTarget(characterCell.getNeighbours().get(chargeSide), chargeSide);
        moveTarget(characterCell, chargeSide);
    }

    @Override
    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        ImmutableList.Builder<Cell> result = ImmutableList.builder();
        Cell characterCell = cellHelper.getCharacterCell(getUser());
        characterCell.getNeighbours().values().forEach(result::add);

        return result.build();
    }

    private void damageAllTargetsOnPath(Cell currentCell, Side side) {
        boolean keepGoing = true;
        while (currentCell != null && keepGoing) {
            if (currentCell.getCharacter().isPresent()) {
                Entity entity = currentCell.getCharacter().get();
                Effect effect = new Effect(EffectType.HEALTH_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
                healthManager.changeCharacterHealth(effect, entity, null);
                if (entity.getCurrentHealth() > 0) {
                    keepGoing = false;
                }
            }
            currentCell = currentCell.getNeighbours().get(side);
        }
    }

    private void moveTarget(Cell currentCell, Side side) {
        Entity entity = null;
        Cell cell = currentCell;
        boolean keepGoing = true;
        while (keepGoing) {
            if (cell.getCharacter().isPresent() && cell.getCharacter().get().getCurrentHealth() > 0) {
                entity = cell.getCharacter().get();
            }
            if (entity != null || cell.getNeighbours().get(side) == null) {
                keepGoing = false;
            } else {
                cell = cell.getNeighbours().get(side);
            }
        }
        if (entity != null) {
            Cell targetCell = findLastUnoccupiedCell(cell, side);
            cellHelper.getCharacterCell(entity).setCharacter(null);
            targetCell.setCharacter(entity);
        }
    }

    private Cell findLastUnoccupiedCell(Cell currentCell, Side side) {
        Cell result = currentCell;
        boolean keepGoing = true;
        while (keepGoing) {
            Cell cell = result.getNeighbours().get(side);
            if (cell != null && (!cell.getCharacter().isPresent() || cell.getCharacter().get().getCurrentHealth() <= 0)) {
                result = cell;
            } else {
                keepGoing = false;
            }
        }
        return result;
    }
}
