package com.slob.aqfw.level.trigger.result;

import com.google.inject.Inject;
import com.slob.aqfw.core.trigger.TriggerResultType;
import com.slob.aqfw.level.trigger.result.handlers.*;

import java.util.HashMap;
import java.util.Map;

public class TriggerResultHandler {
    private final AddAurasResultResultHandler addAurasResultResultHandler;
    private final AddTriggersResultResultHandler addTriggersResultResultHandler;
    private final LoseResultResultHandler loseResultResultHandler;
    private final SpawnCharactersResultResultHandler spawnCharactersResultResultHandler;
    private final WinResultResultHandler winResultResultHandler;
    private final DialogResultResultHandler dialogResultResultHandler;
    private final Map<TriggerResultType, ResultHandler> resultHandlerMap = new HashMap<>();

    @Inject
    TriggerResultHandler(AddAurasResultResultHandler addAurasResultResultHandler, AddTriggersResultResultHandler addTriggersResultResultHandler, LoseResultResultHandler loseResultResultHandler, SpawnCharactersResultResultHandler spawnCharactersResultResultHandler, WinResultResultHandler winResultResultHandler, DialogResultResultHandler dialogResultResultHandler) {
        this.addAurasResultResultHandler = addAurasResultResultHandler;
        this.addTriggersResultResultHandler = addTriggersResultResultHandler;
        this.loseResultResultHandler = loseResultResultHandler;
        this.spawnCharactersResultResultHandler = spawnCharactersResultResultHandler;
        this.winResultResultHandler = winResultResultHandler;
        this.dialogResultResultHandler = dialogResultResultHandler;
        init();
    }

    private void init() {
        resultHandlerMap.put(TriggerResultType.ADD_AURAS, addAurasResultResultHandler);
        resultHandlerMap.put(TriggerResultType.ADD_TRIGGERS, addTriggersResultResultHandler);
        resultHandlerMap.put(TriggerResultType.LOSE, loseResultResultHandler);
        resultHandlerMap.put(TriggerResultType.SPAWN_CHARACTERS, spawnCharactersResultResultHandler);
        resultHandlerMap.put(TriggerResultType.WIN, winResultResultHandler);
        resultHandlerMap.put(TriggerResultType.DIALOG, dialogResultResultHandler);
    }

    public void handleResult(TriggerResult triggerResult) {
        ResultHandler resultHandler = resultHandlerMap.get(triggerResult.getResultType());
        resultHandler.handle(triggerResult);
    }
}
