package com.slob.aqfw.level.aura.skillauras;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.AuraDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.aura.Aura;

import java.util.List;

public class FireballAura extends Aura {
    private CharacterDamageManager characterDamageManager;

    private FireballAura(AuraDescriptor descriptor, Cell center) {
        super(descriptor, center);
    }

    public static FireballAura getInstance(AuraDescriptor descriptor, Injector injector, Cell center) {
        FireballAura fireballAura = new FireballAura(descriptor, center);
        fireballAura.characterDamageManager = injector.getInstance(CharacterDamageManager.class);
        return fireballAura;
    }

    @Override
    public void apply() {
        if (remainingTime == 1) {
            List<Cell> affectedCells = getAffectedCells();
            for (Cell cell : affectedCells) {
                if (cell.getCharacter().isPresent()) {
                    Effect effect = new Effect(EffectType.HEALTH_CHANGE, value);
                    characterDamageManager.changeCharacterHealth(effect, cell.getCharacter().get(), null);
                }
            }
        }
    }
}
