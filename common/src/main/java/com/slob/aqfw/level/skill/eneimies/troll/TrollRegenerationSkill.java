package com.slob.aqfw.level.skill.eneimies.troll;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;

public class TrollRegenerationSkill extends Skill {
    private CharacterDamageManager healthManager;

    private TrollRegenerationSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        TrollRegenerationSkill skill = new TrollRegenerationSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        return skill;
    }


    @Override
    public void apply(Cell targetedCell) {
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
        healthManager.changeCharacterHealth(effect, getUser(), null);
    }
}
