package com.slob.aqfw.level.skill;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.Optional;

public class KnockbackManager {
    private final CellHelper cellHelper;
    private final CharacterDamageManager damageManager;

    @Inject
    public KnockbackManager(CellHelper cellHelper, CharacterDamageManager damageManager) {
        this.cellHelper = cellHelper;
        this.damageManager = damageManager;
    }

    public void knockback(Entity target, Cell source) {
        Cell targetCell = cellHelper.getCharacterCell(target);
        Optional<Side> attackedSide;
        if (targetCell.getNeighbours().containsValue(source)) {
            attackedSide = cellHelper.getAttackedSide(source.getCharacter().get(), target);
        } else {
            attackedSide = cellHelper.getSideOfDistantCell(source, targetCell);
        }
        if (attackedSide.isPresent()) {
            Side side = attackedSide.get();
            if (targetCell.getNeighbours().containsKey(side)) {
                Cell destinationCell = targetCell.getNeighbours().get(side);
                if (destinationCell.getCharacter().isPresent()) {
                    Effect damageEffect = new Effect(EffectType.HEALTH_CHANGE, -2);
                    damageManager.changeCharacterHealth(damageEffect, target, null);
                    damageManager.changeCharacterHealth(damageEffect, destinationCell.getCharacter().get(), null);
                } else {
                    targetCell.setCharacter(null);
                    destinationCell.setCharacter(target);
                }
            }
        }
    }
}
