package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.entities.characters.CharacterRepository;
import com.slob.aqfw.level.trigger.Trigger;

public class SurviveAnyTriggerHandler implements TriggerHandler {
    private final CharacterRepository characterRepository;

    @Inject
    public SurviveAnyTriggerHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean isMet(Trigger trigger) {
        if (trigger.getElapsed() >= trigger.getTime()) {
            return characterRepository.getCharacterList().stream()
                    .anyMatch(character -> trigger.getCharactersId().contains(character.getId()));
        }
        return false;
    }
}
