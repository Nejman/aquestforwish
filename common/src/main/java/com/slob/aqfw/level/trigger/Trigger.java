package com.slob.aqfw.level.trigger;

import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.trigger.TriggerType;
import com.slob.aqfw.level.trigger.result.TriggerResult;

import java.util.List;

public class Trigger {
    private final String id;
    private final boolean shouldDisplay;
    private final List<Cell> cells;
    private final List<String> charactersId;
    private final int time;
    private final TriggerType triggerType;
    private final List<TriggerResult> triggerResults;
    private int elapsed;

    public Trigger(String id, boolean shouldDisplay, List<Cell> cells, List<String> charactersId, int time, int elapsed, TriggerType triggerType, List<TriggerResult> triggerResults) {
        this.id = id;
        this.shouldDisplay = shouldDisplay;
        this.cells = cells;
        this.charactersId = charactersId;
        this.time = time;
        this.elapsed = elapsed;
        this.triggerType = triggerType;
        this.triggerResults = triggerResults;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public List<String> getCharactersId() {
        return charactersId;
    }

    public int getTime() {
        return time;
    }

    public int getElapsed() {
        return elapsed;
    }

    public void setElapsed(int elapsed) {
        this.elapsed = elapsed;
    }

    public TriggerType getTriggerType() {
        return triggerType;
    }

    public List<TriggerResult> getTriggerResults() {
        return triggerResults;
    }

    public boolean isShouldDisplay() {
        return shouldDisplay;
    }

    public String getId() {
        return id;
    }
}
