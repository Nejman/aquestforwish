package com.slob.aqfw.level.trigger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.LinkedList;
import java.util.List;

@Singleton
public class TriggerRepository {
    private final List<Trigger> triggers = new LinkedList<>();

    @Inject
    TriggerRepository() {
        //empty guice constructor
    }

    public List<Trigger> getTriggers() {
        return triggers;
    }
}
