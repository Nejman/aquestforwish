package com.slob.aqfw.level.trigger.result;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.ResultDescriptor;
import com.slob.aqfw.core.trigger.TriggerResultType;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.List;
import java.util.Optional;

public class TriggerResultFactory {
    private final DescriptorReader descriptorReader;
    private final CellHelper cellHelper;

    @Inject
    TriggerResultFactory(DescriptorReader descriptorReader, CellHelper cellHelper) {
        this.descriptorReader = descriptorReader;
        this.cellHelper = cellHelper;
    }

    public TriggerResult createTriggerResult(String id) {
        ResultDescriptor descriptor = descriptorReader.getById(ResultDescriptor.class, id);
        return new TriggerResult(descriptor.getId(), descriptor.getIds(), getCells(descriptor), TriggerResultType.getResultTypeByCode(descriptor.getResultType()));
    }


    private List<Cell> getCells(ResultDescriptor descriptor) {
        ImmutableList.Builder<Cell> result = ImmutableList.builder();
        if (descriptor.getCell() != null) {
            for (ResultDescriptor.CellDescriptor cellDescriptor : descriptor.getCell()) {
                Optional<Cell> cell = cellHelper.getCellAtCoordinates(cellDescriptor.x, cellDescriptor.y);
                cell.ifPresent(result::add);
            }
        }

        return result.build();
    }
}
