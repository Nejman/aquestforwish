package com.slob.aqfw.level.skill.heroes.ranger;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;

public class PinDownSkill extends Skill {
    private CharacterDamageManager healthManager;

    private PinDownSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        PinDownSkill skill = new PinDownSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        if (targetedCell.getCharacter().isPresent()) {
            Effect damageEffect = new Effect(EffectType.BASE_ATTACK, 0);
            Entity target = targetedCell.getCharacter().get();
            healthManager.changeCharacterHealth(damageEffect, target, getUser());
            Effect pinningEffect = new Effect(EffectType.SPEED_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
            pinningEffect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, getIntegerValue(SkillValueType.DURATION));
            target.getActiveEffects().add(pinningEffect);
        }
    }
}
