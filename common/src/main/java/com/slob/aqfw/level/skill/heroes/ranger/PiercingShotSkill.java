package com.slob.aqfw.level.skill.heroes.ranger;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.Optional;

public class PiercingShotSkill extends Skill {
    private CharacterDamageManager healthManager;
    private CellHelper cellHelper;

    private PiercingShotSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        PiercingShotSkill skill = new PiercingShotSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        skill.cellHelper = injector.getInstance(CellHelper.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        Cell characterCell = cellHelper.getCharacterCell(getUser());
        Optional<Side> sideOptional = cellHelper.getSideOfDistantCell(characterCell, targetedCell);
        if (sideOptional.isPresent()) {
            Side side = sideOptional.get();
            int distance = 1;
            Cell cell = characterCell.getNeighbours().get(side);
            while (cell != null && distance < getRange()) {
                cell.getCharacter().ifPresent(this::damageCharacter);
                cell = cell.getNeighbours().get(side);
                distance++;
            }
        }
    }

    private void damageCharacter(Entity character) {
        Effect effect = new Effect(EffectType.BASE_ATTACK, 0);
        healthManager.changeCharacterHealth(effect, character, getUser());
    }
}
