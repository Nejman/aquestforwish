package com.slob.aqfw.level.trigger.result;

import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.trigger.TriggerResultType;

import java.util.List;

public class TriggerResult {
    private final String id;
    private final List<String> ids;
    private final List<Cell> cells;
    private final TriggerResultType resultType;

    public TriggerResult(String id, List<String> ids, List<Cell> cells, TriggerResultType resultType) {
        this.id = id;
        this.ids = ids;
        this.cells = cells;
        this.resultType = resultType;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public List<String> getIds() {
        return ids;
    }

    public String getId() {
        return id;
    }

    public TriggerResultType getResultType() {
        return resultType;
    }
}
