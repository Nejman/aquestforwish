package com.slob.aqfw.level;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.LevelDescriptor;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterManager;
import com.slob.aqfw.level.hex.CellRepository;
import com.slob.aqfw.level.trigger.Trigger;
import com.slob.aqfw.level.trigger.TriggerFactory;
import com.slob.aqfw.level.trigger.TriggerRepository;

import java.util.List;

public class LevelFactory {
    private final DescriptorReader reader;
    private final CellRepository cellRepository;
    private final LevelCache levelCache;
    private final CharacterFactory factory;
    private final CharacterManager characterManager;
    private final TriggerFactory triggerFactory;
    private final TriggerRepository triggerRepository;

    @Inject
    LevelFactory(DescriptorReader reader, CellRepository cellRepository, LevelCache levelCache, CharacterFactory factory, CharacterManager characterManager, TriggerFactory triggerFactory, TriggerRepository triggerRepository) {
        this.reader = reader;
        this.cellRepository = cellRepository;
        this.levelCache = levelCache;
        this.factory = factory;
        this.characterManager = characterManager;
        this.triggerFactory = triggerFactory;
        this.triggerRepository = triggerRepository;
    }

    public void generateLevel(String levelId) {
        LevelDescriptor descriptor = reader.getById(LevelDescriptor.class, levelId);
        levelCache.setCurrentLevelId(descriptor.getId());
        levelCache.setNextLevelId(descriptor.getNextLevelId());
        levelCache.setCampDialogId(descriptor.getCampDialogId());
        cellRepository.setCells(HexCreator.createHexField(descriptor.getSize()));
        addEntities(descriptor.getEntities());
        addTriggers(descriptor.getTriggers());
    }

    private void addEntities(List<LevelDescriptor.EntityDescriptor> descriptors) {
        if (descriptors != null) {
            for (LevelDescriptor.EntityDescriptor descriptor : descriptors) {
                Character character = factory.createCharacter(descriptor.id, descriptor.uniqueId);
                characterManager.addCharacter(character, descriptor.cell.x, descriptor.cell.y);
            }
        }
    }

    private void addTriggers(List<LevelDescriptor.TriggerDescriptor> descriptors) {
        if (descriptors != null) {
            for (LevelDescriptor.TriggerDescriptor descriptor : descriptors) {
                Trigger trigger = triggerFactory.createTrigger(descriptor.id);
                triggerRepository.getTriggers().add(trigger);
            }
        }
    }
}
