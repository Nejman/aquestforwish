package com.slob.aqfw.level.skill.heroes.wizard;

import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterMovementManager;

import java.util.LinkedList;
import java.util.List;

public class TeleportSkill extends Skill {
    private CharacterMovementManager movementManager;

    private TeleportSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        TeleportSkill skill = new TeleportSkill(descriptor, character);
        skill.movementManager = injector.getInstance(CharacterMovementManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        movementManager.teleportCharacter(getUser(), targetedCell);
    }

    @Override
    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        List<Cell> result = new LinkedList<>();

        for (Cell cell : originalCells) {
            if (!cell.getCharacter().isPresent()) {
                result.add(cell);
            }
        }
        return ImmutableList.copyOf(result);
    }

}
