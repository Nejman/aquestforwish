package com.slob.aqfw.level.skill;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.level.skill.base.BaseAttackSkill;
import com.slob.aqfw.level.skill.base.BaseItemUseSkill;
import com.slob.aqfw.level.skill.base.BaseMovementSkill;
import com.slob.aqfw.level.skill.base.BaseRangedAttackSkill;
import com.slob.aqfw.level.skill.eneimies.ork.OrkPassiveSkill;
import com.slob.aqfw.level.skill.eneimies.shaman.Fireball;
import com.slob.aqfw.level.skill.eneimies.troll.TrollAttackSkill;
import com.slob.aqfw.level.skill.eneimies.troll.TrollRegenerationSkill;
import com.slob.aqfw.level.skill.eneimies.warboss.WarbossSkill;
import com.slob.aqfw.level.skill.heroes.beast.*;
import com.slob.aqfw.level.skill.heroes.knight.ChargeSkill;
import com.slob.aqfw.level.skill.heroes.knight.ShieldBreakSkill;
import com.slob.aqfw.level.skill.heroes.knight.TauntSkill;
import com.slob.aqfw.level.skill.heroes.knight.WideStrikeSkill;
import com.slob.aqfw.level.skill.heroes.medic.*;
import com.slob.aqfw.level.skill.heroes.ranger.JumpOutSkill;
import com.slob.aqfw.level.skill.heroes.ranger.PiercingShotSkill;
import com.slob.aqfw.level.skill.heroes.ranger.PinDownSkill;
import com.slob.aqfw.level.skill.heroes.wizard.*;


public class SkillFactory {
    private static final String BASE_ATTACK_ID = "BASE_ATTACK";
    private static final String BASE_RANGED_ATTACK_ID = "BASE_RANGED_ATTACK";
    public static final String BASE_MOVEMENT_SKILL = "BASE_MOVEMENT_SKILL";

    private static final String ORK_PASSIVE_SKILL_ID = "ORK_PASSIVE_SKILL";
    private static final String TROLL_ATTACK_ID = "TROLL_ATTACK";
    private static final String TROLL_REGENERATION_ID = "TROLL_REGENERATION";
    private static final String WARBOSS_CALL_ID = "WARBOSS_CALL";
    private static final String FIREBALL = "FIREBALL";

    private static final String ITEM_USE_SKILL = "ITEM_USE_SKILL";

    private static final String KNIGHT_CHARGE = "KNIGHT_CHARGE";
    private static final String KNIGHT_SHIELD_BREAKER = "KNIGHT_SHIELD_BREAKER";
    private static final String KNIGHT_TAUNT = "KNIGHT_TAUNT";
    private static final String KNIGHT_WIDE_STRIKE = "KNIGHT_WIDE_STRIKE";

    private static final String HEALING = "HEALING";
    private static final String HEAL_JUMP_TO_WOUNDED = "JUMP_TO_WOUNDED";
    private static final String KNOCKOUT = "KNOCKOUT";
    private static final String REGENERATION_GRANADE = "REGENERATION_GRANADE";
    private static final String WAKE_UP = "WAKE_UP";

    private static final String PIERCING_SHOT = "PIERCING_SHOT";
    private static final String PINNING_SHOT = "PINNING_SHOT";
    private static final String JUMPOUT = "JUMPOUT";

    private static final String WIZARD_ATTACK = "WIZARD_ATTACK";
    private static final String HAIL = "HAIL";
    private static final String TELEPORT = "TELEPORT";
    private static final String CHAIN_LIGHTNING = "CHAIN_LIGHTNING";
    private static final String DETONATION = "DETONATION";

    private static final String BEAST_MOVEMENT_SKILL = "BEAST_MOVEMENT_SKILL";
    private static final String EN_PASSANT = "EN_PASSANT";
    private static final String DIRECTED_STRIKE = "DIRECTED_STRIKE";
    private static final String BERSERK = "BERSERK";
    private static final String IGNORE_OBSTACLE = "IGNORE_OBSTACLE";


    private final Injector injector;
    private final DescriptorReader descriptorReader;

    @Inject
    SkillFactory(Injector injector, DescriptorReader descriptorReader) {
        this.injector = injector;
        this.descriptorReader = descriptorReader;
    }

    public Skill getSkillById(String id, Character character) {
        SkillDescriptor descriptor = descriptorReader.getById(SkillDescriptor.class, id);
        switch (id) {
            case BASE_ATTACK_ID:
                return BaseAttackSkill.getInstance(character, injector, descriptor);
            case BASE_RANGED_ATTACK_ID:
                return BaseRangedAttackSkill.getInstance(character, injector, descriptor);
            case BASE_MOVEMENT_SKILL:
                return BaseMovementSkill.getInstance(character, injector, descriptor);
            case ORK_PASSIVE_SKILL_ID:
                return OrkPassiveSkill.getInstance(character, injector, descriptor);
            case TROLL_ATTACK_ID:
                return TrollAttackSkill.getInstance(character, injector, descriptor);
            case TROLL_REGENERATION_ID:
                return TrollRegenerationSkill.getInstance(character, injector, descriptor);
            case WARBOSS_CALL_ID:
                return WarbossSkill.getInstance(character, injector, descriptor);
            case FIREBALL:
                return Fireball.getInstance(character, injector, descriptor);
            case KNIGHT_CHARGE:
                return ChargeSkill.getInstance(character, injector, descriptor);
            case KNIGHT_SHIELD_BREAKER:
                return ShieldBreakSkill.getInstance(character, injector, descriptor);
            case KNIGHT_TAUNT:
                return TauntSkill.getInstance(character, injector, descriptor);
            case KNIGHT_WIDE_STRIKE:
                return WideStrikeSkill.getInstance(character, injector, descriptor);
            case HEALING:
                return HealingSkill.getInstance(character, injector, descriptor);
            case HEAL_JUMP_TO_WOUNDED:
                return JumpToWoundedSkill.getInstance(character, injector, descriptor);
            case ITEM_USE_SKILL:
                return BaseItemUseSkill.getInstance(character, injector, descriptor);
            case KNOCKOUT:
                return KnockoutSkill.getInstance(character, injector, descriptor);
            case REGENERATION_GRANADE:
                return RegenerationGrenadeSkill.getInstance(character, injector, descriptor);
            case PIERCING_SHOT:
                return PiercingShotSkill.getInstance(character, injector, descriptor);
            case PINNING_SHOT:
                return PinDownSkill.getInstance(character, injector, descriptor);
            case JUMPOUT:
                return JumpOutSkill.getInstance(character, injector, descriptor);
            case HAIL:
                return HailSkill.getInstance(character, injector, descriptor);
            case TELEPORT:
                return TeleportSkill.getInstance(character, injector, descriptor);
            case CHAIN_LIGHTNING:
                return ChainLightningSkill.getInstance(character, injector, descriptor);
            case WAKE_UP:
                return WakeUpSkill.getInstance(character, injector, descriptor);
            case DETONATION:
                return DetonationSkill.getInstance(character, injector, descriptor);
            case BEAST_MOVEMENT_SKILL:
                return BeastMovementSkill.getInstance(character, injector, descriptor);
            case EN_PASSANT:
                return EnPassantSkill.getInstance(character, injector, descriptor);
            case DIRECTED_STRIKE:
                return DirectedStrikeSkill.getInstance(character, injector, descriptor);
            case BERSERK:
                return BerserkSkill.getInstance(character, injector, descriptor);
            case IGNORE_OBSTACLE:
                return IgnoreObstaclesSkill.getInstance(character, injector, descriptor);
            case WIZARD_ATTACK:
                return WizardAttackSkill.getInstance(character, injector, descriptor);
            default:
                throw new IllegalArgumentException("Unknown skill id: " + id);
        }
    }
}
