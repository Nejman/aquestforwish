package com.slob.aqfw.level.trigger;

import com.google.inject.Inject;
import com.slob.aqfw.core.trigger.TriggerType;
import com.slob.aqfw.level.trigger.handlers.*;
import com.slob.aqfw.level.trigger.result.TriggerResultHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TriggerManager {
    private final GoToZoneAllTriggerHandler goToZoneAllTriggerHandler;
    private final GoToZoneAnyTriggerHandler goToZoneAnyTriggerHandler;
    private final KillAllTriggerHandler killAllTriggerHandler;
    private final KillSpecificTriggerHandler killSpecificTriggerHandler;
    private final SurviveAnyTriggerHandler surviveAnyTriggerHandler;
    private final SurviveSpecificTriggerHandler surviveSpecificTriggerHandler;
    private final LevelStartTriggerHandler levelStartTriggerHandler;
    private final TriggerRepository triggerRepository;
    private final TriggerResultHandler resultHandler;
    private final Map<TriggerType, TriggerHandler> typeToHandlerMap = new HashMap<>();

    @Inject
    TriggerManager(GoToZoneAllTriggerHandler goToZoneAllTriggerHandler, GoToZoneAnyTriggerHandler goToZoneAnyTriggerHandler, KillAllTriggerHandler killAllTriggerHandler, KillSpecificTriggerHandler killSpecificTriggerHandler, SurviveAnyTriggerHandler surviveAnyTriggerHandler, SurviveSpecificTriggerHandler surviveSpecificTriggerHandler, LevelStartTriggerHandler levelStartTriggerHandler, TriggerRepository triggerRepository, TriggerResultHandler resultHandler) {
        this.goToZoneAllTriggerHandler = goToZoneAllTriggerHandler;
        this.goToZoneAnyTriggerHandler = goToZoneAnyTriggerHandler;
        this.killAllTriggerHandler = killAllTriggerHandler;
        this.killSpecificTriggerHandler = killSpecificTriggerHandler;
        this.surviveAnyTriggerHandler = surviveAnyTriggerHandler;
        this.surviveSpecificTriggerHandler = surviveSpecificTriggerHandler;
        this.levelStartTriggerHandler = levelStartTriggerHandler;
        this.triggerRepository = triggerRepository;
        this.resultHandler = resultHandler;
        initMap();
    }

    private void initMap() {
        typeToHandlerMap.put(TriggerType.GO_TO_ZONE_ALL, goToZoneAllTriggerHandler);
        typeToHandlerMap.put(TriggerType.GO_TO_ZONE_ANY, goToZoneAnyTriggerHandler);
        typeToHandlerMap.put(TriggerType.KILL_ALL, killAllTriggerHandler);
        typeToHandlerMap.put(TriggerType.KILL_SPECIFIC, killSpecificTriggerHandler);
        typeToHandlerMap.put(TriggerType.SURVIVE_ANY, surviveAnyTriggerHandler);
        typeToHandlerMap.put(TriggerType.SURVIVE_SPECIFIC, surviveSpecificTriggerHandler);
        typeToHandlerMap.put(TriggerType.LEVEL_START, levelStartTriggerHandler);
    }

    public void handleTriggers() {
        List<Trigger> finishedTriggers = new LinkedList<>();
        for (Trigger trigger : triggerRepository.getTriggers()) {
            if (typeToHandlerMap.get(trigger.getTriggerType()).isMet(trigger)) {
                trigger.getTriggerResults().forEach(resultHandler::handleResult);
                finishedTriggers.add(trigger);
            }
        }
        triggerRepository.getTriggers().removeAll(finishedTriggers);
    }

    public void updateOnEndOfTurn() {
        triggerRepository.getTriggers().forEach(objective -> objective.setElapsed(objective.getElapsed() + 1));
    }

    public void clearTriggers() {
        if (triggerRepository.getTriggers() != null) {
            triggerRepository.getTriggers().clear();
        }
    }
}
