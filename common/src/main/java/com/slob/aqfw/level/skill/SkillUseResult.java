package com.slob.aqfw.level.skill;

public class SkillUseResult {
    private final String userId;
    private final String skillId;
    private final int targetCellX;
    private final int targetCellY;

    public SkillUseResult(String userId, String skillId, int targetCellX, int targetCellY) {
        this.userId = userId;
        this.skillId = skillId;
        this.targetCellX = targetCellX;
        this.targetCellY = targetCellY;
    }

    public String getUserId() {
        return userId;
    }

    public String getSkillId() {
        return skillId;
    }

    public int getTargetCellX() {
        return targetCellX;
    }

    public int getTargetCellY() {
        return targetCellY;
    }
}
