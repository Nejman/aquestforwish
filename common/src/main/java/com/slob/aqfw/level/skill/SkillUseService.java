package com.slob.aqfw.level.skill;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterManager;
import com.slob.aqfw.entities.characters.CharacterPointManager;
import com.slob.aqfw.entities.characters.CharacterRepository;
import com.slob.aqfw.level.ai.CellThreatLevelManager;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.Optional;

public class SkillUseService {
    private final CharacterManager characterManager;
    private final CellThreatLevelManager threatLevelManager;
    private final SkillHistory skillHistory;
    private final CharacterRepository characterRepository;
    private final CellHelper cellHelper;

    @Inject
    SkillUseService(CharacterManager characterManager, CellThreatLevelManager threatLevelManager, SkillHistory skillHistory, CharacterRepository characterRepository, CellHelper cellHelper) {
        this.characterManager = characterManager;
        this.threatLevelManager = threatLevelManager;
        this.skillHistory = skillHistory;
        this.characterRepository = characterRepository;
        this.cellHelper = cellHelper;
    }


    public SkillUseResult performSkill(Skill skill, Cell target) {
        skill.apply(target);
        CharacterPointManager.usePoints(skill.getRequiredPoints(), skill.getUser());
        characterManager.updateCharactersLivingStatus();
        threatLevelManager.updateCellsThreatLevels();
        SkillUseResult result = new SkillUseResult(skill.getUser().getId(), skill.getId(), target.getX(), target.getY());
        skillHistory.getResultHistory().add(result);
        return result;
    }

    public void performAllFromHistory() {
        ImmutableList<SkillUseResult> skillHistoryList = ImmutableList.copyOf(skillHistory.getResultHistory());
        this.skillHistory.getResultHistory().clear();
        skillHistoryList.forEach(this::handleSkillFromHistory);
    }


    private void handleSkillFromHistory(SkillUseResult useResult) {
        Optional<Character> characterOptional = characterRepository.getCharacterList().stream()
                .filter(character -> character.getId().equals(useResult.getUserId()))
                .findFirst();
        if (characterOptional.isPresent()) {
            Character character = characterOptional.get();
            Optional<Skill> optionalSkill = character.getSkills().stream()
                    .filter(skill -> skill.getId().equals(useResult.getSkillId()))
                    .findFirst();
            if (optionalSkill.isPresent()) {
                Cell cell = cellHelper.getCellAtCoordinates(useResult.getTargetCellX(), useResult.getTargetCellY()).get();
                performSkill(optionalSkill.get(), cell);
            }
        }
    }
}
