package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.dialog.DialogService;
import com.slob.aqfw.level.trigger.result.TriggerResult;

public class DialogResultResultHandler implements ResultHandler {
    private final DialogService service;

    @Inject
    public DialogResultResultHandler(DialogService service) {
        this.service = service;
    }

    @Override
    public void handle(TriggerResult triggerResult) {
        triggerResult.getIds().forEach(service::loadDialog);
    }
}
