package com.slob.aqfw.level.hex;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.entities.Entity;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class CellHelper {
    private CellRepository repository;

    @Inject
    public CellHelper(CellRepository repository) {
        this.repository = repository;
    }

    public Optional<Cell> getCellAtCoordinates(int x, int y) {
        return repository.getCells().stream()
                .filter(cell -> cell.getX() == x)
                .filter(cell -> cell.getY() == y)
                .findFirst();
    }

    public Optional<Side> getSideForCharacters(Entity target, Entity attacker) {
        List<Cell> cells = repository.getCells();
        Optional<Cell> targetCell = cells.stream()
                .filter(cell -> cell.getCharacter().isPresent())
                .filter(cell -> target.equals(cell.getCharacter().get()))
                .findFirst();
        Optional<Cell> attackerCell = cells.stream()
                .filter(cell -> cell.getCharacter().isPresent())
                .filter(cell -> attacker.equals(cell.getCharacter().get()))
                .findFirst();
        if(targetCell.isPresent() && attackerCell.isPresent()){
            if (targetCell.get().getNeighbours().containsValue(attackerCell.get())) {
                return getSideOfNeighbour(targetCell.get(), attackerCell.get());
            } else {
                return getSideOfDistantCell(targetCell.get(), attackerCell.get());
            }
        }
        return Optional.empty();
    }

    public Optional<Side> getSideOfDistantCell(Cell cell, Cell target) {
        for (Side side : Side.values()) {
            Cell betweenCell = cell;
            while (betweenCell != null) {
                betweenCell = betweenCell.getNeighbours().get(side);
                if (target.equals(betweenCell)) {
                    return Optional.of(side);
                }
            }
        }
        return Optional.empty();
    }

    Optional<Side> getSideOfNeighbour(Cell cell, Cell neighbour) {
        for(Side side : cell.getNeighbours().keySet()){
            if(cell.getNeighbours().get(side).equals(neighbour)){
                return Optional.of(side);
            }
        }
        return Optional.empty();
    }

    public Cell getCharacterCell(Entity character) {
        return repository.getCells().stream()
                .filter(cell -> cell.getCharacter().isPresent())
                .filter(cell -> character.equals(cell.getCharacter().get()))
                .findFirst().orElseGet(new Supplier<Cell>() {
                    @Override
                    public Cell get() {
                        return null;
                    }
                }); //if there is no cell for this character, something is really wrong, so exception is bound to be thrown
    }


    public Optional<Side> getAttackedSide(Entity target, Entity attacker) {
        if (attacker != null) {
            Optional<Side> side = getSideForCharacters(target, attacker);
            if (side.isPresent()) {
                Side attackedSide = side.get();
                int sideCode = attackedSide.getSideCode();
                sideCode = (sideCode - target.getRotation() + 6) % 6;
                return Optional.of(Side.getSideForCode(sideCode));
            }
            return side;
        }
        return Optional.empty();
    }
}
