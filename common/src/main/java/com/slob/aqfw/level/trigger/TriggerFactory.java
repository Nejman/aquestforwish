package com.slob.aqfw.level.trigger;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.TriggerDescriptor;
import com.slob.aqfw.core.trigger.TriggerType;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.trigger.result.TriggerResult;
import com.slob.aqfw.level.trigger.result.TriggerResultFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TriggerFactory {
    private final DescriptorReader descriptorReader;
    private final CellHelper cellHelper;
    private final TriggerResultFactory triggerResultFactory;

    @Inject
    TriggerFactory(DescriptorReader descriptorReader, CellHelper cellHelper, TriggerResultFactory triggerResultFactory) {
        this.descriptorReader = descriptorReader;
        this.cellHelper = cellHelper;
        this.triggerResultFactory = triggerResultFactory;
    }

    public Trigger createTrigger(String id) {
        TriggerDescriptor descriptor = descriptorReader.getById(TriggerDescriptor.class, id);
        List<String> characterIds = new LinkedList<>();
        if (descriptor.getCharacterIds() != null) {
            characterIds = descriptor.getCharacterIds();
        }
        return new Trigger(descriptor.getId(), descriptor.getShouldDisplay(), getCells(descriptor), characterIds, descriptor.getTime(), 0, TriggerType.getTypeByCode(descriptor.getTriggerType()), getResults(descriptor));
    }

    private List<TriggerResult> getResults(TriggerDescriptor descriptor) {
        ImmutableList.Builder<TriggerResult> resultBuilder = ImmutableList.builder();
        if (descriptor.getResults() != null) {
            for (String id : descriptor.getResults()) {
                resultBuilder.add(triggerResultFactory.createTriggerResult(id));
            }
        }
        return resultBuilder.build();
    }

    private List<Cell> getCells(TriggerDescriptor descriptor) {
        ImmutableList.Builder<Cell> result = ImmutableList.builder();
        if (descriptor.getCell() != null) {
            for (TriggerDescriptor.CellDescriptor cellDescriptor : descriptor.getCell()) {
                Optional<Cell> cell = cellHelper.getCellAtCoordinates(cellDescriptor.x, cellDescriptor.y);
                cell.ifPresent(result::add);
            }
        }

        return result.build();
    }
}
