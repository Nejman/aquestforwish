package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Inject;
import com.slob.aqfw.level.trigger.result.TriggerResult;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;

public class LoseResultResultHandler implements ResultHandler {
    private final GameStateCache gameStateCache;

    @Inject
    public LoseResultResultHandler(GameStateCache gameStateCache) {
        this.gameStateCache = gameStateCache;
    }

    @Override
    public void handle(TriggerResult triggerResult) {
        gameStateCache.setGameState(GameState.LOST_LEVEL);
    }
}
