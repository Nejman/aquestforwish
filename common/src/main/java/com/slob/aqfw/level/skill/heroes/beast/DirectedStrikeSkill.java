package com.slob.aqfw.level.skill.heroes.beast;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;

public class DirectedStrikeSkill extends Skill {
    private DirectedStrikeSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        return new DirectedStrikeSkill(descriptor, character);
    }

    @Override
    public void apply(Cell targetedCell) {
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
        effect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, getIntegerValue(SkillValueType.DURATION));
        if (targetedCell.getCharacter().isPresent()) {
            targetedCell.getCharacter().get().getActiveEffects().add(effect);
        }
    }
}
