package com.slob.aqfw.level.skill.heroes.wizard;

import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;

import java.util.LinkedList;
import java.util.List;

public class DetonationSkill extends Skill {
    private CharacterDamageManager healthManager;

    private DetonationSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        DetonationSkill skill = new DetonationSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, getIntegerValue(SkillValueType.CHANGE_VALUE));
        if (targetedCell.getCharacter().isPresent()) {
            Entity target = targetedCell.getCharacter().get();
            healthManager.changeCharacterHealth(effect, target, null);
            if (target.getCurrentHealth() <= 0) {
                Effect secondaryEffect = new Effect(EffectType.HEALTH_CHANGE, getIntegerValue(SkillValueType.SECONDARY_DAMAGE_VALUE));
                for (Cell cell : targetedCell.getNeighbours().values()) {
                    if (cell.getCharacter().isPresent()) {
                        healthManager.changeCharacterHealth(secondaryEffect, cell.getCharacter().get(), null);
                    }
                }
            }
        }
    }


    @Override
    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        List<Cell> result = new LinkedList<>();

        for (Cell cell : originalCells) {
            if (cell.getCharacter().isPresent()) {
                result.add(cell);
            }
        }
        return ImmutableList.copyOf(result);
    }
}
