package com.slob.aqfw.level.skill.eneimies.boarrider;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.slob.aqfw.core.agony.Agony;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterRepository;

public class BoarRiderAgony extends Agony {

    @Inject
    public BoarRiderAgony(Injector injector) {
        super(injector);
    }

    @Override
    public void perform(Cell cell) {
        CharacterFactory orkFactory = injector.getInstance(CharacterFactory.class);
        Character ork = orkFactory.createCharacter("ORK_ID");
        injector.getInstance(CharacterRepository.class).getCharacterList().add(ork);
        cell.setCharacter(ork);
    }
}
