package com.slob.aqfw.level.skill.heroes.wizard;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterAttackManager;
import com.slob.aqfw.entities.characters.CharacterDamageManager;

import java.util.LinkedList;
import java.util.List;

public class ChainLightningSkill extends Skill {
    private CharacterDamageManager healthManager;

    private ChainLightningSkill(SkillDescriptor descriptor, Character user) {
        super(descriptor, user);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        ChainLightningSkill skill = new ChainLightningSkill(descriptor, character);
        skill.healthManager = injector.getInstance(CharacterDamageManager.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        List<Cell> targets = new LinkedList<>();
        targets.add(targetedCell);
        List<Cell> addedCells = getNextTargets(targets);
        while (!addedCells.isEmpty()) {
            targets.addAll(addedCells);
            addedCells = getNextTargets(targets);
        }
        targets.forEach(this::applyDamage);
    }

    private List<Cell> getNextTargets(List<Cell> targets) {
        List<Cell> result = new LinkedList<>();
        for (Cell currentTarget : targets) {
            for (Cell cell : currentTarget.getNeighbours().values()) {
                if (cell.getCharacter().isPresent() && !targets.contains(cell)) {
                    result.add(cell);
                }
            }
        }
        return result;
    }

    private void applyDamage(Cell cell) {
        if (cell.getCharacter().isPresent()) {
            Effect effect = new Effect(EffectType.HEALTH_CHANGE, -CharacterAttackManager.getAttackValue(getUser()));
            healthManager.changeCharacterHealth(effect, cell.getCharacter().get(), null);
        }
    }
}
