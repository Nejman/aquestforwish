package com.slob.aqfw.level;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class LevelCache {
    private String currentLevelId = "EMPTY_LEVEL"; // default test level id
    private String nextLevelId;
    private String campDialogId;

    @Inject
    LevelCache() {
        //empty guice constructor
    }

    public String getCurrentLevelId() {
        return currentLevelId;
    }

    public void setCurrentLevelId(String currentLevelId) {
        this.currentLevelId = currentLevelId;
    }

    public String getNextLevelId() {
        return nextLevelId;
    }

    public void setNextLevelId(String nextLevelId) {
        this.nextLevelId = nextLevelId;
    }

    public String getCampDialogId() {
        return campDialogId;
    }

    public void setCampDialogId(String campDialogId) {
        this.campDialogId = campDialogId;
    }
}
