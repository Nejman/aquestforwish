package com.slob.aqfw.level.agony;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.slob.aqfw.core.agony.Agony;
import com.slob.aqfw.level.skill.eneimies.boarrider.BoarRiderAgony;

public class AgonyFactory {
    private final Injector injector;

    @Inject
    AgonyFactory(Injector injector) {
        this.injector = injector;
    }

    public Agony getAgonyById(String id) {
        switch (id) {
            case "BOAR_AGONY":
                return injector.getInstance(BoarRiderAgony.class);
            default:
                throw new IllegalArgumentException("Unknown id: " + id);
        }
    }
}
