package com.slob.aqfw.level.skill.heroes.ranger;

import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillValueType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterAlignment;
import com.slob.aqfw.entities.characters.CharacterMovementManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;

import java.util.LinkedList;
import java.util.List;

public class JumpOutSkill extends Skill {
    private CharacterMovementManager movementManager;
    private SkillTargetingHelper targetingHelper;
    private CellHelper cellHelper;

    private JumpOutSkill(SkillDescriptor descriptor, Character character) {
        super(descriptor, character);
    }

    public static Skill getInstance(Character character, Injector injector, SkillDescriptor descriptor) {
        JumpOutSkill skill = new JumpOutSkill(descriptor, character);
        skill.movementManager = injector.getInstance(CharacterMovementManager.class);
        skill.targetingHelper = injector.getInstance(SkillTargetingHelper.class);
        skill.cellHelper = injector.getInstance(CellHelper.class);
        return skill;
    }

    @Override
    public void apply(Cell targetedCell) {
        movementManager.teleportCharacter(getUser(), targetedCell);
    }

    @Override
    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        List<Cell> result = new LinkedList<>();

        for (Cell cell : originalCells) {
            if (!cell.getCharacter().isPresent()) {
                if (!isNearEnemyCharacter(cell)) {
                    result.add(cell);
                }
            }
        }

        Cell characterCell = cellHelper.getCharacterCell(getUser());
        List<Cell> underMinimumTarget = targetingHelper.getAnywhereTargeting(characterCell, getIntegerValue(SkillValueType.MINIMUM_RANGE));
        result.removeAll(underMinimumTarget);

        return ImmutableList.copyOf(result);
    }

    private boolean isNearEnemyCharacter(Cell cell) {
        for (Side side : Side.values()) {
            Cell neighbour = cell.getNeighbours().get(side);
            if (neighbour != null && neighbour.getCharacter().isPresent() && neighbour.getCharacter().get() instanceof Character) {
                Character character = (Character) neighbour.getCharacter().get();
                if (character.getAlignment() == CharacterAlignment.EVIL) {
                    return true;
                }
            }
        }
        return false;
    }
}
