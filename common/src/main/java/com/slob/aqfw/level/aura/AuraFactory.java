package com.slob.aqfw.level.aura;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.descriptor.AuraDescriptor;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.level.aura.skillauras.FireballAura;
import com.slob.aqfw.level.aura.skillauras.HailAura;

public class AuraFactory {
    public static final String FIREBALL = "FIREBALL";
    public static final String HAIL = "HAIL";
    private final Injector injector;
    private final DescriptorReader descriptorReader;
    private final AuraRepository auraRepository;

    @Inject
    AuraFactory(Injector injector, DescriptorReader descriptorReader, AuraRepository auraRepository) {
        this.injector = injector;
        this.descriptorReader = descriptorReader;
        this.auraRepository = auraRepository;
    }

    public void createAura(String id, Cell cell) {
        AuraDescriptor descriptor = descriptorReader.getById(AuraDescriptor.class, id);
        Aura result;
        switch (id) {
            case FIREBALL:
                result = FireballAura.getInstance(descriptor, injector, cell);
                break;
            case HAIL:
                result = HailAura.getInstance(descriptor, injector, cell);
                break;
            default:
                throw new IllegalArgumentException("Unknown aura ID: " + id);
        }
        auraRepository.getAuras().add(result);
    }


}
