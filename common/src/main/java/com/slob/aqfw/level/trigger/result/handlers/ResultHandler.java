package com.slob.aqfw.level.trigger.result.handlers;

import com.slob.aqfw.level.trigger.result.TriggerResult;

public interface ResultHandler {
    void handle(TriggerResult triggerResult);
}
