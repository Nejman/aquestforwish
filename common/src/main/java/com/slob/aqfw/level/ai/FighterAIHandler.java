package com.slob.aqfw.level.ai;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.*;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.level.skill.SkillUseResult;
import com.slob.aqfw.level.skill.SkillUseService;

import java.util.*;

public class FighterAIHandler extends AI {

    @Inject
    FighterAIHandler(SkillTargetingHelper skillTargetingHelper, CharacterMovementManager characterMovementManager, CellHelper cellHelper, SkillUseService skillUseService) {
        super(skillUseService, skillTargetingHelper, characterMovementManager, cellHelper);
    }


    @Override
    public Optional<SkillUseResult> handleAI(Character character) {
        LinkedList<Skill> untargetableSkills = new LinkedList<>();
        Skill skill = getNextAvailableSkill(character, untargetableSkills);
        Cell characterCell = cellHelper.getCharacterCell(character);
        while (skill != null) {
            Cell targetCell;
            if (MOVEMENT_SKILL_IDS.contains(skill.getId())) {
                targetCell = getMovementCell(skill);
            } else {
                rotateCharacter(character, characterCell);
                targetCell = getAttackCell(skill);
            }
            if (targetCell == null) {
                untargetableSkills.add(skill);
                skill = getNextAvailableSkill(character, untargetableSkills);
            } else {
                SkillUseResult result = skillUseService.performSkill(skill, targetCell);
                return Optional.of(result);
            }
        }
        return Optional.empty();
    }

    private Skill getNextAvailableSkill(Character character, List<Skill> untargetableSkills) {
        Skill skill = null;
        for (Skill availableSkill : character.getSkills()) {
            if (!untargetableSkills.contains(availableSkill) && availableSkill.getCurrentDelay() == 0) {
                if (CharacterPointManager.hesCharacterEnoughPoints(availableSkill.getRequiredPoints(), character)) {
                    skill = availableSkill;
                    break;
                }
            }
        }
        return skill;
    }

    private Cell getAttackCell(Skill skill) {
        List<Cell> allowedTargetList = skillTargetingHelper.getAllowedTargetList(skill);
        Optional<Cell> cell1 = allowedTargetList.stream()
                .filter(cell -> cell.getCharacter().isPresent())
                .filter(cell -> cell.getCharacter().get() instanceof Character)
                .filter(cell -> ((Character) cell.getCharacter().get()).getAlignment() != ((Character) skill.getUser()).getAlignment())
                .max(Comparator.comparingInt(this::getThreatLevelOfCharacter));
        return cell1.orElse(null);

    }

    private int getThreatLevelOfCharacter(Cell cell) {
        return CharacterThreatManager.getThreatLevel(cell.getCharacter().get());
    }

    private Cell getMovementCell(Skill skill) {
        List<Cell> allowedTargetList = skillTargetingHelper.getAllowedTargetList(skill);
        List<Cell> results = getHighestThreatAvailableTarget(allowedTargetList, skill);

        if (results.isEmpty()) {
            results = getHighestThreatTarget(allowedTargetList);
        }

        if (results.isEmpty()) {
            return null;
        }

        Cell result = getNearestTarget(results, skill);
        if (result.getThreat() == 0) {
            return null;
        }
        return result;
    }

    private Cell getNearestTarget(List<Cell> targets, Skill skill) {
        Cell result = targets.get(0);
        Cell characterCell = cellHelper.getCharacterCell(skill.getUser());
        int characterSpeed = CharacterSpeedManager.getCharacterSpeed(skill.getUser());
        for (Cell cell : targets) {
            if (characterMovementManager.getPath(characterSpeed, skill.getUser(), characterCell, cell).size()
                    < characterMovementManager.getPath(characterSpeed, skill.getUser(), characterCell, result).size()) {
                result = cell;
            }
        }
        return result;
    }

    private List<Cell> getHighestThreatTarget(List<Cell> allowedTargetList) {
        List<Cell> results = new LinkedList<>();
        Cell result;
        result = allowedTargetList.get(0);
        results.add(result);
        for (Cell cell : allowedTargetList) {
            if (cell.getThreat() > result.getThreat()) {
                result = cell;
                results.clear();
                results.add(cell);
            } else if (cell.getThreat() == result.getThreat()) {
                results.add(cell);
            }
        }
        return results;
    }

    private List<Cell> getHighestThreatAvailableTarget(List<Cell> allowedTargetList, Skill skill) {
        List<Cell> results = new LinkedList<>();
        Cell result = null;
        for (Cell cell : allowedTargetList) {
            if (isNearEnemy(cell, skill)) {
                if (result != null) {
                    if (getThreatLevelOfNeighbourCharacter(cell) > getThreatLevelOfNeighbourCharacter(result)) {
                        result = cell;
                        results.clear();
                        results.add(cell);
                    } else if (getThreatLevelOfNeighbourCharacter(cell) == getThreatLevelOfNeighbourCharacter(result)) {
                        results.add(cell);
                    }
                } else {
                    result = cell;
                    results.add(cell);
                }
            }
        }
        return results;
    }

    private int getThreatLevelOfNeighbourCharacter(Cell cell) {
        for (Cell neighbour : cell.getNeighbours().values()) {
            if (neighbour.getCharacter().isPresent() && CharacterThreatManager.getThreatLevel(neighbour.getCharacter().get()) > 0) {
                return CharacterThreatManager.getThreatLevel(neighbour.getCharacter().get());
            }
        }
        return 0;
    }

    private boolean isNearEnemy(Cell cell, Skill skill) {
        for (Cell neighbour : cell.getNeighbours().values()) {
            if (neighbour.getCharacter().isPresent()) {
                Entity entity = neighbour.getCharacter().get();
                if (entity instanceof Character) {
                    Character character = (Character) entity;
                    if (character.getAlignment() != ((Character) skill.getUser()).getAlignment()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void rotateCharacter(Character character, Cell cell) {
        Map<Side, Cell> neighbours = cell.getNeighbours();
        List<Cell> highestThreatTarget = getHighestThreatTarget(ImmutableList.copyOf(neighbours.values()));
        if (!highestThreatTarget.contains(neighbours.get(Side.getSideForCode(character.getRotation())))) {
            Cell target = highestThreatTarget.get(0);
            for (Side side : neighbours.keySet()) {
                if (target.equals(neighbours.get(side))) {
                    character.setRotation(side.getSideCode());
                    break;
                }
            }
        }
    }
}
