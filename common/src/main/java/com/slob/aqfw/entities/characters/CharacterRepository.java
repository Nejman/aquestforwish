package com.slob.aqfw.entities.characters;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.LinkedList;
import java.util.List;

@Singleton
public class CharacterRepository {
    private final List<Character> characterList = new LinkedList<>();

    @Inject
    public CharacterRepository() {
        //empty guice constructor
    }

    public List<Character> getCharacterList() {
        return characterList;
    }

}
