package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.Optional;

public class CharacterManager {
    private final CharacterRepository repository;
    private final CellHelper cellHelper;

    @Inject
    CharacterManager(CharacterRepository repository, CellHelper cellHelper) {
        this.repository = repository;
        this.cellHelper = cellHelper;
    }

    public void updateCharactersLivingStatus() {
        ImmutableList.Builder<Character> charactersToDelete = ImmutableList.builder();
        for (Character character : ImmutableList.copyOf(repository.getCharacterList())) {
            if (isNotUnconscious(character) && character.getCurrentHealth() <= 0) {
                Cell characterCell = cellHelper.getCharacterCell(character);
                characterCell.setCharacter(null);
                character.getAgonyList().forEach(agony -> agony.perform(characterCell));
                charactersToDelete.add(character);
                if (character.getAlignment() == CharacterAlignment.GOOD) {
                    addCorpse(character, characterCell);
                }
            }
        }
        repository.getCharacterList().removeAll(charactersToDelete.build());
    }

    public void addCharacter(Character character, int x, int y) {
        Optional<Cell> cellOptional = cellHelper.getCellAtCoordinates(x, y);
        if (cellOptional.isPresent()) {
            repository.getCharacterList().add(character);
            cellOptional.get().setCharacter(character);
        }
    }

    private void addCorpse(Character character, Cell characterCell) {
        Character corpse = new UnconciousCharacter(character.getId());
        addCharacter(corpse, characterCell.getX(), characterCell.getY());
    }

    private boolean isNotUnconscious(Character character) {
        return !character.getId().startsWith(UnconciousCharacter.UNCONCIOUS_CHARACTTER_ID_PREFIX);
    }
}
