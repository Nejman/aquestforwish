package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;

public class CharacterAttackManager {
    public static int getAttackValue(Entity entity) {
        Character character = (Character) entity;
        int attack = character.getBaseAttackValue();
        int attackModificators = character.getActiveEffects().stream()
                .filter(effect -> effect.getEffectType().equals(EffectType.ATTACK_VALUE_CHANGE))
                .mapToInt(Effect::getValue)
                .sum();

        attack += attackModificators;
        if (attack < 0) {
            attack = 0;
        }
        return attack;
    }
}
