package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.UUID;

public class UnconciousCharacter extends Character {
    public static final String UNCONCIOUS_CHARACTTER_ID_PREFIX = "UNCONCIOUS_CHARACTER";
    private final String deadCharacterId;

    public UnconciousCharacter(String deadCharacterId) {
        super(UNCONCIOUS_CHARACTTER_ID_PREFIX + UUID.randomUUID().toString(), 0, ImmutableMap.of(), ImmutableMap.of(), CharacterAlignment.NEUTRAL, 0, 0, 0, null);
        setSkills(ImmutableList.of());
        this.deadCharacterId = deadCharacterId;
    }

    public String getDeadCharacterId() {
        return deadCharacterId;
    }
}
