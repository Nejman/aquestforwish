package com.slob.aqfw.entities.characters;

public enum CharacterAlignment {
    GOOD("GOOD"),
    EVIL("EVIL"),
    NEUTRAL("NEUTRAL");

    private final String code;

    CharacterAlignment(String code) {
        this.code = code;
    }

    public static CharacterAlignment getByCode(String code) {
        for (CharacterAlignment alignment : CharacterAlignment.values()) {
            if (alignment.code.equals(code)) {
                return alignment;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
