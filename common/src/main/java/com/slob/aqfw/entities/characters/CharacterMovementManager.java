package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.*;

public class CharacterMovementManager {
    private final CellHelper helper;

    @Inject
    public CharacterMovementManager(CellHelper helper) {
        this.helper = helper;
    }

    public List<Cell> getAllowedCellsForMovement(int allowedRange, Entity character) {
        Set<Cell> cellSet = new HashSet<>();
        Cell characterCell = helper.getCharacterCell(character);
        cellSet.add(characterCell);
        for (int range = 0; range < allowedRange; range++) {
            Set<Cell> temporarySet = new HashSet<>();
            for (Cell cell : cellSet) {
                for (Side side : Side.values()) {
                    Cell neighbourSide = cell.getNeighbours().get(side);
                    if (neighbourSide != null && !neighbourSide.getCharacter().isPresent()) {
                        temporarySet.add(neighbourSide);
                    }
                }
            }
            cellSet.addAll(temporarySet);
        }
        return ImmutableList.copyOf(cellSet);
    }

    public List<Cell> getPath(int range, Entity character, Cell start, Cell end) {
        List<Cell> result = new LinkedList<>();
        List<Cell> movableCells = getAllowedCellsForMovement(range, character);
        Map<Cell, Integer> distanceMap = new HashMap<>();
        movableCells.forEach(cell -> distanceMap.put(cell, Integer.MAX_VALUE));
        distanceMap.replace(end, 0);
        Set<Cell> workingCells = new HashSet<>();
        workingCells.add(end);
        while (distanceMap.get(start) == Integer.MAX_VALUE) {
            Set<Cell> passedCells = new HashSet<>();
            for (Cell cell : workingCells) {
                for (Cell neighbour : cell.getNeighbours().values()) {
                    if (distanceMap.containsKey(neighbour) && distanceMap.get(neighbour) > distanceMap.get(cell) + 1) {
                        distanceMap.replace(neighbour, distanceMap.get(cell) + 1);
                        passedCells.add(neighbour);
                    }
                }
            }
            workingCells = passedCells;
        }
        result.add(start);
        Cell lastCellOnPath = start;

        while (!result.contains(end)) {
            Cell cell = lastCellOnPath.getNeighbours().values().stream()
                    .filter(distanceMap::containsKey)
                    .min(Comparator.comparingInt(distanceMap::get))
                    .get();
            result.add(cell);
            lastCellOnPath = cell;
        }

        return ImmutableList.copyOf(result);
    }

    public void teleportCharacter(Entity character, Cell target) {
        Cell startCell = helper.getCharacterCell(character);
        if (!target.getCharacter().isPresent()) {
            startCell.setCharacter(null);
            target.setCharacter(character);
        }
    }

    public void moveCharacter(Entity entity, Cell target) {
        if (entity instanceof Character) {
            Character character = (Character) entity;
            if (!target.getCharacter().isPresent()) {
                Cell startCell = helper.getCharacterCell(character);
                int range = CharacterSpeedManager.getCharacterSpeed(character);
                List<Cell> allowedCells = getAllowedCellsForMovement(range, character);

                //NOTE: if needed, this can be expanded to make character pass through all cells on path
                if (character.getAlignment() == CharacterAlignment.GOOD) {
                    List<Cell> cellsOnPath = getPath(range, character, startCell, target);
                    for (Cell cell : cellsOnPath) {
                        if (cell.getPickable() != null) {
                            cell.getPickable().pickUp();
                        }
                    }
                }

                if (allowedCells.contains(target)) {
                    startCell.setCharacter(null);
                    target.setCharacter(character);
                }
            }
        }
    }

}
