package com.slob.aqfw.entities.characters;

import com.google.inject.Inject;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.level.hex.CellHelper;

import java.util.Optional;

public class CharacterDamageManager {
    private final CellHelper helper;

    @Inject
    public CharacterDamageManager(CellHelper helper) {
        this.helper = helper;
    }

    public void changeCharacterHealth(Effect effect, Entity target, Entity attacker) {
        int amount;
        if (attacker != null && effect.getEffectType() == EffectType.BASE_ATTACK) {
            amount = -CharacterAttackManager.getAttackValue(attacker);
        } else {
            amount = effect.getValue();
        }
        if(amount > 0){
            healCharacter(amount, target);
        } else {
            Optional<Side> attackedSide = helper.getAttackedSide(target, attacker);
            // Damage is described as negative amount in health change, for convenience it is change to positive values here
            if(attackedSide.isPresent()) {
                damageCharacter(-amount, target, attackedSide.get());
            } else {
                damageCharacterIndirectly(-amount, target);
            }
        }
    }

    private void healCharacter(int amount, Entity target) {
        int maxHealth = target.getDefaultMaxHealth();
        int currentHealth = target.getCurrentHealth();
        currentHealth += amount;
        if(currentHealth > maxHealth){
            currentHealth = maxHealth;
        }
        target.setCurrentHealth(currentHealth);
    }

    private void damageCharacterIndirectly(int damage, Entity target) {
        target.setCurrentHealth(target.getCurrentHealth() - damage);
    }

    private void damageCharacter(int damage, Entity target, Side attackedSide) {
        int armour = CharacterArmourManager.getArmourOnSide(target, attackedSide);
        damage = damage - armour;
        if(damage < 0){
            damage = 0;
        }
        target.setCurrentHealth(target.getCurrentHealth() - damage);
    }
}
