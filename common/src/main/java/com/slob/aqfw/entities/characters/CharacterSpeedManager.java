package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;

public class CharacterSpeedManager {
    public static int getCharacterSpeed(Entity entity) {
        if (entity instanceof Character) {
            Character character = (Character) entity;
            int speed = character.getDefaultSpeed();
            int speedModificators = character.getActiveEffects().stream()
                    .filter(effect -> effect.getEffectType().equals(EffectType.SPEED_CHANGE))
                    .mapToInt(Effect::getValue)
                    .sum();
            speed += speedModificators;
            if (speed < 0) {
                speed = 0;
            }
            return speed;
        }
        return 0;
    }

}
