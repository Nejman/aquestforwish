package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.PointType;

import java.util.Map;

public class CharacterPointManager {

    public static int getRemainingPoints(PointType pointType, Character character) {
        int remainingPoints = character.getMaxPoints().get(pointType);
        remainingPoints += character.getActiveEffects().stream()
                .filter(s -> s.getEffectType().equals(pointType.getCorrespondingEffectType()))
                .mapToInt(Effect::getValue)
                .sum();
        remainingPoints = remainingPoints - character.getUsedPoints().get(pointType);
        if (remainingPoints < 0) {
            remainingPoints = 0;
        }
        return remainingPoints;
    }

    public static void usePoints(Map<PointType, Integer> requiredPoints, Entity entity) {
        Character character = (Character) entity;
        for (PointType type : requiredPoints.keySet()) {
            usePoints(requiredPoints.get(type), type, character);
        }
    }

    public static void usePoints(int pointsUsed, PointType type, Character character) {
        int remainingPoints = getRemainingPoints(type, character);
        if (pointsUsed > 0) {
            if (remainingPoints >= pointsUsed) {
                character.getUsedPoints().put(type, character.getUsedPoints().get(type) + pointsUsed);
            } else if (type != PointType.ACTION) {
                int usedActionPoints = pointsUsed - remainingPoints;
                pointsUsed = remainingPoints;
                character.getUsedPoints().put(type, character.getUsedPoints().get(type) + pointsUsed);
                character.getUsedPoints().put(PointType.ACTION, character.getUsedPoints().get(PointType.ACTION) + usedActionPoints);
            }
        }
    }

    public static void resetAllPoints(Character character) {
        for (PointType type : PointType.values()) {
            resetPoints(type, character);
        }
    }

    private static void resetPoints(PointType type, Character character) {
        character.getUsedPoints().put(type, 0);
    }

    public static boolean hesCharacterEnoughPoints(Map<PointType, Integer> requiredPoints, Character character) {
        int remainingActionPoints = getRemainingPoints(PointType.ACTION, character);
        int actionPoints = 0;
        if (requiredPoints.containsKey(PointType.ACTION)) {
            actionPoints = remainingActionPoints - requiredPoints.get(PointType.ACTION);
        }
        int attackPoints = 0;
        if (requiredPoints.containsKey(PointType.ATTACK)) {
            attackPoints = getRemainingPoints(PointType.ATTACK, character) - requiredPoints.get(PointType.ATTACK);
        }
        int movementPoints = 0;
        if (requiredPoints.containsKey(PointType.MOVEMENT)) {
            movementPoints = getRemainingPoints(PointType.MOVEMENT, character) - requiredPoints.get(PointType.MOVEMENT);
        }
        if (actionPoints < 0) {
            return false;
        }
        if (movementPoints < 0) {
            if (remainingActionPoints + movementPoints < 0) {
                return false;
            } else {
                remainingActionPoints += movementPoints;
            }
        }

        if (attackPoints < 0 && remainingActionPoints + attackPoints < 0) {
            return false;
        }

        return true;
    }
}
