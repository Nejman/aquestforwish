package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableMap;
import com.slob.aqfw.core.agony.Agony;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.entities.Entity;
import com.slob.aqfw.core.skill.PointType;
import com.slob.aqfw.core.skill.Skill;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Character extends Entity {
    private final ImmutableMap<PointType, Integer> maxPoints;
    private final CharacterAlignment alignment;
    private final int defaultSpeed;
    private final int baseAttackValue;
    private final Map<PointType, Integer> usedPoints;
    private final int threatLevel;
    private final String aiHandlerName;
    private List<Skill> skills;

    public Character(String id, int defaultMaxHealth, ImmutableMap<PointType, Integer> maxPoints, ImmutableMap<Side, Integer> defaultArmour, CharacterAlignment alignment, int defaultSpeed, int baseAttackValue, int threatLevel, String aiHandlerName) {
        super(id, defaultMaxHealth, defaultArmour, new LinkedList<>(), 0, defaultMaxHealth, new LinkedList<>());
        this.maxPoints = maxPoints;
        this.alignment = alignment;
        this.defaultSpeed = defaultSpeed;
        this.baseAttackValue = baseAttackValue;
        this.threatLevel = threatLevel;
        this.aiHandlerName = aiHandlerName;
        this.usedPoints = new HashMap<>();
        for (PointType type : PointType.values()) {
            usedPoints.put(type, 0);
        }
    }

    public List<Effect> getActiveEffects() {
        return currentEffects;
    }

    public void addEffect(Effect effect){
        currentEffects.add(effect);
    }

    public void resetEffects(){
        currentEffects.clear();
    }

    public CharacterAlignment getAlignment() {
        return alignment;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public String getId() {
        return id;
    }

    public int getDefaultMaxHealth() {
        return defaultMaxHealth;
    }

    public int getRotation() {
        return rotation;
    }

    ImmutableMap<PointType, Integer> getMaxPoints() {
        return ImmutableMap.copyOf(maxPoints);
    }

    Map<PointType, Integer> getUsedPoints() {
        return usedPoints;
    }

    int getDefaultSpeed() {
        return defaultSpeed;
    }

    int getThreatLevel() {
        return threatLevel;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public int getBaseAttackValue() {
        return baseAttackValue;
    }

    public List<Agony> getAgonyList() {
        return agonyList;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public String getAiHandlerName() {
        return aiHandlerName;
    }
}
