package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.slob.aqfw.core.agony.Agony;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.CharacterDescriptor;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.skill.PointType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.level.agony.AgonyFactory;
import com.slob.aqfw.level.skill.SkillFactory;

import java.util.List;
import java.util.UUID;

public class CharacterFactory {
    public static final String ID_DELIMITER = "_!_";
    public static final String KNIGHT_ID = "KNIGHT_ID";
    public static final String BEAST_ID = "BEAST_ID";
    public static final String RANGER_ID = "RANGER_ID";
    public static final String MAGE_ID = "MAGE_ID";
    public static final String HEAL_ID = "HEAL_ID";
    public static final String GOBLIN_WARRIOR_ID = "GOBLIN_WARRIOR_ID";
    public static final String GOBLIN_ARCHER_ID = "GOBLIN_ARCHER_ID";
    public static final String SHAMAN_ID = "SHAMAN_ID";
    public static final String ORK_ID = "ORK_ID";
    public static final String BOAR_RIDER_ID = "BOAR_RIDER_ID";
    public static final String TROLL_ID = "TROLL_ID";
    public static final String ARMOURED_ORK_ID = "ARMOURED_ORK_ID";
    public static final String WARBOSS_ID = "WARBOSS_ID";


    protected Injector injector;
    private final DescriptorReader descriptorReader;
    private final SkillFactory skillFactory;
    private final AgonyFactory agonyFactory;

    @Inject
    protected CharacterFactory(Injector injector, DescriptorReader descriptorReader, SkillFactory skillFactory, AgonyFactory agonyFactory) {
        this.injector = injector;
        this.descriptorReader = descriptorReader;
        this.skillFactory = skillFactory;
        this.agonyFactory = agonyFactory;
    }

    public Character createCharacter(String id) {
        return createCharacter(id, id + ID_DELIMITER + UUID.randomUUID().toString());
    }

    public Character createCharacter(String descriptorId, String uniqueId) {
        CharacterDescriptor descriptor = descriptorReader.getById(CharacterDescriptor.class, descriptorId);
        Character character = new Character(uniqueId, descriptor.getDefaultMaxHealth(), getPoints(descriptor), getArmour(descriptor), CharacterAlignment.getByCode(descriptor.getAlignment()), descriptor.getDefaultSpeed(), descriptor.getBaseAttackValue(), descriptor.getThreatLevel(), descriptor.getAiHandler());
        character.setSkills(createSkills(character, descriptor));
        character.getAgonyList().addAll(createAgonies(descriptor));
        return character;
    }

    private List<Skill> createSkills(Character character, CharacterDescriptor descriptor) {
        ImmutableList.Builder<Skill> skillBuilder = ImmutableList.builder();
        for (String skillId : descriptor.getSkills()) {
            skillBuilder.add(skillFactory.getSkillById(skillId, character));
        }
        return skillBuilder.build();
    }

    private List<Agony> createAgonies(CharacterDescriptor descriptor) {
        ImmutableList.Builder<Agony> agonyBuilder = ImmutableList.builder();
        if (descriptor.getAgonyList() != null) {
            for (String agonyId : descriptor.getAgonyList()) {
                agonyBuilder.add(agonyFactory.getAgonyById(agonyId));
            }
        }
        return agonyBuilder.build();

    }

    private ImmutableMap<PointType, Integer> getPoints(CharacterDescriptor descriptor) {
        ImmutableMap.Builder<PointType, Integer> result = ImmutableMap.builder();
        if (descriptor.getMaxPoints() != null) {
            for (CharacterDescriptor.Point point : descriptor.getMaxPoints()) {
                result.put(PointType.getByCode(point.pointType), point.amount);
            }
        }

        return result.build();
    }

    private ImmutableMap<Side, Integer> getArmour(CharacterDescriptor descriptor) {
        ImmutableMap.Builder<Side, Integer> result = ImmutableMap.builder();
        if (descriptor.getDefaultArmour() != null) {
            for (CharacterDescriptor.Armour armour : descriptor.getDefaultArmour()) {
                result.put(Side.getSideForCode(armour.side), armour.amount);
            }
        }
        return result.build();
    }
}
