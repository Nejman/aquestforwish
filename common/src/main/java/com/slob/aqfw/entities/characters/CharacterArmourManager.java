package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;

public class CharacterArmourManager {
    public static int getArmourOnSide(Entity character, Side side) {
        int armour = 0;
        if (character.getDefaultArmourCopy().containsKey(side)) {
            armour = character.getDefaultArmourCopy().get(side);
        }
        int armourModificators = character.getActiveEffects().stream()
                .filter(effect -> effect.getEffectType() == EffectType.ARMOR_CHANGE)
                .filter(effect -> effect.getAdditionalValues().containsKey(EffectAdditionalValueType.SIDE))
                .filter(effect -> effect.getAdditionalValues().get(EffectAdditionalValueType.SIDE) == side)
                .mapToInt(Effect::getValue)
                .sum();
        return armour + armourModificators;
    }
}
