package com.slob.aqfw.entities.obstacles;

import com.google.common.collect.ImmutableMap;
import com.slob.aqfw.core.agony.Agony;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.entities.Entity;

import java.util.List;

public class Obstacle extends Entity {
    public Obstacle(String id, int defaultMaxHealth, ImmutableMap<Side, Integer> defaultArmour, List<Effect> currentEffects, int rotation, int currentHealth, List<Agony> agonyList) {
        super(id, defaultMaxHealth, defaultArmour, currentEffects, rotation, currentHealth, agonyList);
    }
}
