package com.slob.aqfw.entities.characters;

import com.google.inject.Inject;
import com.slob.aqfw.level.hex.CellHelper;

public class CharacterPassiveSkillManager {
    private final CharacterRepository characterRepository;
    private final CellHelper cellHelper;

    @Inject
    CharacterPassiveSkillManager(CharacterRepository characterRepository, CellHelper cellHelper) {
        this.characterRepository = characterRepository;
        this.cellHelper = cellHelper;
    }

    public void applyPassiveSkills() {
        characterRepository.getCharacterList().stream()
                .forEach(character -> character.getSkills().stream()
                        .filter(skill -> skill.isPassive())
                        .forEach(skill -> skill.apply(cellHelper.getCharacterCell(character))));
    }
}
