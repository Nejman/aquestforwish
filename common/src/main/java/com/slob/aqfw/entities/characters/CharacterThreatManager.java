package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.core.entities.Entity;

public class CharacterThreatManager {
    public static int getThreatLevel(Entity entity) {
        int value = 0;
        if (entity instanceof Character) {
            Character character = (Character) entity;
            int threat = character.getThreatLevel();
            int threatModificators = character.getActiveEffects().stream()
                    .filter(effect -> effect.getEffectType().equals(EffectType.THREAT_CHANGE))
                    .mapToInt(Effect::getValue)
                    .sum();
            value = threat + threatModificators;
        }
        return value;
    }
}
