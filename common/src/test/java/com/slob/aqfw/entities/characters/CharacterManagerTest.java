package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.hex.CellRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CharacterManagerTest {

    private CellHelper helper;
    private CharacterRepository repository;
    private CharacterManager manager;

    private static Character getDefaultCharacter() {
        return new Character("TEST", 0, null, null, null, 0, 5, 0, null);
    }

    @Before
    public void setUp() {
        repository = new CharacterRepository();
        CellRepository cellRepository = new CellRepository();
        cellRepository.setCells(HexCreator.createHexField(1));
        helper = new CellHelper(cellRepository);
        manager = new CharacterManager(repository, helper);
    }

    @Test
    public void characterDeletedWithHealthLessThanZero() {
        //given
        Character character = getDefaultCharacter();
        helper.getCellAtCoordinates(0, 0).get().setCharacter(character);
        repository.getCharacterList().add(character);
        character.setCurrentHealth(0);

        //when
        manager.updateCharactersLivingStatus();

        //then
        Assert.assertEquals(0, repository.getCharacterList().size());
        Assert.assertFalse(helper.getCellAtCoordinates(0, 0).get().getCharacter().isPresent());
    }

    @Test
    public void characterCorpseAddedWhenGoodAlignedCharacterDies() {
        //given
        Character character = new Character("TEST_ID", 0, null, null, CharacterAlignment.GOOD, 0, 5, 0, null);
        helper.getCellAtCoordinates(0, 0).get().setCharacter(character);
        repository.getCharacterList().add(character);
        character.setCurrentHealth(0);

        //when
        manager.updateCharactersLivingStatus();

        //then
        Assert.assertEquals(1, repository.getCharacterList().size());
        Assert.assertTrue(helper.getCellAtCoordinates(0, 0).get().getCharacter().isPresent());
        Assert.assertNotSame(character, helper.getCellAtCoordinates(0, 0).get().getCharacter().get());
        Assert.assertSame(character.getId(), ((UnconciousCharacter) helper.getCellAtCoordinates(0, 0).get().getCharacter().get()).getDeadCharacterId());
    }


    @Test
    public void characterNotDeletedWithHealthMoreThanZero() {
        //given
        Character character = getDefaultCharacter();
        helper.getCellAtCoordinates(0, 0).get().setCharacter(character);
        repository.getCharacterList().add(character);
        character.setCurrentHealth(1);

        //when
        manager.updateCharactersLivingStatus();

        //then
        Assert.assertEquals(1, repository.getCharacterList().size());
        Assert.assertTrue(helper.getCellAtCoordinates(0, 0).get().getCharacter().isPresent());
    }
}