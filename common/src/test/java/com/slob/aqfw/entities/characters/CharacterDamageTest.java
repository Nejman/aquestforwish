package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableMap;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.level.hex.CellHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

public class CharacterDamageTest {
    private static final ImmutableMap<Side, Integer> BASIC_ARMOUR = ImmutableMap.of(Side.RIGHT, 1);

    private static Character getTargetCharacter(){
        return new Character(null, 10, null, BASIC_ARMOUR, null, 0, 0, 0, null);
    }

    private static Character getAttackingCharacter(){
        return new Character(null, 10, null, BASIC_ARMOUR, null, 0, 3, 0, null);
    }

    private CellHelper helper;
    private CharacterDamageManager manager;

    @Before
    public void setup(){
        helper = Mockito.mock(CellHelper.class);
        Mockito.when(helper.getAttackedSide(Mockito.any(), Mockito.any())).thenReturn(Optional.of(Side.RIGHT));
        manager = new CharacterDamageManager(helper);
    }

    @Test
    public void damageCharacter() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, -2);

        //when
        manager.changeCharacterHealth(effect, target, attacker);

        //then
        Assert.assertEquals(9, target.getCurrentHealth());
    }


    @Test
    public void baseDamageCharacter() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect effect = new Effect(EffectType.BASE_ATTACK, -2);

        //when
        manager.changeCharacterHealth(effect, target, attacker);

        //then
        Assert.assertEquals(8, target.getCurrentHealth());
    }

    @Test
    public void damageLessThanArmourCharacter() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, -1);

        //when
        manager.changeCharacterHealth(effect, target, attacker);

        //then
        Assert.assertEquals(10, target.getCurrentHealth());
    }

    @Test
    public void normalDamageWithBoostCharacter() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect increasedAttackEffect = new Effect(EffectType.ATTACK_VALUE_CHANGE, 3);
        attacker.addEffect(increasedAttackEffect);
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, -2);

        //when
        manager.changeCharacterHealth(effect, target, attacker);

        //then
        Assert.assertEquals(9, target.getCurrentHealth());
    }

    @Test
    public void baseDamageWithBoostCharacter() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect increasedAttackEffect = new Effect(EffectType.ATTACK_VALUE_CHANGE, 3);
        attacker.addEffect(increasedAttackEffect);
        Effect effect = new Effect(EffectType.BASE_ATTACK, -2);

        //when
        manager.changeCharacterHealth(effect, target, attacker);

        //then
        Assert.assertEquals(5, target.getCurrentHealth());
    }

    @Test
    public void healAfterDamageCharacter() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, -3);

        //when
        manager.changeCharacterHealth(effect, target, attacker);
        Effect healEffect = new Effect(EffectType.HEALTH_CHANGE, 1);
        manager.changeCharacterHealth(healEffect, target, attacker);

        //then
        Assert.assertEquals(9, target.getCurrentHealth());
    }

    @Test
    public void healToMaxAfterDamageCharacter() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect effect = new Effect(EffectType.BASE_ATTACK, -2);

        //when
        manager.changeCharacterHealth(effect, target, attacker);
        Effect healEffect = new Effect(EffectType.HEALTH_CHANGE, 5);
        manager.changeCharacterHealth(healEffect, target, attacker);

        //then
        Assert.assertEquals(10, target.getCurrentHealth());
    }

    @Test
    public void damageCharacterWithIncreasedArmour() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, -2);
        Effect armourChange = new Effect(EffectType.ARMOR_CHANGE, 1);
        armourChange.getAdditionalValues().put(EffectAdditionalValueType.SIDE, Side.RIGHT);
        target.addEffect(armourChange);

        //when
        manager.changeCharacterHealth(effect, target, attacker);

        //then
        Assert.assertEquals(10, target.getCurrentHealth());
    }

    @Test
    public void damageCharacterWithDecreasedArmour() {
        //given
        Character target = getTargetCharacter();
        Character attacker = getAttackingCharacter();
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, -2);
        Effect armourChange = new Effect(EffectType.ARMOR_CHANGE, -1);
        armourChange.getAdditionalValues().put(EffectAdditionalValueType.SIDE, Side.RIGHT);
        target.addEffect(armourChange);

        //when
        manager.changeCharacterHealth(effect, target, attacker);

        //then
        Assert.assertEquals(8, target.getCurrentHealth());
    }

    @Test
    public void damageCharacterIndirectly() {
        //given
        Character target = getTargetCharacter();
        Effect effect = new Effect(EffectType.HEALTH_CHANGE, -2);
        Mockito.when(helper.getAttackedSide(Mockito.any(), Mockito.any())).thenCallRealMethod();

        //when
        manager.changeCharacterHealth(effect, target, null);

        //then
        Assert.assertEquals(8, target.getCurrentHealth());
    }
}