package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableMap;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.skill.PointType;
import org.junit.Assert;
import org.junit.Test;

public class CharacterPointTest {
    private static final ImmutableMap<PointType, Integer> DEFAULT_MAX_POINT = ImmutableMap.of(
            PointType.ACTION, 3,
            PointType.ATTACK, 3,
            PointType.MOVEMENT, 3);

    private static Character getDefaultCharacter(){
        return new Character(null, 0, DEFAULT_MAX_POINT, null, null, 0, 0, 0, null);
    }
    
    @Test
    public void pointResetTest() {
        //given
        Character character = getDefaultCharacter();

        //then
        for(PointType type : PointType.values()){
            Assert.assertEquals(3, CharacterPointManager.getRemainingPoints(type, character));
        }
    }

    @Test
    public void pointAfterUseTest() {
        //given
        Character character = getDefaultCharacter();

        //when
        CharacterPointManager.usePoints(4, PointType.ATTACK, character);

        //then
        Assert.assertEquals(2, CharacterPointManager.getRemainingPoints(PointType.ACTION, character));
        Assert.assertEquals(0, CharacterPointManager.getRemainingPoints(PointType.ATTACK, character));
    }


    @Test
    public void pointAfterUseWithNeedToUseActionTest() {
        //given
        Character character = getDefaultCharacter();
        for (PointType type : PointType.values()) {

            //when
            CharacterPointManager.usePoints(1, type, character);

            //then
            Assert.assertEquals(2, CharacterPointManager.getRemainingPoints(type, character));
        }
    }

    @Test
    public void pointResetAfterUseTest() {
        //given
        Character character = getDefaultCharacter();

        //when
        for(PointType type : PointType.values()){
            CharacterPointManager.usePoints(1, type, character);
        }
        CharacterPointManager.resetAllPoints(character);

        //then
        for(PointType type : PointType.values()){
            Assert.assertEquals(3, CharacterPointManager.getRemainingPoints(type, character));
        }
    }

    @Test
    public void pointEffectsTest() {
        //given
        Character character = getDefaultCharacter();
        for(PointType type : PointType.values()){
            Effect pointEffect = new Effect(type.getCorrespondingEffectType(), 1);

            //when
            character.addEffect(pointEffect);

            //then
            Assert.assertEquals(4, CharacterPointManager.getRemainingPoints(type, character));
        }
    }

    @Test
    public void addingEffectsTest() {
        //given
        Character character = getDefaultCharacter();
        for(PointType type : PointType.values()){
            Effect pointEffect = new Effect(type.getCorrespondingEffectType(), 1);

            //when
            character.addEffect(pointEffect);
        }

        //then
        Assert.assertEquals(3, character.getActiveEffects().size());
    }

    @Test
    public void resetEffectsTest() {
        //given
        Character character = getDefaultCharacter();
        for(PointType type : PointType.values()){
            Effect pointEffect = new Effect(type.getCorrespondingEffectType(), 1);
            character.addEffect(pointEffect);
        }

        //when
        character.resetEffects();

        //then
        Assert.assertEquals(0, character.getActiveEffects().size());
    }

    @Test
    public void checkIfPointsCanBeUsed() {
        //given
        Character character = getDefaultCharacter();
        ImmutableMap<PointType, Integer> pointMap = ImmutableMap.of(PointType.ATTACK, 1, PointType.ACTION, 1, PointType.MOVEMENT, 1);

        //when
        boolean canBeUsed = CharacterPointManager.hesCharacterEnoughPoints(pointMap, character);

        //then
        Assert.assertTrue(canBeUsed);
    }

    @Test
    public void checkIfPointsCanBeUsedWithActionPoints() {
        //given
        Character character = getDefaultCharacter();
        ImmutableMap<PointType, Integer> pointMap = ImmutableMap.of(PointType.ATTACK, 6);

        //when
        boolean canBeUsed = CharacterPointManager.hesCharacterEnoughPoints(pointMap, character);

        //then
        Assert.assertTrue(canBeUsed);

    }

    @Test
    public void checkIfPointsCannotBeUsed() {
        //given
        Character character = getDefaultCharacter();
        ImmutableMap<PointType, Integer> pointMap = ImmutableMap.of(PointType.ATTACK, 4, PointType.ACTION, 4, PointType.MOVEMENT, 4);

        //when
        boolean canBeUsed = CharacterPointManager.hesCharacterEnoughPoints(pointMap, character);

        //then
        Assert.assertFalse(canBeUsed);

    }

    @Test
    public void checkIfPointsCannotBeUsedEvenWithActionPoints() {
        //given
        Character character = getDefaultCharacter();
        ImmutableMap<PointType, Integer> pointMap = ImmutableMap.of(PointType.ATTACK, 7);

        //when
        boolean canBeUsed = CharacterPointManager.hesCharacterEnoughPoints(pointMap, character);

        //then
        Assert.assertFalse(canBeUsed);
    }

    @Test
    public void checkIfPointsCanBeUsedWithActionPointsWhenBothTypesAreInUse() {
        //given
        Character character = getDefaultCharacter();
        ImmutableMap<PointType, Integer> pointMap = ImmutableMap.of(PointType.ATTACK, 4, PointType.MOVEMENT, 4);

        //when
        boolean canBeUsed = CharacterPointManager.hesCharacterEnoughPoints(pointMap, character);

        //then
        Assert.assertTrue(canBeUsed);
    }

    @Test
    public void checkIfPointsCannotBeUsedEvenWithActionPointsWhenBothTypesAreInUse() {
        //given
        Character character = getDefaultCharacter();
        ImmutableMap<PointType, Integer> pointMap = ImmutableMap.of(PointType.ATTACK, 6, PointType.MOVEMENT, 4);

        //when
        boolean canBeUsed = CharacterPointManager.hesCharacterEnoughPoints(pointMap, character);

        //then
        Assert.assertFalse(canBeUsed);
    }

}