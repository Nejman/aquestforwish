package com.slob.aqfw.entities.characters;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.slob.aqfw.level.skill.base.BaseAttackSkill;
import org.junit.Assert;
import org.junit.Test;

public class SkillTest {
    @Test
    public void checkForBaseAttackInBeast() {
        //given
        Injector injector = Guice.createInjector();
        CharacterFactory factory = injector.getInstance(CharacterFactory.class);
        Character beast = factory.createCharacter("BEAST_ID");

        //then
        Assert.assertNotNull(beast.getSkills());
        Assert.assertFalse(beast.getSkills().isEmpty());
        Assert.assertTrue(beast.getSkills().get(0) instanceof BaseAttackSkill);
    }
}