package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import org.junit.Assert;
import org.junit.Test;


public class CharacterAttackTest {

    private static Character getDefaultCharacter() {
        return new Character(null, 0, null, null, null, 0, 5, 0, null);
    }

    @Test
    public void getCharacterSpeed() {
        //given
        Character character = getDefaultCharacter();

        //when
        int speed = CharacterAttackManager.getAttackValue(character);

        //then
        Assert.assertEquals(5, speed);
    }

    @Test
    public void getCharacterSpeedAfterModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.ATTACK_VALUE_CHANGE, 1);
        character.addEffect(effect);

        //when
        int speed = CharacterAttackManager.getAttackValue(character);

        //then
        Assert.assertEquals(6, speed);
    }

    @Test
    public void getCharacterSpeedAfterNegativeModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.ATTACK_VALUE_CHANGE, -1);
        character.addEffect(effect);

        //when
        int speed = CharacterAttackManager.getAttackValue(character);

        //then
        Assert.assertEquals(4, speed);
    }

    @Test
    public void getCharacterSpeedAfterModificationsNoLessThanZero() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.ATTACK_VALUE_CHANGE, -7);
        character.addEffect(effect);

        //when
        int speed = CharacterAttackManager.getAttackValue(character);

        //then
        Assert.assertEquals(0, speed);
    }
}