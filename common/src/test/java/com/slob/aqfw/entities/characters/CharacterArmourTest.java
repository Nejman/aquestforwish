package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableMap;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import org.junit.Assert;
import org.junit.Test;


public class CharacterArmourTest {
    private static final ImmutableMap<Side, Integer> BASIC_ARMOUR = ImmutableMap.of(Side.RIGHT, 1);

    private static Character getDefaultCharacter() {
        return new Character(null, 0, null, BASIC_ARMOUR, null, 0, 0, 0, null);
    }

    @Test
    public void getCharacterArmour() {
        //given
        Character character = getDefaultCharacter();

        //when
        int armour = CharacterArmourManager.getArmourOnSide(character, Side.RIGHT);

        //then
        Assert.assertEquals(1, armour);
    }

    @Test
    public void getCharacterArmourAfterModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.ARMOR_CHANGE, 1);
        effect.getAdditionalValues().put(EffectAdditionalValueType.SIDE, Side.RIGHT);
        character.addEffect(effect);

        //when
        int armour = CharacterArmourManager.getArmourOnSide(character, Side.RIGHT);

        //then
        Assert.assertEquals(2, armour);
    }

    @Test
    public void getCharacterArmourAfterNegativeModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.ARMOR_CHANGE, -1);
        effect.getAdditionalValues().put(EffectAdditionalValueType.SIDE, Side.RIGHT);
        character.addEffect(effect);

        //when
        int armour = CharacterArmourManager.getArmourOnSide(character, Side.RIGHT);

        //then
        Assert.assertEquals(0, armour);
    }

}