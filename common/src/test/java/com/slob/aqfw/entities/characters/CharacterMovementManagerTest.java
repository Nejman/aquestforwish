package com.slob.aqfw.entities.characters;

import com.google.common.collect.ImmutableList;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.hex.CellRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class CharacterMovementManagerTest {
    private CharacterMovementManager manager;
    private Character character;
    private CellHelper helper;

    @Before
    public void setUp() {
        CellRepository repository = new CellRepository();
        repository.setCells(HexCreator.createHexField(5));
        helper = new CellHelper(repository);
        manager = new CharacterMovementManager(helper);
        character = Mockito.mock(Character.class);
        helper.getCellAtCoordinates(5, 4).get().setCharacter(character);
    }

    @Test
    public void getAllowedCellsForMovement() {
        //when
        List<Cell> allowedCellsForMovement = manager.getAllowedCellsForMovement(2, character);

        //then
        Assert.assertEquals(19, allowedCellsForMovement.size());

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 6).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 6).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 6).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 5).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(7, 4).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 2).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 3).get()));
    }


    @Test
    public void getAllowedCellsForMovementWhenNearEdge() {
        //given
        helper.getCellAtCoordinates(5, 4).get().setCharacter(null);
        helper.getCellAtCoordinates(0, 2).get().setCharacter(character);

        //when
        List<Cell> allowedCellsForMovement = manager.getAllowedCellsForMovement(2, character);

        //then
        Assert.assertEquals(12, allowedCellsForMovement.size());
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 1).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 0).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(1, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(1, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(1, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(1, 1).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(2, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(2, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(2, 2).get()));
    }

    @Test
    public void getAllowedCellsForMovementWhenInCorner() {
        //given
        helper.getCellAtCoordinates(5, 4).get().setCharacter(null);
        helper.getCellAtCoordinates(0, 0).get().setCharacter(character);

        //when
        List<Cell> allowedCellsForMovement = manager.getAllowedCellsForMovement(2, character);

        //then
        Assert.assertEquals(9, allowedCellsForMovement.size());

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 0).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 1).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(0, 2).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(1, 0).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(1, 1).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(1, 2).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(2, 0).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(2, 1).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(2, 2).get()));
    }


    @Test
    public void getAllowedCellsForMovementWithObstacles() {
        //given
        Character obstacle = Mockito.mock(Character.class);
        helper.getCellAtCoordinates(3, 4).get().setCharacter(obstacle);

        //when
        List<Cell> allowedCellsForMovement = manager.getAllowedCellsForMovement(2, character);

        //then
        Assert.assertEquals(18, allowedCellsForMovement.size());

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 6).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 6).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 6).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 5).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(7, 4).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 2).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 3).get()));
    }

    @Test
    public void getAllowedCellsForMovementWithUnreachableFieldDueToObstacles() {
        //given
        Character obstacle = Mockito.mock(Character.class);
        helper.getCellAtCoordinates(4, 4).get().setCharacter(obstacle);

        //when
        List<Cell> allowedCellsForMovement = manager.getAllowedCellsForMovement(2, character);

        //then
        Assert.assertEquals(17, allowedCellsForMovement.size());

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 6).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 6).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 6).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 5).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(7, 4).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(4, 2).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 2).get()));

        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 3).get()));
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(6, 3).get()));
    }


    @Test
    public void getAllowedCellsForMovementWhenSurroundedByObstacles() {
        //given
        Character obstacle1 = Mockito.mock(Character.class);
        Character obstacle2 = Mockito.mock(Character.class);
        Character obstacle3 = Mockito.mock(Character.class);
        Character obstacle4 = Mockito.mock(Character.class);
        Character obstacle5 = Mockito.mock(Character.class);
        Character obstacle6 = Mockito.mock(Character.class);
        helper.getCellAtCoordinates(4, 4).get().setCharacter(obstacle1);
        helper.getCellAtCoordinates(4, 5).get().setCharacter(obstacle2);
        helper.getCellAtCoordinates(5, 5).get().setCharacter(obstacle3);
        helper.getCellAtCoordinates(6, 4).get().setCharacter(obstacle4);
        helper.getCellAtCoordinates(5, 3).get().setCharacter(obstacle5);
        helper.getCellAtCoordinates(4, 3).get().setCharacter(obstacle6);

        //when
        List<Cell> allowedCellsForMovement = manager.getAllowedCellsForMovement(2, character);

        //then
        Assert.assertEquals(1, allowedCellsForMovement.size());
        Assert.assertTrue(allowedCellsForMovement.contains(helper.getCellAtCoordinates(5, 4).get()));
    }

    @Test
    public void getPathToNeighbour() {
        //when
        List<Cell> path = manager.getPath(2, character, helper.getCellAtCoordinates(5, 4).get(), helper.getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertEquals(2, path.size());
        assertTrue(path.contains(helper.getCellAtCoordinates(5, 4).get()));
        assertTrue(path.contains(helper.getCellAtCoordinates(4, 4).get()));
    }

    @Test
    public void getPathToDistantNeighbour() {
        //when
        List<Cell> path = manager.getPath(3, character, helper.getCellAtCoordinates(5, 4).get(), helper.getCellAtCoordinates(3, 4).get());

        //then
        Assert.assertEquals(3, path.size());
        assertTrue(path.contains(helper.getCellAtCoordinates(5, 4).get()));
        assertTrue(path.contains(helper.getCellAtCoordinates(4, 4).get()));
        assertTrue(path.contains(helper.getCellAtCoordinates(3, 4).get()));
    }

    @Test
    public void getPathToDistantAngledCell() {
        //when
        List<Cell> path = manager.getPath(3, character, helper.getCellAtCoordinates(5, 4).get(), helper.getCellAtCoordinates(2, 5).get());

        //then
        Assert.assertEquals(4, path.size());
    }

    @Test
    public void getPathToDistantNeighbourWithObstacle() {
        //given
        Character obstacle = Mockito.mock(Character.class);
        helper.getCellAtCoordinates(4, 4).get().setCharacter(obstacle);

        //when
        List<Cell> path = manager.getPath(3, character, helper.getCellAtCoordinates(5, 4).get(), helper.getCellAtCoordinates(3, 4).get());

        //then
        Assert.assertEquals(4, path.size());
        assertTrue(path.contains(helper.getCellAtCoordinates(5, 4).get()));
        assertTrue(!path.contains(helper.getCellAtCoordinates(4, 4).get()));
        assertTrue(path.contains(helper.getCellAtCoordinates(3, 4).get()));
    }

    @Test
    public void movementTest() {
        //given
        Mockito.when(character.getDefaultSpeed()).thenReturn(5);
        Mockito.when(character.getActiveEffects()).thenReturn(ImmutableList.of());
        Cell target = helper.getCellAtCoordinates(3, 4).get();

        //when
        manager.moveCharacter(character, target);

        //then
        Assert.assertFalse(helper.getCellAtCoordinates(5, 4).get().getCharacter().isPresent());
        Assert.assertTrue(helper.getCellAtCoordinates(3, 4).get().getCharacter().isPresent());
        Assert.assertEquals(helper.getCellAtCoordinates(3, 4).get().getCharacter().get(), character);
    }
}