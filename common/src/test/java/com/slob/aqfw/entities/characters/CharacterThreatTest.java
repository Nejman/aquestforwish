package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import org.junit.Assert;
import org.junit.Test;

public class CharacterThreatTest {

    private static Character getDefaultCharacter() {
        return new Character(null, 0, null, null, null, 0, 0, 5, null);
    }

    @Test
    public void getCharacterThreat() {
        //given
        Character character = getDefaultCharacter();

        //when
        int threat = CharacterThreatManager.getThreatLevel(character);

        //then
        Assert.assertEquals(5, threat);
    }

    @Test
    public void getCharacterThreatAfterModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.THREAT_CHANGE, 1);
        character.addEffect(effect);

        //when
        int threat = CharacterThreatManager.getThreatLevel(character);

        //then
        Assert.assertEquals(6, threat);
    }

    @Test
    public void getCharacterThreatAfterNegativeModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.THREAT_CHANGE, -1);
        character.addEffect(effect);

        //when
        int threat = CharacterThreatManager.getThreatLevel(character);

        //then
        Assert.assertEquals(4, threat);
    }

}