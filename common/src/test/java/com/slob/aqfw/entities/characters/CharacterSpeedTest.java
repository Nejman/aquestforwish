package com.slob.aqfw.entities.characters;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import org.junit.Assert;
import org.junit.Test;

public class CharacterSpeedTest {

    private static Character getDefaultCharacter() {
        return new Character(null, 0, null, null, null, 5, 0, 0, null);
    }

    @Test
    public void getCharacterSpeed() {
        //given
        Character character = getDefaultCharacter();

        //when
        int speed = CharacterSpeedManager.getCharacterSpeed(character);

        //then
        Assert.assertEquals(5, speed);
    }

    @Test
    public void getCharacterSpeedAfterModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.SPEED_CHANGE, 1);
        character.addEffect(effect);

        //when
        int speed = CharacterSpeedManager.getCharacterSpeed(character);

        //then
        Assert.assertEquals(6, speed);
    }

    @Test
    public void getCharacterSpeedAfterNegativeModifications() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.SPEED_CHANGE, -1);
        character.addEffect(effect);

        //when
        int speed = CharacterSpeedManager.getCharacterSpeed(character);

        //then
        Assert.assertEquals(4, speed);
    }

    @Test
    public void getCharacterSpeedAfterModificationsNoLessThanZero() {
        //given
        Character character = getDefaultCharacter();
        Effect effect = new Effect(EffectType.SPEED_CHANGE, -7);
        character.addEffect(effect);

        //when
        int speed = CharacterSpeedManager.getCharacterSpeed(character);

        //then
        Assert.assertEquals(0, speed);
    }

}