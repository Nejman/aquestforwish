package com.slob.aqfw.service;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillHistory;
import com.slob.aqfw.level.skill.SkillUseResult;
import com.slob.aqfw.level.skill.SkillUseService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SkillUseServiceTest {

    private LevelService levelService;
    private SkillHistory skillHistory;
    private SkillUseService skillUseService;
    private CellHelper cellHelper;

    @Before
    public void setUp() {
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = mainService.getService();
        levelService = logicService.getLevelService();
        levelService.generateLevel();
        skillHistory = logicService.getInjector().getInstance(SkillHistory.class);
        skillUseService = logicService.getInjector().getInstance(SkillUseService.class);
        cellHelper = logicService.getInjector().getInstance(CellHelper.class);
    }


    @Test
    public void performAllFromHistory() {
        //given
        Character mage = levelService.createCharacter(CharacterFactory.MAGE_ID);
        Character ork = levelService.createCharacter(CharacterFactory.ORK_ID);
        Character goblin = levelService.createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character knight = levelService.createCharacter(CharacterFactory.KNIGHT_ID);
        levelService.addCharacter(mage, 0, 0);
        levelService.addCharacter(ork, 1, 0);
        levelService.addCharacter(goblin, 4, 0);
        levelService.addCharacter(knight, 3, 0);

        SkillUseResult result1 = new SkillUseResult(ork.getId(), "BASE_ATTACK", 0, 0);
        SkillUseResult result2 = new SkillUseResult(ork.getId(), "BASE_ATTACK", 0, 0);
        SkillUseResult result3 = new SkillUseResult(ork.getId(), "BASE_MOVEMENT_SKILL", 3, 1);
        SkillUseResult result4 = new SkillUseResult(goblin.getId(), "BASE_ATTACK", 3, 0);
        SkillUseResult result5 = new SkillUseResult(goblin.getId(), "BASE_ATTACK", 3, 0);

        SkillUseResult result6 = new SkillUseResult(mage.getId(), "BASE_MOVEMENT_SKILL", 1, 1);
        SkillUseResult result7 = new SkillUseResult(knight.getId(), "BASE_MOVEMENT_SKILL", 5, 2);

        skillHistory.getResultHistory().add(result1);
        skillHistory.getResultHistory().add(result2);
        skillHistory.getResultHistory().add(result3);
        skillHistory.getResultHistory().add(result4);
        skillHistory.getResultHistory().add(result5);
        skillHistory.getResultHistory().add(result6);
        skillHistory.getResultHistory().add(result7);

        //when
        skillUseService.performAllFromHistory();

        //then
        Cell mageCell = cellHelper.getCharacterCell(mage);
        Cell orkCell = cellHelper.getCharacterCell(ork);
        Cell goblinCell = cellHelper.getCharacterCell(goblin);
        Cell knightCell = cellHelper.getCharacterCell(knight);
        Assert.assertEquals(1, mageCell.getX());
        Assert.assertEquals(1, mageCell.getY());

        Assert.assertEquals(3, orkCell.getX());
        Assert.assertEquals(1, orkCell.getY());

        Assert.assertEquals(4, goblinCell.getX());
        Assert.assertEquals(0, goblinCell.getY());

        Assert.assertEquals(5, knightCell.getX());
        Assert.assertEquals(2, knightCell.getY());

        Assert.assertEquals(3, mage.getCurrentHealth());
        Assert.assertEquals(5, ork.getCurrentHealth());
        Assert.assertEquals(2, goblin.getCurrentHealth());
        Assert.assertEquals(13, knight.getCurrentHealth());
    }
}