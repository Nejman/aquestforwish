package com.slob.aqfw.service;

import com.slob.aqfw.CommonSkillSetUp;
import org.junit.Assert;
import org.junit.Test;

public class MainServiceTest {

    @Test
    public void instantiate() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = service.getService();

        //then
        Assert.assertNotNull(logicService);
    }
}