package com.slob.aqfw.service;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.level.skill.SkillUseResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LevelServiceTest {

    private LevelService levelService;

    @Before
    public void setUp() {
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = mainService.getService();
        levelService = logicService.getLevelService();
        levelService.generateLevel();
    }

    @Test
    public void getPlayerCharactersTest() {
        //given
        Character mage = levelService.createCharacter(CharacterFactory.MAGE_ID);
        Character ork = levelService.createCharacter(CharacterFactory.ORK_ID);
        Character goblin = levelService.createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character knight = levelService.createCharacter(CharacterFactory.KNIGHT_ID);
        levelService.addCharacter(mage, 0, 0);
        levelService.addCharacter(ork, 1, 0);
        levelService.addCharacter(goblin, 2, 0);
        levelService.addCharacter(knight, 3, 0);

        //when
        List<Character> playerCharacters = levelService.getPlayerCharacters();

        //then
        Assert.assertEquals(2, playerCharacters.size());
        Assert.assertTrue(playerCharacters.contains(mage));
        Assert.assertTrue(playerCharacters.contains(knight));
        Assert.assertFalse(playerCharacters.contains(ork));
        Assert.assertFalse(playerCharacters.contains(goblin));
    }

    @Test
    public void handleAiTest() {
        //given
        Character mage = levelService.createCharacter(CharacterFactory.MAGE_ID);
        Character ork = levelService.createCharacter(CharacterFactory.ORK_ID);
        Character goblin = levelService.createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character knight = levelService.createCharacter(CharacterFactory.KNIGHT_ID);
        levelService.addCharacter(mage, 0, 0);
        levelService.addCharacter(ork, 1, 0);
        levelService.addCharacter(goblin, 2, 0);
        levelService.addCharacter(knight, 3, 0);
        levelService.startLevel();
        levelService.endPlayerTurn();

        //when
        List<SkillUseResult> skillUseHistory = new LinkedList<>();
        while (!levelService.isPlayersTurn()) {
            Optional<SkillUseResult> skillUseResult = levelService.handleAITurn();
            skillUseResult.ifPresent(skillUseHistory::add);
        }

        //then
        Assert.assertEquals(6, skillUseHistory.size());
    }

    @Test
    public void multipleTurnsWithNoPlayer() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = mainService.getService();
        logicService.getLevelService().generateLevel();

        //when
        for (int i = 0; i < 50; i++) {
            logicService.getLevelService().endPlayerTurn();
            while (!logicService.getLevelService().isPlayersTurn()) {
                logicService.getLevelService().handleAITurn();
            }
        }
    }

}