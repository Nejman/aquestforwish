package com.slob.aqfw.service.dictionary;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DictionaryTest {
    private Dictionary dictionary;
    private LogicService logicService;

    @Before
    public void setUp() {
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        logicService = mainService.getService();
        dictionary = logicService.getDictionary();
    }

    @Test
    public void getSkillName() {
        //given
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);

        //when
        String skillName = dictionary.getSkillName(character.getSkills().get(0));

        //then
        Assert.assertEquals("Melee attack", skillName);
    }

    @Test
    public void getSkillDescription() {
        //given
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);

        //when
        String skillDescription = dictionary.getSkillDescription(character.getSkills().get(0));

        //then
        Assert.assertEquals("Melee attack Description", skillDescription);
    }

    @Test
    public void setLang() {
        //given
        dictionary.setLang(Lang.POLISH.getCode());
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);

        //when
        String skillName = dictionary.getSkillName(character.getSkills().get(0));

        //then
        Assert.assertEquals("Atak wręcz", skillName);
    }
}