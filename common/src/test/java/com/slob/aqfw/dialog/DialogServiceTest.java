package com.slob.aqfw.dialog;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;


public class DialogServiceTest {
    private DialogService dialogService;
    private LogicService logicService;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        dialogService = logicService.getDialogService();
    }

    @Test
    public void DialogInNewLevel() {
        //given
        logicService.getGameStateCache().setGameState(GameState.CAMP);
        logicService.handleGamestate();

        //when
        List<Dialog> activeDialogs = dialogService.getActiveDialogs();
        Optional<Dialog> currentDialog = dialogService.getCurrentDialog();

        //then
        Assert.assertEquals(1, activeDialogs.size());
        Assert.assertTrue(currentDialog.isPresent());
    }

    @Test
    public void correctDialogInCamp() {
        //given
        logicService.getGameStateCache().setGameState(GameState.NEW_GAME);
        logicService.handleGamestate();
        List<Dialog> activeDialogs = dialogService.getActiveDialogs();
        logicService.getGameStateCache().setGameState(GameState.CAMP);
        logicService.handleGamestate();

        //when
        activeDialogs = dialogService.getActiveDialogs();
        Optional<Dialog> currentDialog = dialogService.getCurrentDialog();

        //then
        Assert.assertEquals(1, activeDialogs.size());
        Assert.assertTrue(currentDialog.isPresent());
        Assert.assertEquals(3, currentDialog.get().getStatements().size());
    }

    @Test
    public void noDialogInNextLevel() {
        //given
        logicService.getGameStateCache().setGameState(GameState.NEW_GAME);
        logicService.handleGamestate();
        logicService.getGameStateCache().setGameState(GameState.START_LEVEL);
        logicService.handleGamestate();


        //when
        List<Dialog> activeDialogs = dialogService.getActiveDialogs();
        Optional<Dialog> currentDialog = dialogService.getCurrentDialog();

        //then
        Assert.assertEquals(0, activeDialogs.size());
        Assert.assertFalse(currentDialog.isPresent());
    }
}