package com.slob.aqfw.level;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.hex.CellRepository;
import com.slob.aqfw.level.trigger.TriggerRepository;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

public class LevelFactoryTest {

    @Test
    public void generateLevel() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = service.getService();
        CellRepository cellRepository = logicService.getInjector().getInstance(CellRepository.class);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        TriggerRepository triggerRepository = logicService.getInjector().getInstance(TriggerRepository.class);

        //when
        logicService.getLevelService().generateLevel("TEST_LEVEL_1");

        //then
        Assert.assertEquals(61, cellRepository.getCells().size());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(0, 0).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 0).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(2, 0).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 0).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 0).get().getCharacter().isPresent());

        Assert.assertTrue(cellHelper.getCellAtCoordinates(2, 6).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 6).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(2, 2).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 2).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 2).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 3).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 3).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(5, 3).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(2, 8).get().getCharacter().isPresent());

        Assert.assertFalse(triggerRepository.getTriggers().isEmpty());
        Assert.assertEquals(2, triggerRepository.getTriggers().size());
    }
}