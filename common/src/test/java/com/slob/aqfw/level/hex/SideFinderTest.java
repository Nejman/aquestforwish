package com.slob.aqfw.level.hex;

import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.entities.characters.Character;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class SideFinderTest {

    private CellHelper helper;

    @Before
    public void setup() {
        CellRepository repository = new CellRepository();
        repository.setCells(HexCreator.createHexField(4));
        helper = new CellHelper(repository);
    }

    @Test
    public void testCellFinder() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);

        //then
        Assert.assertTrue(cell.isPresent());
    }

    @Test
    public void testNeighboursSide() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);
        Optional<Cell> secondCell = helper.getCellAtCoordinates(1, 0);

        //when
        Optional<Side> optionalSide = helper.getSideOfNeighbour(cell.get(), secondCell.get());

        //then
        Assert.assertTrue(optionalSide.isPresent());
        Assert.assertEquals(Side.RIGHT, optionalSide.get());
    }

    @Test
    public void testIncorrectNeighboursSide() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);
        Optional<Cell> secondCell = helper.getCellAtCoordinates(2, 0);

        //when
        Optional<Side> optionalSide = helper.getSideOfNeighbour(cell.get(), secondCell.get());

        //then
        Assert.assertFalse(optionalSide.isPresent());
    }

    @Test
    public void testDistantNeighboursSide() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);
        Optional<Cell> secondCell = helper.getCellAtCoordinates(2, 0);

        //when
        Optional<Side> optionalSide = helper.getSideOfDistantCell(cell.get(), secondCell.get());

        //then
        Assert.assertTrue(optionalSide.isPresent());
        Assert.assertEquals(Side.RIGHT, optionalSide.get());
    }

    @Test
    public void testIncorrectDistantNeighboursSide() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);
        Optional<Cell> secondCell = helper.getCellAtCoordinates(2, 1);

        //when
        Optional<Side> optionalSide = helper.getSideOfDistantCell(cell.get(), secondCell.get());

        //then
        Assert.assertFalse(optionalSide.isPresent());
    }

    @Test
    public void testCharactersSideNeighbours() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);
        Optional<Cell> secondCell = helper.getCellAtCoordinates(1, 0);
        Character characterA = new Character(null, 0, null, null, null, 0, 0, 0, null);
        Character characterB = new Character(null, 0, null, null, null, 0, 0, 0, null);

        cell.get().setCharacter(characterA);
        secondCell.get().setCharacter(characterB);

        //when
        Optional<Side> optionalSide = helper.getSideForCharacters(characterA, characterB);

        //then
        Assert.assertTrue(optionalSide.isPresent());
        Assert.assertEquals(Side.RIGHT, optionalSide.get());
    }

    @Test
    public void testCharactersSideDistant() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);
        Optional<Cell> secondCell = helper.getCellAtCoordinates(2, 0);
        Character characterA = new Character(null, 0, null, null, null, 0, 0, 0, null);
        Character characterB = new Character(null, 0, null, null, null, 0, 0, 0, null);

        cell.get().setCharacter(characterA);
        secondCell.get().setCharacter(characterB);

        //when
        Optional<Side> optionalSide = helper.getSideForCharacters(characterA, characterB);

        //then
        Assert.assertTrue(optionalSide.isPresent());
        Assert.assertEquals(Side.RIGHT, optionalSide.get());
    }

    @Test
    public void testCharactersSideEmpty() {
        //given
        Optional<Cell> cell = helper.getCellAtCoordinates(0, 0);
        Optional<Cell> secondCell = helper.getCellAtCoordinates(2, 1);
        Character characterA = new Character(null, 0, null, null, null, 0, 0, 0, null);
        Character characterB = new Character(null, 0, null, null, null, 0, 0, 0, null);

        cell.get().setCharacter(characterA);
        secondCell.get().setCharacter(characterB);

        //when
        Optional<Side> optionalSide = helper.getSideForCharacters(characterA, characterB);

        //then
        Assert.assertFalse(optionalSide.isPresent());
    }
}