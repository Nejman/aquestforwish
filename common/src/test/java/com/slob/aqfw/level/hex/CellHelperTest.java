package com.slob.aqfw.level.hex;

import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.entities.characters.Character;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class CellHelperTest {

    @Test
    public void getCellForCharacterTest() {
        //given
        CellRepository repository = new CellRepository();
        repository.setCells(HexCreator.createHexField(2));
        CellHelper helper = new CellHelper(repository);
        Character character = Mockito.mock(Character.class);
        helper.getCellAtCoordinates(0, 0).get().setCharacter(character);

        //when
        Cell characterCell = helper.getCharacterCell(character);

        //then
        Assert.assertNotNull(characterCell);
        Assert.assertTrue(characterCell.getCharacter().isPresent());
        Assert.assertTrue(characterCell.getCharacter().get().equals(character));

    }

    @Test
    public void getNoCellForUnbindCharacterTest() {
        //given
        CellRepository repository = new CellRepository();
        repository.setCells(HexCreator.createHexField(2));
        CellHelper helper = new CellHelper(repository);
        Character character = Mockito.mock(Character.class);

        //when
        Cell characterCell = helper.getCharacterCell(character);

        //then
        Assert.assertNull(characterCell);

    }
}