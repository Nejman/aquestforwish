package com.slob.aqfw.level.skill.heroes.ranger;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PiercingShotSkillTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.RANGER_ID);
        logicService.getLevelService().addCharacter(character, 4, 4);
    }

    @Test
    public void use() {
        //given
        Character ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        Character ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        Character ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 3, 4);
        logicService.getLevelService().addCharacter(ork2, 0, 4);
        logicService.getLevelService().addCharacter(ork3, 1, 5);

        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().performSkill(character.getSkills().get(2), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(2, 4).get());

        //then
        Assert.assertEquals(3, ork1.getCurrentHealth());
        Assert.assertEquals(3, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
    }
}