package com.slob.aqfw.level.aura.skillauras;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.aura.AuraFactory;
import com.slob.aqfw.level.aura.AuraRepository;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HailAuraTest {
    private LogicService logicService;
    private Character knight;
    private Character ork1;
    private Character ork2;
    private Character ork3;
    private CellHelper cellHelper;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(knight, 3, 5);
        ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 4, 4);
        ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork2, 3, 4);
        ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork3, 6, 4);
        cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell cell = cellHelper.getCellAtCoordinates(3, 4).get();
        logicService.getInjector().getInstance(AuraFactory.class).createAura(AuraFactory.HAIL, cell);
    }

    @Test
    public void oneTurnHail() {
        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(12, knight.getCurrentHealth());
        Assert.assertEquals(2, ork1.getCurrentHealth());
        Assert.assertEquals(2, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertEquals(1, logicService.getInjector().getInstance(AuraRepository.class).getAuras().size());
    }

    @Test
    public void TwoTurnHail() {
        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(9, knight.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(3, 4).get().getCharacter().isPresent());
        Assert.assertEquals(1, logicService.getInjector().getInstance(AuraRepository.class).getAuras().size());
    }

    @Test
    public void ThreeTurnHail() {
        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().endTurn();
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(6, knight.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(3, 4).get().getCharacter().isPresent());
        Assert.assertEquals(0, logicService.getInjector().getInstance(AuraRepository.class).getAuras().size());
    }
}