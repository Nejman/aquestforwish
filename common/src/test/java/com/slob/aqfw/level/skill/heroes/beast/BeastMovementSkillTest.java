package com.slob.aqfw.level.skill.heroes.beast;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BeastMovementSkillTest {

    private LogicService logicService;
    private CellHelper cellHelper;
    private Character ork1;
    private Character ork2;
    private Character ork3;
    private Character ork4;
    private Character beast;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        ork4 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        beast = logicService.getLevelService().createCharacter(CharacterFactory.BEAST_ID);
        cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        logicService.getLevelService().addCharacter(beast, 4, 4);
        logicService.getLevelService().addCharacter(ork1, 1, 5);
        logicService.getLevelService().addCharacter(ork2, 2, 5);
        logicService.getLevelService().addCharacter(ork3, 1, 2);
        logicService.getLevelService().addCharacter(ork4, 0, 3);
    }

    @Test
    public void applyWithoutEnPassant() {
        //when
        logicService.getLevelService().performSkill(beast.getSkills().get(1), cellHelper.getCellAtCoordinates(1, 4).get());

        //then
        Assert.assertEquals(5, ork1.getCurrentHealth());
        Assert.assertEquals(5, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertEquals(5, ork4.getCurrentHealth());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 4).get().getCharacter().isPresent());
        Assert.assertSame(beast, cellHelper.getCellAtCoordinates(1, 4).get().getCharacter().get());
    }

    @Test
    public void applyWithEnPassant() {
        //given
        Effect effect = new Effect(EffectType.EN_PASSANT, 0);
        beast.getActiveEffects().add(effect);

        //when
        logicService.getLevelService().performSkill(beast.getSkills().get(1), cellHelper.getCellAtCoordinates(1, 4).get());

        //then
        Assert.assertEquals(1, ork1.getCurrentHealth());
        Assert.assertEquals(1, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertEquals(2, ork4.getCurrentHealth());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 4).get().getCharacter().isPresent());
        Assert.assertSame(beast, cellHelper.getCellAtCoordinates(1, 4).get().getCharacter().get());
    }
}