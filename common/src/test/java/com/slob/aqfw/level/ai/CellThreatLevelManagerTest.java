package com.slob.aqfw.level.ai;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CellThreatLevelManagerTest {
    private LogicService logicService;
    private CellHelper cellHelper;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel("EMPTY_LEVEL");
        Character knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        Character ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        logicService.getLevelService().addCharacter(knight, 4, 4);
        logicService.getLevelService().addCharacter(ork, 0, 0);
    }

    @Test
    public void updateCellsThreatLevels() {
        //when
        logicService.getInjector().getInstance(CellThreatLevelManager.class).updateCellsThreatLevels();

        //then
        Assert.assertEquals(5, cellHelper.getCellAtCoordinates(4, 4).get().getThreat());

        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(3, 5).get().getThreat());
        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(3, 4).get().getThreat());
        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(3, 3).get().getThreat());
        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(4, 5).get().getThreat());
        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(5, 4).get().getThreat());
        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(4, 3).get().getThreat());

        Assert.assertEquals(3, cellHelper.getCellAtCoordinates(2, 4).get().getThreat());
        Assert.assertEquals(3, cellHelper.getCellAtCoordinates(6, 4).get().getThreat());

        Assert.assertEquals(2, cellHelper.getCellAtCoordinates(1, 4).get().getThreat());
        Assert.assertEquals(2, cellHelper.getCellAtCoordinates(7, 4).get().getThreat());

        Assert.assertEquals(1, cellHelper.getCellAtCoordinates(0, 4).get().getThreat());
        Assert.assertEquals(1, cellHelper.getCellAtCoordinates(8, 4).get().getThreat());
    }

    @Test
    public void updateCellsWithMoreThanSingleCharacter() {
        //given
        Character beast = logicService.getLevelService().createCharacter(CharacterFactory.BEAST_ID);
        logicService.getLevelService().addCharacter(beast, 4, 1);


        //when
        logicService.getInjector().getInstance(CellThreatLevelManager.class).updateCellsThreatLevels();

        //then
        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(4, 1).get().getThreat());

        Assert.assertEquals(3, cellHelper.getCellAtCoordinates(4, 2).get().getThreat());
        Assert.assertEquals(4, cellHelper.getCellAtCoordinates(4, 3).get().getThreat());
        Assert.assertEquals(3, cellHelper.getCellAtCoordinates(4, 0).get().getThreat());
    }
}