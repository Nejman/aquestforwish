package com.slob.aqfw.level.skill;

import com.google.common.collect.ImmutableList;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.core.skill.SkillTargetsType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.hex.CellRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

public class SkillTargetingHelperTest {
    private Character character;
    private CellHelper cellHelper;
    private SkillTargetingHelper targetingHelper;
    private Skill skill;
    private static final ImmutableList<Side> ALL_SIDES_ALLOWED = ImmutableList.of(Side.LEFT, Side.LEFT_UP, Side.LEFT_DOWN, Side.RIGHT, Side.RIGHT_DOWN, Side.RIGHT_UP);
    private static final ImmutableList<Side> SOME_SIDES_ALLOWED = ImmutableList.of(Side.LEFT, Side.LEFT_UP, Side.LEFT_DOWN);

    @Before
    public void setUp() {
        CellRepository repository = new CellRepository();
        List<Cell> hexField = HexCreator.createHexField(5);
        repository.setCells(hexField);
        character = Mockito.mock(Character.class);
        Mockito.when(character.getRotation()).thenReturn(0);
        cellHelper = new CellHelper(repository);
        Optional<Cell> cell = cellHelper.getCellAtCoordinates(5, 4);
        cell.ifPresent(cell1 -> cell1.setCharacter(character));
        targetingHelper = new SkillTargetingHelper(null, cellHelper);
        skill = Mockito.mock(Skill.class);
        Mockito.when(skill.getUser()).thenReturn(character);
        Mockito.when(skill.modifyTargetedCells(Mockito.any())).thenCallRealMethod();
    }

    @Test
    public void getAllowedTargetListForSelf() {
        //given
        Mockito.when(skill.getTargetsType()).thenReturn(SkillTargetsType.SELF);
        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(skill);

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(1, allowedTargetList.size());
        Assert.assertEquals(cellHelper.getCellAtCoordinates(5, 4).get(), allowedTargetList.get(0));
    }

    @Test
    public void getAllowedTargetListForLineAtFullAllowance() {
        //given
        Mockito.when(skill.getTargetsType()).thenReturn(SkillTargetsType.LINE);
        Mockito.when(skill.getRange()).thenReturn(1);
        Mockito.when(skill.getAllowedSides()).thenReturn(ALL_SIDES_ALLOWED);

        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(skill);

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(6, allowedTargetList.size());
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 3).get()));
    }

    @Test
    public void getAllowedTargetListForLineRangeTwoAtFullAllowance() {
        //given
        Mockito.when(skill.getTargetsType()).thenReturn(SkillTargetsType.LINE);
        Mockito.when(skill.getRange()).thenReturn(2);
        Mockito.when(skill.getAllowedSides()).thenReturn(ALL_SIDES_ALLOWED);

        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(skill);

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(12, allowedTargetList.size());
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 3).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 6).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 6).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(7, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 2).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 2).get()));
    }
    @Test
    public void getAllowedTargetListForLineAtSomeAllowance() {
        //given
        Mockito.when(skill.getTargetsType()).thenReturn(SkillTargetsType.LINE);
        Mockito.when(skill.getRange()).thenReturn(1);
        Mockito.when(skill.getAllowedSides()).thenReturn(SOME_SIDES_ALLOWED);

        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(skill);

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(3, allowedTargetList.size());
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 3).get()));
    }

    @Test
    public void getAllowedTargetListForLineRangeTwoAtSomeAllowance() {
        //given
        Mockito.when(skill.getTargetsType()).thenReturn(SkillTargetsType.LINE);
        Mockito.when(skill.getRange()).thenReturn(2);
        Mockito.when(skill.getAllowedSides()).thenReturn(SOME_SIDES_ALLOWED);

        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(skill);

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(6, allowedTargetList.size());
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 3).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 6).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 2).get()));
    }

    @Test
    public void getAllowedTargetListForAnywhere() {
        //given
        Mockito.when(skill.getTargetsType()).thenReturn(SkillTargetsType.ANYWHERE);
        Mockito.when(skill.getRange()).thenReturn(1);
        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(skill);

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(7, allowedTargetList.size());
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 4).get()));
    }

    @Test
    public void getAllowedTargetListForAnywhereRangeTwo() {
        //given
        Mockito.when(skill.getTargetsType()).thenReturn(SkillTargetsType.ANYWHERE);
        Mockito.when(skill.getRange()).thenReturn(2);
        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(skill);

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(19, allowedTargetList.size());
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 6).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 6).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 6).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 5).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 4).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 3).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 2).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 2).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 2).get()));

    }
}