package com.slob.aqfw.level.skill.heroes.beast;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BerserkSkillTest {
    private MainService mainService;
    private LevelService service;
    private Character beast;

    @Before
    public void setup() {
        mainService = CommonSkillSetUp.getMainServiceInstance();
        service = mainService.getService().getLevelService();
        service.generateLevel();
        beast = service.createCharacter(CharacterFactory.BEAST_ID);
        service.addCharacter(beast, 4, 4);
    }


    @Test
    public void oneTurnSkillUse() {
        //when
        service.performSkill(beast.getSkills().get(4), mainService.getService().getInjector().getInstance(CellHelper.class).getCharacterCell(beast));

        //then
        Assert.assertFalse(beast.getActiveEffects().isEmpty());
        Assert.assertEquals(3, beast.getActiveEffects().size());
        Assert.assertSame(EffectType.IGNORE_OBSTACLE, beast.getActiveEffects().get(0).getEffectType());
        Assert.assertSame(EffectType.EN_PASSANT, beast.getActiveEffects().get(1).getEffectType());
        Assert.assertSame(EffectType.MOVE_POINT_CHANGE, beast.getActiveEffects().get(2).getEffectType());
    }


    @Test
    public void twoTurnSkillUse() {
        //when
        service.performSkill(beast.getSkills().get(4), mainService.getService().getInjector().getInstance(CellHelper.class).getCharacterCell(beast));
        service.endTurn();

        //then
        Assert.assertTrue(beast.getActiveEffects().isEmpty());
    }
}