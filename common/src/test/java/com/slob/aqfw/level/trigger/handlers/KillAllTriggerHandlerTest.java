package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.level.trigger.Trigger;
import com.slob.aqfw.level.trigger.TriggerFactory;
import com.slob.aqfw.level.trigger.TriggerManager;
import com.slob.aqfw.level.trigger.TriggerRepository;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KillAllTriggerHandlerTest {
    private Injector injector;
    private LevelService levelService;
    private Character knight;
    private Character ork;

    @Before
    public void setUp() {
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        injector = mainService.getService().getInjector();
        levelService = mainService.getService().getLevelService();
        levelService.generateLevel();
        CharacterFactory characterFactory = injector.getInstance(CharacterFactory.class);
        knight = characterFactory.createCharacter("KNIGHT_ID", "KNIGHT_ID");
        ork = characterFactory.createCharacter("ORK_ID", "ORK_ID");
        Trigger trigger = injector.getInstance(TriggerFactory.class).createTrigger("TEST_KILL_ALL");
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);
        triggerRepository.getTriggers().add(trigger);

    }

    @Test
    public void oneEnemyLive() {
        //given
        levelService.addCharacter(ork, 3, 4);
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertFalse(triggerRepository.getTriggers().isEmpty());
        Assert.assertEquals(GameState.MAIN_MENU, injector.getInstance(GameStateCache.class).getGameState());
    }

    @Test
    public void noEnemyLive() {
        //given
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertEquals(1, triggerRepository.getTriggers().size());
        Assert.assertEquals(GameState.WON_LEVEL, injector.getInstance(GameStateCache.class).getGameState());
    }

    @Test
    public void onlyGoodLive() {
        //given
        levelService.addCharacter(knight, 3, 4);
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertEquals(1, triggerRepository.getTriggers().size());
        Assert.assertEquals(GameState.WON_LEVEL, injector.getInstance(GameStateCache.class).getGameState());
    }
}