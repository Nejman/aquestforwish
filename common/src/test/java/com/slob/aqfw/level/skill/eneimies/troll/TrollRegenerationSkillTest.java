package com.slob.aqfw.level.skill.eneimies.troll;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TrollRegenerationSkillTest {
    private LogicService logicService;
    private Character troll;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        troll = logicService.getLevelService().createCharacter(CharacterFactory.TROLL_ID);
        logicService.getLevelService().addCharacter(troll, 5, 4);
    }

    @Test
    public void skillTest() {
        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(10, troll.getCurrentHealth());
    }


    @Test
    public void damagedCharacterSkillTest() {
        //given
        CharacterDamageManager damageManager = logicService.getInjector().getInstance(CharacterDamageManager.class);
        damageManager.changeCharacterHealth(new Effect(EffectType.HEALTH_CHANGE, -3), troll, null);

        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(9, troll.getCurrentHealth());
    }

    @Test
    public void damagedCharacterSkillNotOverMaxTest() {
        //given
        CharacterDamageManager damageManager = logicService.getInjector().getInstance(CharacterDamageManager.class);
        damageManager.changeCharacterHealth(new Effect(EffectType.HEALTH_CHANGE, -3), troll, null);

        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(10, troll.getCurrentHealth());
    }
}