package com.slob.aqfw.level.effect;

import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectAdditionalValueType;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EffectManagerTest {
    private Character character;
    private EffectManager manager;

    private static Character getDefaultCharacter() {
        return new Character(null, 0, null, null, null, 0, 5, 0, null);
    }

    @Before
    public void setUp() {
        CharacterRepository repository;
        repository = new CharacterRepository();
        character = getDefaultCharacter();
        repository.getCharacterList().add(character);
        manager = new EffectManager(repository, null);
    }

    @Test
    public void removeEndingEffect() {
        //given
        Effect effect = new Effect(EffectType.BASE_ATTACK, 0);
        effect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, 1);
        character.getActiveEffects().add(effect);

        //when
        manager.manageEffectsAtEndOfTurn();

        //then
        Assert.assertEquals(0, character.getActiveEffects().size());
    }

    @Test
    public void doNotRemoveLastingEffect() {
        //given
        Effect effect = new Effect(EffectType.BASE_ATTACK, 0);
        effect.getAdditionalValues().put(EffectAdditionalValueType.DURATION, 2);
        character.getActiveEffects().add(effect);

        //when
        manager.manageEffectsAtEndOfTurn();

        //then
        Assert.assertEquals(1, character.getActiveEffects().size());
        Assert.assertSame(effect, character.getActiveEffects().get(0));
        Assert.assertEquals(1, effect.getAdditionalValues().get(EffectAdditionalValueType.DURATION));
    }
}