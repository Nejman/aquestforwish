package com.slob.aqfw.level.skill;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SkillDelayManagerTest {
    private Character character;
    private LogicService logicService;

    @Before
    public void setUp() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        logicService = mainService.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(character, 0, 0);
        logicService.getLevelService().performSkill(character.getSkills().get(4), logicService.getInjector().getInstance(CellHelper.class).getCharacterCell(character));
    }

    @Test
    public void updateDelay() {
        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(2, character.getSkills().get(4).getCurrentDelay());
    }

    @Test
    public void updateDelayMultipleTimes() {
        //when
        for (int i = 0; i < 5; i++) {
            logicService.getLevelService().endTurn();
        }

        //then
        Assert.assertEquals(0, character.getSkills().get(4).getCurrentDelay());
    }
}