package com.slob.aqfw.level.skill.item;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterArmourManager;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterThreatManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class BarrierSealItemTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);
        Item item = new BarrierSealItem();
        logicService.getInjector().getInstance(ItemRepository.class).getCharacterItemMap().put(character, item);
    }

    @Test
    public void use() {
        //when
        logicService.getLevelService().performSkill(character.getSkills().get(6), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(5, 4).get());

        //then
        Assert.assertEquals(5, CharacterThreatManager.getThreatLevel(character));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.LEFT));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.LEFT_DOWN));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT_DOWN));
        Assert.assertEquals(1, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT));
        Assert.assertEquals(1, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT_UP));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.LEFT_UP));
    }

    @Test
    public void getRange() {
        //given
        SkillTargetingHelper skillTargetingHelper = logicService.getInjector().getInstance(SkillTargetingHelper.class);

        //when
        List<Cell> allowedTargetList = skillTargetingHelper.getAllowedTargetList(character.getSkills().get(6));

        //then
        Assert.assertEquals(1, allowedTargetList.size());
        Assert.assertSame(character, allowedTargetList.get(0).getCharacter().get());
    }
}