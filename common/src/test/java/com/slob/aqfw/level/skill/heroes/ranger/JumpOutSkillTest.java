package com.slob.aqfw.level.skill.heroes.ranger;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class JumpOutSkillTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.RANGER_ID);
        logicService.getLevelService().addCharacter(character, 0, 0);
    }

    @Test
    public void getRange() {
        //given
        Character ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        Character ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        Character ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 3, 3);
        logicService.getLevelService().addCharacter(ork2, 2, 5);
        logicService.getLevelService().addCharacter(ork3, 4, 8);

        //when
        List<Cell> allowedCellsForSkill = logicService.getLevelService().getAllowedCellsForSkill(character.getSkills().get(4));

        //then
        Assert.assertEquals(18, allowedCellsForSkill.size());
    }


    @Test
    public void use() {
        //given
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);

        //when
        logicService.getLevelService().performSkill(character.getSkills().get(4), cellHelper.getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(0, 0).get().getCharacter().isPresent());
    }
}