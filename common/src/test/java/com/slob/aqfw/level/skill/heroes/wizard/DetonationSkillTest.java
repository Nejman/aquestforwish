package com.slob.aqfw.level.skill.heroes.wizard;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DetonationSkillTest {
    private LogicService logicService;
    private Character wizard;
    private Character knight;
    private Character target;
    private Character ork2;
    private Character ork3;
    private Character ork4;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        wizard = logicService.getLevelService().createCharacter(CharacterFactory.MAGE_ID);
        logicService.getLevelService().addCharacter(wizard, 4, 4);
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(knight, 2, 5);
        ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork2, 1, 5);
        ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork3, 1, 3);
        ork4 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork4, 1, 1);
    }

    @Test
    public void attackEnemyStillAlive() {
        //given
        target = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(target, 2, 4);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell cell = cellHelper.getCellAtCoordinates(2, 4).get();

        //when
        logicService.getLevelService().performSkill(wizard.getSkills().get(5), cell);

        //then
        Assert.assertEquals(15, knight.getCurrentHealth());
        Assert.assertEquals(2, target.getCurrentHealth());
        Assert.assertEquals(5, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertEquals(5, ork4.getCurrentHealth());
    }

    @Test
    public void attackEnemyThatDies() {
        //given
        target = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        logicService.getLevelService().addCharacter(target, 2, 4);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell cell = cellHelper.getCellAtCoordinates(2, 4).get();

        //when
        logicService.getLevelService().performSkill(wizard.getSkills().get(5), cell);

        //then
        Assert.assertEquals(13, knight.getCurrentHealth());
        Assert.assertFalse(cell.getCharacter().isPresent());
        Assert.assertEquals(3, ork2.getCurrentHealth());
        Assert.assertEquals(3, ork3.getCurrentHealth());
        Assert.assertEquals(5, ork4.getCurrentHealth());
    }
}