package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.level.aura.Aura;
import com.slob.aqfw.level.aura.AuraRepository;
import com.slob.aqfw.level.aura.skillauras.FireballAura;
import com.slob.aqfw.level.aura.skillauras.HailAura;
import com.slob.aqfw.level.trigger.result.TriggerResult;
import com.slob.aqfw.level.trigger.result.TriggerResultFactory;
import com.slob.aqfw.level.trigger.result.TriggerResultHandler;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class AddAurasResultResultHandlerTest {

    @Test
    public void handle() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        mainService.getService().getLevelService().generateLevel();
        Injector injector = mainService.getService().getInjector();
        TriggerResult result = injector.getInstance(TriggerResultFactory.class).createTriggerResult("TEST_ADD_AURAS_RESULT");

        //when
        injector.getInstance(TriggerResultHandler.class).handleResult(result);

        //then
        List<Aura> auras = injector.getInstance(AuraRepository.class).getAuras();
        Assert.assertEquals(4, auras.size());
        Assert.assertEquals(FireballAura.class, auras.get(0).getClass());
        Assert.assertEquals(FireballAura.class, auras.get(1).getClass());
        Assert.assertEquals(HailAura.class, auras.get(2).getClass());
        Assert.assertEquals(HailAura.class, auras.get(3).getClass());
    }
}