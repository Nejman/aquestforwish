package com.slob.aqfw.level.skill.eneimies.troll;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TrollAttackSkillTest {
    private LogicService logicService;
    private Character troll;
    private Character ork1;
    private Character ork2;
    private Character ork3;
    private Character ork4;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        troll = logicService.getLevelService().createCharacter(CharacterFactory.TROLL_ID);
        logicService.getLevelService().addCharacter(troll, 5, 4);
        ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        ork4 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 4, 5);
        logicService.getLevelService().addCharacter(ork2, 4, 4);
        logicService.getLevelService().addCharacter(ork3, 4, 3);
        logicService.getLevelService().addCharacter(ork4, 5, 3);

    }

    @Test
    public void skillTargetTest() {
        //given
        SkillTargetingHelper skillTargetingHelper = logicService.getInjector().getInstance(SkillTargetingHelper.class);

        //when
        List<Cell> allowedTargetList = skillTargetingHelper.getAllowedTargetList(troll.getSkills().get(0));

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(1, allowedTargetList.size());
        Assert.assertSame(logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(4, 4).get(), allowedTargetList.get(0));
    }

    @Test
    public void skillTest() {
        //given
        Cell target = logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(4, 4).get();

        //when
        logicService.getLevelService().performSkill(troll.getSkills().get(0), target);

        //then
        Assert.assertEquals(4, ork1.getCurrentHealth()); // side with armour
        Assert.assertEquals(3, ork2.getCurrentHealth()); // no armour side
        Assert.assertEquals(3, ork3.getCurrentHealth()); // no armour side
        Assert.assertEquals(5, ork4.getCurrentHealth()); // outside attack range
    }

}