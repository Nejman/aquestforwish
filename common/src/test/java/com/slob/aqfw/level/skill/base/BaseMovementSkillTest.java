package com.slob.aqfw.level.skill.base;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.DescriptorRepository;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterMovementManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.hex.CellRepository;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

public class BaseMovementSkillTest {
    private Character character;
    private CellHelper cellHelper;
    private SkillTargetingHelper targetingHelper;

    @Before
    public void setUp() {
        character = new Character(null, 0, ImmutableMap.of(), ImmutableMap.of(), null, 2, 5, 0, null);
        CellRepository repository = new CellRepository();
        List<Cell> cells = HexCreator.createHexField(5);
        repository.setCells(cells);
        cellHelper = new CellHelper(repository);
        Optional<Cell> attackerCell = cellHelper.getCellAtCoordinates(5, 4);
        attackerCell.get().setCharacter(character);
        CharacterMovementManager characterMovementManager = new CharacterMovementManager(cellHelper);


        Injector injector = Mockito.mock(Injector.class);
        Mockito.when(injector.getInstance(CharacterMovementManager.class)).thenReturn(characterMovementManager);
        targetingHelper = new SkillTargetingHelper(characterMovementManager, cellHelper);

        DescriptorRepository descriptorRepository = new DescriptorRepository();
        DescriptorReader descriptorReader = new DescriptorReader(descriptorRepository);

        Skill skill = BaseMovementSkill.getInstance(character, injector, descriptorReader.getById(SkillDescriptor.class, "BASE_MOVEMENT_SKILL"));
        character.setSkills(ImmutableList.of(skill));
    }

    @Test
    public void getAllowedCells() {
        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(character.getSkills().get(0));

        //then
        Assert.assertEquals(19, allowedTargetList.size());

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 6).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 6).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 6).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 5).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(7, 4).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 2).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 2).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 2).get()));

        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(3, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(6, 3).get()));
    }

    @Test
    public void testMovement() {
        //when
        character.getSkills().get(0).apply(cellHelper.getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertFalse(cellHelper.getCellAtCoordinates(5, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertSame(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().get(), character);
    }
}