package com.slob.aqfw.level.skill.item;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.core.skill.PointType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterPointManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class DiademOfFrostItemTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);
        Item item = new DiademOfFrostItem();
        logicService.getInjector().getInstance(ItemRepository.class).getCharacterItemMap().put(character, item);
    }

    @Test
    public void use() {
        //given
        Character ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork, 4, 4);

        //when
        logicService.getLevelService().performSkill(character.getSkills().get(6), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertEquals(0, CharacterPointManager.getRemainingPoints(PointType.ACTION, ork));
        Assert.assertEquals(0, CharacterPointManager.getRemainingPoints(PointType.MOVEMENT, ork));
        Assert.assertEquals(0, CharacterPointManager.getRemainingPoints(PointType.ATTACK, ork));
    }

    @Test
    public void range() {
        //given
        SkillTargetingHelper skillTargetingHelper = logicService.getInjector().getInstance(SkillTargetingHelper.class);

        //when
        List<Cell> allowedTargetList = skillTargetingHelper.getAllowedTargetList(character.getSkills().get(6));

        //then
        Assert.assertEquals(37, allowedTargetList.size());

    }
}