package com.slob.aqfw.level.skill.heroes.ranger;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterMovementManager;
import com.slob.aqfw.entities.characters.CharacterSpeedManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PinDownSkillTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.RANGER_ID);
        logicService.getLevelService().addCharacter(character, 4, 4);
    }

    @Test
    public void use() {
        //given
        Character ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork, 2, 4);
        CharacterMovementManager movementManager = logicService.getInjector().getInstance(CharacterMovementManager.class);

        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().performSkill(character.getSkills().get(3), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(2, 4).get());

        //then
        Assert.assertEquals(3, ork.getCurrentHealth());
        Assert.assertEquals(0, CharacterSpeedManager.getCharacterSpeed(ork));
        Assert.assertEquals(1, movementManager.getAllowedCellsForMovement(CharacterSpeedManager.getCharacterSpeed(ork), ork).size());
    }
}