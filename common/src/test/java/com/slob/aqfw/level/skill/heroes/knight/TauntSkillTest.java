package com.slob.aqfw.level.skill.heroes.knight;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterArmourManager;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterThreatManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

public class TauntSkillTest {
    @Test
    public void testTauntSkill() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = service.getService();
        logicService.getLevelService().generateLevel();
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);

        //when
        logicService.getLevelService().performSkill(character.getSkills().get(4), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(5, 4).get());

        //then
        Assert.assertEquals(15, CharacterThreatManager.getThreatLevel(character));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.LEFT));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.LEFT_DOWN));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT_DOWN));
        Assert.assertEquals(1, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT));
        Assert.assertEquals(1, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT_UP));
        Assert.assertEquals(2, CharacterArmourManager.getArmourOnSide(character, Side.LEFT_UP));
    }
}