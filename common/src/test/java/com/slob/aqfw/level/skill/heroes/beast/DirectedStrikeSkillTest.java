package com.slob.aqfw.level.skill.heroes.beast;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DirectedStrikeSkillTest {
    private LogicService logicService;
    private Character beast;
    private Character ork;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        beast = logicService.getLevelService().createCharacter(CharacterFactory.BEAST_ID);
        logicService.getLevelService().addCharacter(beast, 4, 4);
        ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork, 4, 5);

    }

    @Test
    public void skillTest() {
        //given
        Cell target = logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(4, 5).get();

        //when
        logicService.getLevelService().performSkill(beast.getSkills().get(3), target);

        //then
        Assert.assertEquals(5, ork.getCurrentHealth());
        logicService.getLevelService().endTurn();
        Assert.assertEquals(3, ork.getCurrentHealth());
        logicService.getLevelService().endTurn();
        Assert.assertEquals(1, ork.getCurrentHealth());
        logicService.getLevelService().endTurn();
        Assert.assertEquals(1, ork.getCurrentHealth());
    }
}