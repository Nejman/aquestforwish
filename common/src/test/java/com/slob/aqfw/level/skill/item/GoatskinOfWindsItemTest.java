package com.slob.aqfw.level.skill.item;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GoatskinOfWindsItemTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);
        Item item = new GoatskinOfWindsItem();
        logicService.getInjector().getInstance(ItemRepository.class).getCharacterItemMap().put(character, item);
    }

    @Test
    public void use() {
        //given
        Character ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 4, 4);
        Character ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork2, 3, 4);
        Character ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork3, 4, 5);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);

        //when
        logicService.getLevelService().performSkill(character.getSkills().get(6), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(5, 4).get());

        //then
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 4).get().getCharacter().isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 5).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 6).get().getCharacter().isPresent());
        Assert.assertEquals(3, ork1.getCurrentHealth());
        Assert.assertEquals(3, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
    }

}