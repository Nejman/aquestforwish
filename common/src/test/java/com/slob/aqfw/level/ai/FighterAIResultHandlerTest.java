package com.slob.aqfw.level.ai;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillUseResult;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class FighterAIResultHandlerTest {
    private Character ork;
    private Character knight;
    private LogicService logicService;
    private CellHelper cellHelper;

    @Before
    public void setUp() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        logicService = mainService.getService();
        logicService.getLevelService().generateLevel();
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(knight, 0, 0);
        logicService.getInjector().getInstance(CellThreatLevelManager.class).updateCellsThreatLevels();
        cellHelper = logicService.getInjector().getInstance(CellHelper.class);
    }

    @Test
    public void handleAIMovement() {
        //given
        logicService.getLevelService().addCharacter(ork, 4, 4);
        FighterAIHandler ai = logicService.getInjector().getInstance(FighterAIHandler.class);

        //when
        Optional<SkillUseResult> skillUseResult = ai.handleAI(ork);

        //then
        Assert.assertTrue(skillUseResult.isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 1).get().getCharacter().isPresent());
        Assert.assertSame(ork, cellHelper.getCellAtCoordinates(1, 1).get().getCharacter().get());
    }

    @Test
    public void handleAIMovementWithNearerTarget() {
        //given
        logicService.getLevelService().addCharacter(ork, 4, 5);
        Character beast = logicService.getLevelService().createCharacter(CharacterFactory.BEAST_ID);
        logicService.getLevelService().addCharacter(beast, 2, 5);
        FighterAIHandler ai = logicService.getInjector().getInstance(FighterAIHandler.class);
        logicService.getInjector().getInstance(CellThreatLevelManager.class).updateCellsThreatLevels();

        //when
        Optional<SkillUseResult> skillUseResult = ai.handleAI(ork);

        //then
        Assert.assertTrue(skillUseResult.isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(1, 1).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 5).get().getCharacter().isPresent());
        Assert.assertSame(ork, cellHelper.getCellAtCoordinates(3, 5).get().getCharacter().get());
    }

    @Test
    public void handleAIAttack() {
        //given
        logicService.getLevelService().addCharacter(ork, 1, 1);
        FighterAIHandler ai = logicService.getInjector().getInstance(FighterAIHandler.class);

        //when
        Optional<SkillUseResult> skillUseResult = ai.handleAI(ork);

        //then
        Assert.assertTrue(skillUseResult.isPresent());
        Assert.assertEquals(13, knight.getCurrentHealth());
    }


    @Test
    public void handleAITwoActions() {
        //given
        logicService.getLevelService().addCharacter(ork, 4, 4);
        FighterAIHandler ai = logicService.getInjector().getInstance(FighterAIHandler.class);

        //when
        Optional<SkillUseResult> skillUseResult1 = ai.handleAI(ork);
        Optional<SkillUseResult> skillUseResult2 = ai.handleAI(ork);

        //then
        Assert.assertTrue(skillUseResult1.isPresent());
        Assert.assertTrue(skillUseResult2.isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 1).get().getCharacter().isPresent());
        Assert.assertSame(ork, cellHelper.getCellAtCoordinates(1, 1).get().getCharacter().get());
        Assert.assertEquals(13, knight.getCurrentHealth());
    }

    @Test
    public void handleAIFourActions() {
        //given
        logicService.getLevelService().addCharacter(ork, 4, 4);
        FighterAIHandler ai = logicService.getInjector().getInstance(FighterAIHandler.class);

        //when
        Optional<SkillUseResult> skillUseResult1 = ai.handleAI(ork);
        Optional<SkillUseResult> skillUseResult2 = ai.handleAI(ork);
        Optional<SkillUseResult> skillUseResult3 = ai.handleAI(ork);
        Optional<SkillUseResult> skillUseResult4 = ai.handleAI(ork);

        //then
        Assert.assertTrue(skillUseResult1.isPresent());
        Assert.assertTrue(skillUseResult2.isPresent());
        Assert.assertTrue(skillUseResult3.isPresent());
        Assert.assertFalse(skillUseResult4.isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 1).get().getCharacter().isPresent());
        Assert.assertSame(ork, cellHelper.getCellAtCoordinates(1, 1).get().getCharacter().get());
        Assert.assertEquals(11, knight.getCurrentHealth());
    }
}