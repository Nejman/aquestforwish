package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.level.trigger.result.TriggerResult;
import com.slob.aqfw.level.trigger.result.TriggerResultFactory;
import com.slob.aqfw.level.trigger.result.TriggerResultHandler;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

public class WinResultResultHandlerTest {

    @Test
    public void handle() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        mainService.getService().getLevelService().generateLevel();
        Injector injector = mainService.getService().getInjector();
        TriggerResult result = injector.getInstance(TriggerResultFactory.class).createTriggerResult("TEST_WIN_RESULT");

        //when
        injector.getInstance(TriggerResultHandler.class).handleResult(result);

        //then
        Assert.assertEquals(GameState.WON_LEVEL, injector.getInstance(GameStateCache.class).getGameState());
    }
}