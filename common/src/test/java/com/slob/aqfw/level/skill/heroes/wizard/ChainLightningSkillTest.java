package com.slob.aqfw.level.skill.heroes.wizard;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ChainLightningSkillTest {
    private LogicService logicService;
    private Character wizard;
    private Character knight;
    private Character ork1;
    private Character ork2;
    private Character ork3;
    private Character ork4;
    private Character ork5;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        wizard = logicService.getLevelService().createCharacter(CharacterFactory.MAGE_ID);
        logicService.getLevelService().addCharacter(wizard, 4, 4);
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(knight, 2, 5);
        ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 2, 4);
        ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork2, 1, 5);
        ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork3, 1, 3);
        ork4 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork4, 1, 1);
        ork5 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork5, 2, 6);
    }

    @Test
    public void use() {
        //given
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell cell = cellHelper.getCellAtCoordinates(2, 4).get();

        //when
        logicService.getLevelService().performSkill(wizard.getSkills().get(4), cell);

        //then
        Assert.assertEquals(14, knight.getCurrentHealth());
        Assert.assertEquals(4, ork1.getCurrentHealth());
        Assert.assertEquals(4, ork2.getCurrentHealth());
        Assert.assertEquals(4, ork3.getCurrentHealth());
        Assert.assertEquals(5, ork4.getCurrentHealth());
        Assert.assertEquals(4, ork5.getCurrentHealth());
    }
}