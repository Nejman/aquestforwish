package com.slob.aqfw.level.skill;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.item.DiademOfFrostItem;
import com.slob.aqfw.level.skill.item.ItemRepository;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KnockbackManagerTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);
        Item item = new DiademOfFrostItem();
        logicService.getInjector().getInstance(ItemRepository.class).getCharacterItemMap().put(character, item);
    }

    @Test
    public void knockback() {
        //given
        Character ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork, 4, 4);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell characterCell = cellHelper.getCharacterCell(character);

        //when
        logicService.getInjector().getInstance(KnockbackManager.class).knockback(ork, characterCell);

        //then
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 4).get().getCharacter().isPresent());
        Assert.assertEquals(5, ork.getCurrentHealth());
    }

    @Test
    public void knockbackWithCharacterBlocking() {
        //given
        Character ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 4, 4);
        Character ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork2, 3, 4);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell characterCell = cellHelper.getCharacterCell(character);

        //when
        logicService.getInjector().getInstance(KnockbackManager.class).knockback(ork1, characterCell);

        //then
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(3, 4).get().getCharacter().isPresent());
        Assert.assertEquals(3, ork1.getCurrentHealth());
        Assert.assertEquals(3, ork2.getCurrentHealth());
    }
}