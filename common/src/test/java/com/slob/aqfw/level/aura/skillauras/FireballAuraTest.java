package com.slob.aqfw.level.aura.skillauras;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.aura.AuraFactory;
import com.slob.aqfw.level.aura.AuraRepository;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FireballAuraTest {
    private LogicService logicService;
    private Character ork1;
    private Character ork2;
    private Character ork3;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        ork1 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork1, 4, 4);
        ork2 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork2, 3, 4);
        ork3 = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork3, 4, 5);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell cell = cellHelper.getCellAtCoordinates(3, 4).get();
        logicService.getInjector().getInstance(AuraFactory.class).createAura(AuraFactory.FIREBALL, cell);
    }

    @Test
    public void oneTurnFireball() {
        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(5, ork1.getCurrentHealth());
        Assert.assertEquals(5, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertEquals(1, logicService.getInjector().getInstance(AuraRepository.class).getAuras().size());
    }

    @Test
    public void twoTurnFireball() {
        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(3, ork1.getCurrentHealth());
        Assert.assertEquals(3, ork2.getCurrentHealth());
        Assert.assertEquals(5, ork3.getCurrentHealth());
        Assert.assertEquals(0, logicService.getInjector().getInstance(AuraRepository.class).getAuras().size());
    }
}