package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.level.trigger.Trigger;
import com.slob.aqfw.level.trigger.TriggerFactory;
import com.slob.aqfw.level.trigger.TriggerManager;
import com.slob.aqfw.level.trigger.TriggerRepository;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KillSpecificTriggerHandlerTest {
    private Injector injector;
    private LevelService levelService;
    private Character goblin;
    private Character ork;

    @Before
    public void setUp() {
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        injector = mainService.getService().getInjector();
        levelService = mainService.getService().getLevelService();
        levelService.generateLevel();
        CharacterFactory characterFactory = injector.getInstance(CharacterFactory.class);
        goblin = characterFactory.createCharacter("GOBLIN_WARRIOR_ID", "GOBLIN_WARRIOR_ID");
        ork = characterFactory.createCharacter("ORK_ID", "ORK_ID");
        Trigger trigger = injector.getInstance(TriggerFactory.class).createTrigger("TEST_KILL_SPECIFIC");
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);
        triggerRepository.getTriggers().add(trigger);

    }

    @Test
    public void oneEnemyLive() {
        //given
        levelService.addCharacter(ork, 3, 4);
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertFalse(triggerRepository.getTriggers().isEmpty());
        Assert.assertEquals(GameState.MAIN_MENU, injector.getInstance(GameStateCache.class).getGameState());
    }

    @Test
    public void noEnemyLive() {
        //given
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertEquals(1, triggerRepository.getTriggers().size());
        Assert.assertEquals(GameState.WON_LEVEL, injector.getInstance(GameStateCache.class).getGameState());
    }

    @Test
    public void onlyOtherLive() {
        //given
        levelService.addCharacter(goblin, 3, 4);
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertEquals(1, triggerRepository.getTriggers().size());
        Assert.assertEquals(GameState.WON_LEVEL, injector.getInstance(GameStateCache.class).getGameState());
    }
}