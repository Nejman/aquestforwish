package com.slob.aqfw.level.skill.heroes.medic;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.effect.Effect;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RegenerationGrenadeSkillTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.HEAL_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);
    }

    @Test
    public void use() {
        //given
        CharacterDamageManager characterDamageManager = logicService.getInjector().getInstance(CharacterDamageManager.class);
        Character knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(knight, 4, 4);
        Effect effectKnight = new Effect(EffectType.HEALTH_CHANGE, -1);
        characterDamageManager.changeCharacterHealth(effectKnight, knight, null);

        Character beast = logicService.getLevelService().createCharacter(CharacterFactory.BEAST_ID);
        logicService.getLevelService().addCharacter(beast, 2, 5);
        Effect effectBeast = new Effect(EffectType.HEALTH_CHANGE, -3);
        characterDamageManager.changeCharacterHealth(effectBeast, beast, null);

        Character ranger = logicService.getLevelService().createCharacter(CharacterFactory.RANGER_ID);
        logicService.getLevelService().addCharacter(ranger, 1, 2);
        Effect effectRanger = new Effect(EffectType.HEALTH_CHANGE, -2);
        characterDamageManager.changeCharacterHealth(effectRanger, ranger, null);

        //when
        logicService.getLevelService().performSkill(character.getSkills().get(4), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(3, 4).get());

        //then
        Assert.assertEquals(15, knight.getCurrentHealth());
        Assert.assertEquals(9, beast.getCurrentHealth());
        Assert.assertEquals(2, ranger.getCurrentHealth());
    }
}