package com.slob.aqfw.level.skill.eneimies.warboss;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;


public class WarbossSkillTest {
    private LogicService logicService;
    private Character warboss;
    private SkillTargetingHelper targetingHelper;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        warboss = logicService.getLevelService().createCharacter(CharacterFactory.WARBOSS_ID);
        logicService.getLevelService().addCharacter(warboss, 5, 4);
        targetingHelper = logicService.getInjector().getInstance(SkillTargetingHelper.class);
    }

    @Test
    public void testUnobstructedRange() {
        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(warboss.getSkills().get(0));

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(37, allowedTargetList.size());
    }


    @Test
    public void testObstructedRange() {
        //given
        Character obstacle1 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character obstacle2 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character obstacle3 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character obstacle4 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character obstacle5 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        Character obstacle6 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        logicService.getLevelService().addCharacter(obstacle1, 3, 4);
        logicService.getLevelService().addCharacter(obstacle2, 2, 4);
        logicService.getLevelService().addCharacter(obstacle3, 2, 5);
        logicService.getLevelService().addCharacter(obstacle4, 3, 5);
        logicService.getLevelService().addCharacter(obstacle5, 4, 5);
        logicService.getLevelService().addCharacter(obstacle6, 3, 5);

        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(warboss.getSkills().get(0));

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(33, allowedTargetList.size());
        Assert.assertFalse(allowedTargetList.contains(logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(4, 4).get()));
    }


    @Test
    public void testSkillPerform() {
        //given
        Cell cell = logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(3, 4).get();

        //when
        logicService.getLevelService().performSkill(warboss.getSkills().get(0), cell);

        //then
        Assert.assertTrue(cell.getCharacter().isPresent());
        Assert.assertTrue(cell.getNeighbours().get(Side.LEFT).getCharacter().isPresent());
        Assert.assertTrue(cell.getNeighbours().get(Side.LEFT_DOWN).getCharacter().isPresent());
        Assert.assertTrue(cell.getNeighbours().get(Side.RIGHT_DOWN).getCharacter().isPresent());
    }


    @Test
    public void testSkillPerformOnTakenCell() {
        //given
        Cell cell = logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(5, 4).get();

        //when
        logicService.getLevelService().performSkill(warboss.getSkills().get(0), cell);

        //then
        Assert.assertTrue(cell.getCharacter().isPresent());
        Assert.assertTrue(cell.getNeighbours().get(Side.LEFT).getCharacter().isPresent());
        Assert.assertTrue(cell.getNeighbours().get(Side.LEFT_DOWN).getCharacter().isPresent());
        Assert.assertTrue(cell.getNeighbours().get(Side.RIGHT_DOWN).getCharacter().isPresent());
        Assert.assertTrue(cell.getNeighbours().get(Side.RIGHT).getCharacter().isPresent());
    }
}