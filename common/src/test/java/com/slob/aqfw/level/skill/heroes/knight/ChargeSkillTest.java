package com.slob.aqfw.level.skill.heroes.knight;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ChargeSkillTest {

    private LogicService logicService;
    private CellHelper cellHelper;
    private Character goblin1;
    private Character goblin2;
    private Character goblin3;
    private Character ork;
    private Character knight;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        goblin1 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        goblin2 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        goblin3 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        logicService.getLevelService().addCharacter(knight, 5, 4);
        logicService.getLevelService().addCharacter(goblin1, 4, 4);
        logicService.getLevelService().addCharacter(goblin2, 2, 5);
        logicService.getLevelService().addCharacter(goblin3, 0, 4);
        logicService.getLevelService().addCharacter(ork, 2, 4);
    }

    @Test
    public void targetingTest() {
        //when
        List<Cell> cells = logicService.getLevelService().getAllowedCellsForSkill(knight.getSkills().get(2));

        //then
        Assert.assertEquals(6, cells.size());
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(4, 5).get()));
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(4, 3).get()));
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(5, 5).get()));
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(6, 4).get()));
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(5, 3).get()));
    }

    @Test
    public void targetingTestInCorner() {
        //given
        cellHelper.getCharacterCell(knight).setCharacter(null);
        cellHelper.getCellAtCoordinates(0, 0).get().setCharacter(knight);

        //when
        List<Cell> cells = logicService.getLevelService().getAllowedCellsForSkill(knight.getSkills().get(2));

        //then
        Assert.assertEquals(3, cells.size());
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(0, 1).get()));
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(1, 1).get()));
        Assert.assertTrue(cells.contains(cellHelper.getCellAtCoordinates(1, 0).get()));
    }

    @Test
    public void testCharge() {
        //when
        logicService.getLevelService().performSkill(knight.getSkills().get(2), cellHelper.getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(5, 4).get().getCharacter().isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(3, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(2, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(1, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(0, 4).get().getCharacter().isPresent());
        Assert.assertSame(knight, cellHelper.getCellAtCoordinates(2, 4).get().getCharacter().get());
        Assert.assertSame(ork, cellHelper.getCellAtCoordinates(1, 4).get().getCharacter().get());
        Assert.assertSame(goblin3, cellHelper.getCellAtCoordinates(0, 4).get().getCharacter().get());
        Assert.assertEquals(3, ork.getCurrentHealth());
    }

    @Test
    public void testChargeAtWall() {
        //given
        cellHelper.getCharacterCell(ork).setCharacter(null);
        cellHelper.getCellAtCoordinates(6, 4).get().setCharacter(ork);
        //when
        logicService.getLevelService().performSkill(knight.getSkills().get(2), cellHelper.getCellAtCoordinates(6, 4).get());

        //then
        Assert.assertFalse(cellHelper.getCellAtCoordinates(5, 4).get().getCharacter().isPresent());
        Assert.assertFalse(cellHelper.getCellAtCoordinates(6, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(7, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(8, 4).get().getCharacter().isPresent());
        Assert.assertSame(knight, cellHelper.getCellAtCoordinates(7, 4).get().getCharacter().get());
        Assert.assertSame(ork, cellHelper.getCellAtCoordinates(8, 4).get().getCharacter().get());
        Assert.assertEquals(3, ork.getCurrentHealth());
    }

    @Test
    public void testChargeNoEnemies() {
        //when
        logicService.getLevelService().performSkill(knight.getSkills().get(2), cellHelper.getCellAtCoordinates(6, 4).get());

        //then
        Assert.assertFalse(cellHelper.getCellAtCoordinates(5, 4).get().getCharacter().isPresent());
        Assert.assertTrue(cellHelper.getCellAtCoordinates(8, 4).get().getCharacter().isPresent());
        Assert.assertSame(knight, cellHelper.getCellAtCoordinates(8, 4).get().getCharacter().get());
    }
}