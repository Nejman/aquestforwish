package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.trigger.result.TriggerResult;
import com.slob.aqfw.level.trigger.result.TriggerResultFactory;
import com.slob.aqfw.level.trigger.result.TriggerResultHandler;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

public class SpawnCharactersResultResultHandlerTest {

    @Test
    public void handle() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        mainService.getService().getLevelService().generateLevel();
        Injector injector = mainService.getService().getInjector();
        TriggerResult result = injector.getInstance(TriggerResultFactory.class).createTriggerResult("TEST_SPAWN_CHARACTERS_RESULT");

        //when
        injector.getInstance(TriggerResultHandler.class).handleResult(result);

        //then
        CellHelper cellHelper = injector.getInstance(CellHelper.class);
        Cell orkCell = cellHelper.getCellAtCoordinates(4, 4).get();
        Cell boarRiderCell = cellHelper.getCellAtCoordinates(4, 5).get();
        Assert.assertTrue(orkCell.getCharacter().isPresent());
        Assert.assertTrue(boarRiderCell.getCharacter().isPresent());
    }
}