package com.slob.aqfw.level.skill.heroes.medic;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterRepository;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class WakeUpSkillTest {
    private LogicService logicService;
    private Character heal;
    private SkillTargetingHelper targetingHelper;
    private CellHelper cellHelper;
    private CharacterRepository characterRepository;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        Character knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        heal = logicService.getLevelService().createCharacter(CharacterFactory.HEAL_ID);
        Character ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(knight, 5, 4);
        logicService.getLevelService().addCharacter(ork, 4, 5);
        logicService.getLevelService().addCharacter(heal, 6, 4);
        Injector injector = logicService.getInjector();
        targetingHelper = injector.getInstance(SkillTargetingHelper.class);
        cellHelper = injector.getInstance(CellHelper.class);
        characterRepository = injector.getInstance(CharacterRepository.class);

        Cell knightCell = cellHelper.getCharacterCell(knight);
        for (int i = 0; i < 20; i++) {
            logicService.getLevelService().performSkill(ork.getSkills().get(0), knightCell);
        }
    }

    @Test
    public void apply() {
        //given
        Cell cell = cellHelper.getCellAtCoordinates(5, 4).get();

        //when
        logicService.getLevelService().performSkill(heal.getSkills().get(6), cell);

        //then
        Assert.assertTrue(cell.getCharacter().isPresent());
        Assert.assertTrue(cell.getCharacter().get().getId().startsWith(CharacterFactory.KNIGHT_ID));
        Assert.assertTrue(cell.getCharacter().isPresent());
        Assert.assertEquals(3, characterRepository.getCharacterList().size());
    }

    @Test
    public void getTargetedCells() {
        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(heal.getSkills().get(6));

        //then
        Assert.assertFalse(allowedTargetList.isEmpty());
        Assert.assertEquals(1, allowedTargetList.size());
        Assert.assertSame(cellHelper.getCellAtCoordinates(5, 4).get(), allowedTargetList.get(0));
    }
}