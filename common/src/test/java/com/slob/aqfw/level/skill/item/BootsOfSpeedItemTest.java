package com.slob.aqfw.level.skill.item;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.item.Item;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterSpeedManager;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

public class BootsOfSpeedItemTest {

    @Test
    public void use() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = service.getService();
        logicService.getLevelService().generateLevel();
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);
        Item item = new BootsOfSpeedItem();
        logicService.getInjector().getInstance(ItemRepository.class).getCharacterItemMap().put(character, item);

        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(4, CharacterSpeedManager.getCharacterSpeed(character));
    }
}