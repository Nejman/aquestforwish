package com.slob.aqfw.level.skill.eneimies.boarrider;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

public class BoarRiderAgonyTest {

    @Test
    public void performAgonyTest() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        LogicService logicService = service.getService();
        logicService.getLevelService().generateLevel();
        Character rider = logicService.getLevelService().createCharacter(CharacterFactory.BOAR_RIDER_ID);
        logicService.getLevelService().addCharacter(rider, 0, 0);
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.BEAST_ID);
        logicService.getLevelService().addCharacter(character, 1, 1);
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);

        //when
        logicService.getLevelService().performSkill(character.getSkills().get(0), cellHelper.getCharacterCell(rider));

        //then
        Cell cell = cellHelper.getCellAtCoordinates(0, 0).get();
        Assert.assertTrue(cell.getCharacter().isPresent());
        Assert.assertTrue(cell.getCharacter().get().getId().startsWith("ORK_ID"));
    }
}