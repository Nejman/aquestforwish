package com.slob.aqfw.level.skill.heroes.medic;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class JumpToWoundedSkillTest {
    private LogicService logicService;
    private Character knight;
    private Character heal;
    private Character ork;
    private SkillTargetingHelper targetingHelper;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        heal = logicService.getLevelService().createCharacter(CharacterFactory.HEAL_ID);
        ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(knight, 5, 4);
        logicService.getLevelService().addCharacter(ork, 4, 5);
        logicService.getLevelService().addCharacter(heal, 6, 4);
        targetingHelper = logicService.getInjector().getInstance(SkillTargetingHelper.class);
    }

    @Test
    public void jumpToWoundedTargetingSkillTest() {
        //given
        CellHelper cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        Cell target = cellHelper.getCellAtCoordinates(5, 4).get();
        logicService.getLevelService().performSkill(ork.getSkills().get(0), target);

        //when
        List<Cell> allowedTargetList = targetingHelper.getAllowedTargetList(heal.getSkills().get(2));

        //then
        Assert.assertEquals(4, allowedTargetList.size());
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 3).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(4, 4).get()));
        Assert.assertTrue(allowedTargetList.contains(cellHelper.getCellAtCoordinates(5, 5).get()));
    }

}