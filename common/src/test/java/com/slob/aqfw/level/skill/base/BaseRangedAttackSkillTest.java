package com.slob.aqfw.level.skill.base;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.HexCreator;
import com.slob.aqfw.core.descriptor.DescriptorReader;
import com.slob.aqfw.core.descriptor.DescriptorRepository;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterDamageManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.level.hex.CellRepository;
import com.slob.aqfw.level.skill.SkillTargetingHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

public class BaseRangedAttackSkillTest {
    private Character attacker;
    private Character target;
    private CellHelper cellHelper;
    private CellRepository repository;

    @Before
    public void setUp() {
        attacker = new Character(null, 0, ImmutableMap.of(), ImmutableMap.of(), null, 0, 5, 0, null);
        target = new Character(null, 10, ImmutableMap.of(), ImmutableMap.of(), null, 0, 0, 0, null);
        repository = new CellRepository();
        List<Cell> cells = HexCreator.createHexField(3);
        repository.setCells(cells);
        cellHelper = new CellHelper(repository);
        Optional<Cell> attackerCell = cellHelper.getCellAtCoordinates(2, 2);
        attackerCell.get().setCharacter(attacker);
        Optional<Cell> targetCell = cellHelper.getCellAtCoordinates(0, 2);
        targetCell.get().setCharacter(target);
        CharacterDamageManager healthManager = new CharacterDamageManager(cellHelper);

        Injector injector = Mockito.mock(Injector.class);
        Mockito.when(injector.getInstance(CharacterDamageManager.class)).thenReturn(healthManager);

        DescriptorRepository descriptorRepository = new DescriptorRepository();
        DescriptorReader descriptorReader = new DescriptorReader(descriptorRepository);

        Skill skill = BaseRangedAttackSkill.getInstance(attacker, injector, descriptorReader.getById(SkillDescriptor.class, "BASE_RANGED_ATTACK"));
        attacker.setSkills(ImmutableList.of(skill));

    }

    @Test
    public void baseAttackTargetingTest() {
        //given
        SkillTargetingHelper helper = new SkillTargetingHelper(null, cellHelper);

        //when
        List<Cell> allowedTargetList = helper.getAllowedTargetList(attacker.getSkills().get(0));

        //then
        boolean isInRange = false;
        for (Cell cell : allowedTargetList) {
            if (cell.getCharacter().isPresent() && cell.getCharacter().get().equals(target)) {
                isInRange = true;
                break;
            }
        }
        Assert.assertTrue(isInRange);
    }

    @Test
    public void baseAttackTest() {
        //when
        attacker.getSkills().get(0).apply(cellHelper.getCellAtCoordinates(0, 2).get());

        //then
        Assert.assertEquals(5, target.getCurrentHealth());
    }

}