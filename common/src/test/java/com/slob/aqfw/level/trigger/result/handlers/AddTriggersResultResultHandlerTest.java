package com.slob.aqfw.level.trigger.result.handlers;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.level.trigger.Trigger;
import com.slob.aqfw.level.trigger.TriggerRepository;
import com.slob.aqfw.level.trigger.result.TriggerResult;
import com.slob.aqfw.level.trigger.result.TriggerResultFactory;
import com.slob.aqfw.level.trigger.result.TriggerResultHandler;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class AddTriggersResultResultHandlerTest {

    @Test
    public void handle() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        mainService.getService().getLevelService().generateLevel();
        Injector injector = mainService.getService().getInjector();
        TriggerResult result = injector.getInstance(TriggerResultFactory.class).createTriggerResult("TEST_ADD_TRIGGERS_RESULT");

        //when
        injector.getInstance(TriggerResultHandler.class).handleResult(result);

        //then
        List<Trigger> triggers = injector.getInstance(TriggerRepository.class).getTriggers();
        Assert.assertEquals(3, triggers.size());
        Assert.assertEquals("TEST_DIALOG", triggers.get(0).getId());
        Assert.assertEquals("TEST_GO_TO_ZONE_ALL", triggers.get(1).getId());
        Assert.assertEquals("TEST_GO_TO_ZONE_ANY", triggers.get(2).getId());
    }
}