package com.slob.aqfw.level.trigger.handlers;

import com.google.inject.Injector;
import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.level.trigger.Trigger;
import com.slob.aqfw.level.trigger.TriggerFactory;
import com.slob.aqfw.level.trigger.TriggerManager;
import com.slob.aqfw.level.trigger.TriggerRepository;
import com.slob.aqfw.service.GameState;
import com.slob.aqfw.service.GameStateCache;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GoToZoneAllTriggerHandlerTest {
    private Injector injector;
    private LevelService levelService;
    private Character knight;
    private Character mage;

    @Before
    public void setUp() {
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        injector = mainService.getService().getInjector();
        levelService = mainService.getService().getLevelService();
        levelService.generateLevel();
        CharacterFactory characterFactory = injector.getInstance(CharacterFactory.class);
        knight = characterFactory.createCharacter("KNIGHT_ID", "KNIGHT_ID");
        mage = characterFactory.createCharacter("MAGE_ID", "MAGE_ID");
        Trigger trigger = injector.getInstance(TriggerFactory.class).createTrigger("TEST_GO_TO_ZONE_ALL");
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);
        triggerRepository.getTriggers().add(trigger);

    }

    @Test
    public void noneInZone() {
        //given
        levelService.addCharacter(knight, 0, 0);
        levelService.addCharacter(mage, 1, 0);
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertFalse(triggerRepository.getTriggers().isEmpty());
        Assert.assertEquals(GameState.MAIN_MENU, injector.getInstance(GameStateCache.class).getGameState());
    }


    @Test
    public void oneInZone() {
        //given
        levelService.addCharacter(knight, 3, 4);
        levelService.addCharacter(mage, 1, 0);
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertFalse(triggerRepository.getTriggers().isEmpty());
        Assert.assertEquals(GameState.MAIN_MENU, injector.getInstance(GameStateCache.class).getGameState());
    }

    @Test
    public void allInZone() {
        //given
        levelService.addCharacter(knight, 3, 4);
        levelService.addCharacter(mage, 3, 5);
        TriggerRepository triggerRepository = injector.getInstance(TriggerRepository.class);

        //when
        injector.getInstance(TriggerManager.class).handleTriggers();

        //then
        Assert.assertEquals(1, triggerRepository.getTriggers().size());
        Assert.assertEquals(GameState.WON_LEVEL, injector.getInstance(GameStateCache.class).getGameState());
    }
}