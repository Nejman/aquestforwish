package com.slob.aqfw.level.skill.heroes.beast;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.effect.EffectType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.LevelService;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Test;

public class EnPassantSkillTest {

    @Test
    public void apply() {
        //given
        MainService mainService = CommonSkillSetUp.getMainServiceInstance();
        LevelService service = mainService.getService().getLevelService();
        service.generateLevel();
        Character beast = service.createCharacter(CharacterFactory.BEAST_ID);
        service.addCharacter(beast, 4, 4);

        //when
        service.performSkill(beast.getSkills().get(2), mainService.getService().getInjector().getInstance(CellHelper.class).getCharacterCell(beast));

        //then
        Assert.assertFalse(beast.getActiveEffects().isEmpty());
        Assert.assertEquals(1, beast.getActiveEffects().size());
        Assert.assertSame(EffectType.EN_PASSANT, beast.getActiveEffects().get(0).getEffectType());
    }
}