package com.slob.aqfw.level.skill.heroes.knight;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterArmourManager;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ShieldBreakSkillTest {

    private LogicService logicService;
    private CellHelper cellHelper;
    private Character knight;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        cellHelper = logicService.getInjector().getInstance(CellHelper.class);
        logicService.getLevelService().addCharacter(knight, 5, 4);
    }

    @Test
    public void noArmourTest() {
        //given
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        logicService.getLevelService().addCharacter(character, 4, 4);

        //when
        logicService.getLevelService().performSkill(knight.getSkills().get(3), cellHelper.getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertFalse(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());

    }

    @Test
    public void armouredSideTest() {
        //given
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        character.setRotation(Side.LEFT_DOWN.getSideCode());
        logicService.getLevelService().addCharacter(character, 4, 4);

        //when
        logicService.getLevelService().performSkill(knight.getSkills().get(3), cellHelper.getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertEquals(4, character.getCurrentHealth());
        Assert.assertEquals(0, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT));
    }

    @Test
    public void nonArmouredSideTest() {
        //given
        Character character = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(character, 4, 4);

        //when
        logicService.getLevelService().performSkill(knight.getSkills().get(3), cellHelper.getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertTrue(cellHelper.getCellAtCoordinates(4, 4).get().getCharacter().isPresent());
        Assert.assertEquals(3, character.getCurrentHealth());
        Assert.assertEquals(1, CharacterArmourManager.getArmourOnSide(character, Side.LEFT_DOWN));
        Assert.assertEquals(1, CharacterArmourManager.getArmourOnSide(character, Side.RIGHT_DOWN));
    }
}