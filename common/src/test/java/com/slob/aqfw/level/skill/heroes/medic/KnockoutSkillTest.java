package com.slob.aqfw.level.skill.heroes.medic;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.skill.PointType;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterPointManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KnockoutSkillTest {
    private LogicService logicService;
    private Character character;

    @Before
    public void setUp() {
        //given
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        character = logicService.getLevelService().createCharacter(CharacterFactory.HEAL_ID);
        logicService.getLevelService().addCharacter(character, 5, 4);
    }

    @Test
    public void use() {
        //given
        Character goblin = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        logicService.getLevelService().addCharacter(goblin, 4, 4);

        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().performSkill(character.getSkills().get(3), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(4, 4).get());

        //then
        Assert.assertEquals(0, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin));
        Assert.assertEquals(0, CharacterPointManager.getRemainingPoints(PointType.MOVEMENT, goblin));
        Assert.assertEquals(0, CharacterPointManager.getRemainingPoints(PointType.ATTACK, goblin));
    }

    @Test
    public void pointsAreResetInNextTurn() {
        //given
        Character goblin = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        logicService.getLevelService().addCharacter(goblin, 4, 4);

        //when
        logicService.getLevelService().endTurn();
        logicService.getLevelService().performSkill(character.getSkills().get(3), logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(4, 4).get());
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin));
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.MOVEMENT, goblin));
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ATTACK, goblin));

    }
}