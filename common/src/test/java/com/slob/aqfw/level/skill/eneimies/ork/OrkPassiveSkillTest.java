package com.slob.aqfw.level.skill.eneimies.ork;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.skill.PointType;
import com.slob.aqfw.core.skill.Skill;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.entities.characters.CharacterPointManager;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class OrkPassiveSkillTest {
    private LogicService logicService;
    private Character goblin1;
    private Character goblin2;
    private Character goblin3;
    private Character knight;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        Character ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(ork, 5, 4);
        goblin1 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        goblin2 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        goblin3 = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        logicService.getLevelService().addCharacter(goblin1, 4, 4);
        logicService.getLevelService().addCharacter(goblin2, 5, 6);
        logicService.getLevelService().addCharacter(goblin3, 5, 1);
        logicService.getLevelService().addCharacter(knight, 6, 4);

    }

    @Test
    public void skillTest() {
        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(3, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin1));
        Assert.assertEquals(3, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin2));
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin3));
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ACTION, knight));
    }


    @Test
    public void noSkillAfterOrkDeathTest() {
        //given
        Character attacker = logicService.getLevelService().createCharacter(CharacterFactory.GOBLIN_WARRIOR_ID);
        logicService.getLevelService().addCharacter(attacker, 0, 0);
        logicService.getLevelService().endTurn();
        Skill skill = attacker.getSkills().get(0);
        Cell target = logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(5, 4).get();
        logicService.getLevelService().performSkill(skill, target);
        logicService.getLevelService().performSkill(skill, target);
        logicService.getLevelService().performSkill(skill, target);
        logicService.getLevelService().performSkill(skill, target);
        logicService.getLevelService().performSkill(skill, target);
        logicService.getLevelService().performSkill(skill, target);

        //when
        logicService.getLevelService().endTurn();

        //then
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin1));
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin2));
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ACTION, goblin3));
        Assert.assertEquals(1, CharacterPointManager.getRemainingPoints(PointType.ACTION, knight));
    }
}