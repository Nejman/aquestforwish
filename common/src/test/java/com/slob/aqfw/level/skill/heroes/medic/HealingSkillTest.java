package com.slob.aqfw.level.skill.heroes.medic;

import com.slob.aqfw.CommonSkillSetUp;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.entities.characters.Character;
import com.slob.aqfw.entities.characters.CharacterFactory;
import com.slob.aqfw.level.hex.CellHelper;
import com.slob.aqfw.service.LogicService;
import com.slob.aqfw.service.MainService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class HealingSkillTest {
    private LogicService logicService;
    private Character knight;
    private Character heal;
    private Character ork;

    @Before
    public void setUp() {
        MainService service = CommonSkillSetUp.getMainServiceInstance();
        logicService = service.getService();
        logicService.getLevelService().generateLevel();
        knight = logicService.getLevelService().createCharacter(CharacterFactory.KNIGHT_ID);
        heal = logicService.getLevelService().createCharacter(CharacterFactory.HEAL_ID);
        ork = logicService.getLevelService().createCharacter(CharacterFactory.ORK_ID);
        logicService.getLevelService().addCharacter(knight, 5, 4);
        logicService.getLevelService().addCharacter(ork, 4, 5);
        logicService.getLevelService().addCharacter(heal, 6, 5);
    }

    @Test
    public void healingSkillTest() {
        //given
        Cell target = logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(5, 4).get();
        logicService.getLevelService().performSkill(ork.getSkills().get(0), target);

        //when
        logicService.getLevelService().performSkill(heal.getSkills().get(0), target);

        //then
        Assert.assertEquals(15, knight.getCurrentHealth());
    }

    @Test
    public void healingToLessThanMaxSkillTest() {
        //given
        Cell target = logicService.getInjector().getInstance(CellHelper.class).getCellAtCoordinates(5, 4).get();
        for (int i = 0; i < 5; i++) {
            logicService.getLevelService().performSkill(ork.getSkills().get(0), target);
        }

        //when
        logicService.getLevelService().performSkill(heal.getSkills().get(0), target);

        //then
        Assert.assertEquals(13, knight.getCurrentHealth());
    }
}