package com.slob.aqfw;

import com.slob.aqfw.service.MainService;

public class CommonSkillSetUp {
    private static final String lock = "lock";

    private static volatile MainService mainService = null;

    public static MainService getMainServiceInstance() {
        if (mainService == null) {
            synchronized (lock) {
                if (mainService == null) {
                    mainService = new MainService();
                }
            }
        }
        return mainService;
    }
}
